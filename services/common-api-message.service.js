const Masters = {
    UNIT_NOT_FOUND: "Unit id is not exist in our records.",
    SECTION_NOT_FOUND: "Section id is not exist in our records.",
    DESK_NOT_FOUND: "Desk id is not exist in our records.",
}

const Message = {
    DATA_FOUND: "Data Found!",
    NO_DATA_FOUND: "No Data Found!",
    TO_MANY_ATTEMPTS: "User account has been blocked, Please contact Admin",
    MOBILE_NOT_MAPPED: "Mobile Not Mapped To This User Id",
    MOBILE_ALREADY_REG: "Mobile is already registered",
    MOBILE_NOT_ALREADY_REG: "Mobile is not registered",
    PIN_NOT_SET: "User is not registered",
    ACTIVE: "Active Session",
    INVALID_TOKEN: "Invalid Token!",
    SOMETHING_WRONG: "Something went wrong!"
}

const AdminnRole = {
    ALREADY_EXIST: "Role Already Exist",
    SAVE_SUCCESS: "Role save successfully!",
    UPDATE_SUCCESS: "Role update successfully!",
    DELETE_SUCCESS: "Role status updated successfully!",
}

const Role = {
    ALREADY_EXIST: "Role already exist",
    SAVE_SUCCESS: "Role has been saved successfully!",
    UPDATE_SUCCESS: "Role has been updated successfully!",
    DELETE_SUCCESS: "Role status has been updated successfully!",
    REQUIRED_ADMIN_ROLE_NAME: "Role name is mandatory!",
    REQUIRED_ADMIN_ROLE_ID: "Admin Role id is mandatory!"
}

const State = {
    ALREADY_EXIST: "State already exist",
    SAVE_SUCCESS: "State has been saved successfully!",
    UPDATE_SUCCESS: "State has been updated successfully!",
    DELETE_SUCCESS: "State status has been updated successfully!",
}

const District = {
    ALREADY_EXIST: "District already exist",
    SAVE_SUCCESS: "District has been saved successfully!",
    UPDATE_SUCCESS: "District has been updated successfully!",
    DELETE_SUCCESS: "District status has been updated successfully!",
}

const Taluk = {
    ALREADY_EXIST: "Taluk already exist",
    SAVE_SUCCESS: "Taluk has been saved successfully!",
    UPDATE_SUCCESS: "Taluk has been updated successfully!",
    DELETE_SUCCESS: "Taluk status has been updated successfully!",
}
const Place = {
    ALREADY_EXIST: "Place already exist",
    SAVE_SUCCESS: "Place has been saved successfully!",
    UPDATE_SUCCESS: "Place has been updated successfully!",
    DELETE_SUCCESS: "Place status has been updated successfully!",
}
const Tag = {
    ALREADY_EXIST: "Tag already exist",
    SAVE_SUCCESS: "Tag has been saved successfully!",
    UPDATE_SUCCESS: "Tag has been updated successfully!",
    DELETE_SUCCESS: "Tag status has been updated successfully!",

};

const Grade = {
    ALREADY_EXIST: "Grade already exist",
    SAVE_SUCCESS: "Grade has been saved successfully!",
    UPDATE_SUCCESS: "Grade has been updated successfully!",
    DELETE_SUCCESS: "Grade status has been updated successfully!",

};
const ProfileType = {
    ALREADY_EXIST: "Profile Type already exist",
    SAVE_SUCCESS: "Profile Type has been saved successfully!",
    UPDATE_SUCCESS: "Profile Type has been updated successfully!",
    DELETE_SUCCESS: "Profile Type status has been updated successfully!",

};
const AccusedAbscondingType = {
    ALREADY_EXIST: "Accused Absconding Type already exist",
    SAVE_SUCCESS: "Accused Absconding Type has been saved successfully!",
    UPDATE_SUCCESS: "Accused Absconding Type has been updated successfully!",
    DELETE_SUCCESS: "Accused Absconding Type status has been updated successfully!",

};
const RequestType = {
    ALREADY_EXIST: "Request Type already exist",
    SAVE_SUCCESS: "Request Type has been saved successfully!",
    UPDATE_SUCCESS: "Request Type has been updated successfully!",
    DELETE_SUCCESS: "Request status has been updated successfully!",
    REQUEST_NOT_EXISTS: "Request Type is not exist in our records",
    REQUEST_EXISTS: "Request is not exist in our records",
    REQUEST_STATUS_NOT_EXISTS: "Request Status Type is not exist in our records",
    REQUEST_SUMMARY_UPDATED: "Request final summary has been updated successfully.",

}

const Organization = {
    ALREADY_EXIST: "Organization already exist",
    SAVE_SUCCESS: "Organization has been saved successfully!",
    UPDATE_SUCCESS: "Organization has been updated successfully!",
    DELETE_SUCCESS: "Organization status has been updated successfully!",
    REQUIRED_ORGANIZATION_NAME: "Organization name is mandatory!",
    REQUIRED_ORGANIZATION_ID: "Organization id is mandatory!"
};

const OrganizationType = {
    ALREADY_EXIST: "Organization type already exist",
    SAVE_SUCCESS: "Organization type has been saved successfully!",
    UPDATE_SUCCESS: "Organization type has been updated successfully!",
    DELETE_SUCCESS: "Organization type status has been updated successfully!",
    REQUIRED_ORGANIZATION_TYPE_NAME: "Organization type name is mandatory!",
    REQUIRED_ORGANIZATION_TYPE_ID: "Organization type id is mandatory!"
};

const OrganizationUnitDesk = {
    ALREADY_EXIST: "Organization unit desk already exist",
    SAVE_SUCCESS: "Organization unit desk has been saved successfully!",
    UPDATE_SUCCESS: "Organization unit desk has been updated successfully!",
    DELETE_SUCCESS: "Organization unit desk status has been updated successfully!",
    UNIT_NOT_FOUND: "Unit id is not exist in our records",
    SECTION_NOT_FOUND: "Section id is not exist in our records",
    DESK_NOT_FOUND: "Desk id is not exist in our records",
    ORGANIZATION_NOT_FOUND: "Organization id is not exist in our records",
    ORGANIZATION_TYPE_NOT_FOUND: "Organization type id is not exist in our records",
};

const Leader = {
    ALREADY_EXIST: "Leader already exist",
    SAVE_SUCCESS: "Leader has been saved successfully!",
    UPDATE_SUCCESS: "Leader has been updated successfully!",
    DELETE_SUCCESS: "Leader status has been updated successfully!",
    REQUIRED_LEADER_NAME: "Leader name is mandatory!",
    REQUIRED_LEADER_ID: "Leader id is mandatory!",
    UNIT_NOT_FOUND: "Unit id is not exist in our records",
    SECTION_NOT_FOUND: "Section id is not exist in our records",
    DESK_NOT_FOUND: "Desk id is not exist in our records",
    INVALID_LEADER_ID: "Leader id is not exist in our records!",
};

const Jurisdiction = {
    ALREADY_EXIST: "Jurisdiction already exist",
    SAVE_SUCCESS: "Jurisdiction has been saved successfully!",
    UPDATE_SUCCESS: "Jurisdiction has been updated successfully!",
    DELETE_SUCCESS: "Jurisdiction status has been updated successfully!",
    REQUIRED_Jurisdiction_NAME: "Jurisdiction name is mandatory!",
    REQUIRED_Jurisdiction_ID: "Jurisdiction id is mandatory!"
};

const Division = {
    ALREADY_EXIST: "Division already exist",
    SAVE_SUCCESS: "Division has been saved successfully!",
    UPDATE_SUCCESS: "Division has been updated successfully!",
    DELETE_SUCCESS: "Division status has been updated successfully!",
    REQUIRED_Division_NAME: "Division name is mandatory!",
    REQUIRED_Division_ID: "Division id is mandatory!"
};

const Section = {
    ALREADY_EXIST: "Section already exist",
    SAVE_SUCCESS: "Section has been saved successfully!",
    UPDATE_SUCCESS: "Section has been updated successfully!",
    DELETE_SUCCESS: "Section status has been updated successfully!",
    REQUIRED_Section_NAME: "Section name is mandatory!",
    REQUIRED_Section_ID: "Section id is mandatory!"
};

const Desk = {
    ALREADY_EXIST: "Desk already exist",
    SAVE_SUCCESS: "Desk has been saved successfully!",
    UPDATE_SUCCESS: "Desk has been updated successfully!",
    DELETE_SUCCESS: "Desk status has been updated successfully!",
    REQUIRED_Desk_NAME: "Desk name is mandatory!",
    REQUIRED_Desk_ID: "Desk id is mandatory!"
};

const RequestMessageType = {
    ALREADY_EXIST: "Request Message Type already exist",
    SAVE_SUCCESS: "Request message type has been saved successfully!",
    UPDATE_SUCCESS: "Request message type has been updated successfully!",
    DELETE_SUCCESS: "Request message type status has been updated successfully!",
    REQUIRED_REQUEST_MESSAGE_TYPE_NAME: "Request message type name is mandatory!",
    REQUIRED_REQUEST_MESSAGE_TYPE_ID: "Request message type id is mandatory!"
};

const RequestMessageSubType = {
    ALREADY_EXIST: "Request Message Sub Type already exist",
    SAVE_SUCCESS: "Request message sub type has been saved successfully!",
    UPDATE_SUCCESS: "Request message sub type has been updated successfully!",
    DELETE_SUCCESS: "Request message sub type status has been updated successfully!",
    REQUIRED_REQUEST_MESSAGE_TYPE_ID: "Request message type id is mandatory!",
    REQUIRED_REQUEST_MESSAGE_SUB_TYPE_NAME: "Request message sub type name is mandatory!",
    REQUIRED_REQUEST_MESSAGE_SUB_TYPE_ID: "Request message sub type id is mandatory!"
};

const RecipientType = {
    ALREADY_EXIST: "Recipient type already exists",
    SAVE_SUCCESS: "Recipient type has been saved successfully!",
    UPDATE_SUCCESS: "Recipient type has been updated successfully!",
    DELETE_SUCCESS: "Recipient type status has been updated successfully!",
    REQUIRED_RECIPIENT_TYPE_NAME: "Recipient type name is mandatory!",
    REQUIRED_RECIPIENT_TYPE_ID: "Recipient type id is mandatory!"
};

const ForwardStatusType = {
    ALREADY_EXIST: "Forward status type already exists",
    SAVE_SUCCESS: "Forward status type has been saved successfully!",
    UPDATE_SUCCESS: "Forward status type has been updated successfully!",
    DELETE_SUCCESS: "Forward status type status has been updated successfully!",
    REQUIRED_FORWARD_STATUS_TYPE_NAME: "Forward status type name is mandatory!",
    REQUIRED_FORWARD_STATUS_TYPE_ID: "Forward status type id is mandatory!",
    STATUS_NOT_EXISTS: "Forward status not exists in our records!",
    FORWARD_NOT_EXISTS: "Forward not exists in our records!",
};

const RequestStatusType = {
    ALREADY_EXIST: "Request Status Type already exist",
    SAVE_SUCCESS: "Request Status Type has been saved successfully!",
    UPDATE_SUCCESS: "Request Status Type has been updated successfully!",
    DELETE_SUCCESS: "Request Status Type status has been updated successfully!",
    NOT_EXISTS: "Request status not exists in our records!",
};
const AttributeType = {
    ALREADY_EXIST: "Attribute Type already exist",
    SAVE_SUCCESS: "Attribute Type has been saved successfully!",
    UPDATE_SUCCESS: "Attribute Type has been updated successfully!",
    DELETE_SUCCESS: "Attribute Type status has been updated successfully!",

};
const Language = {
    ALREADY_EXIST: "Language already exist",
    SAVE_SUCCESS: "Language has been saved successfully!",
    UPDATE_SUCCESS: "Language has been updated successfully!",
    DELETE_SUCCESS: "Language status has been updated successfully!",

};

const User = {
    ALREADY_EXIST: "User already exists",
    SAVE_SUCCESS: "User has been saved successfully!",
    UPDATE_SUCCESS: "User has been updated successfully!",
    DELETE_SUCCESS: "User status has been updated successfully!",
    REQUIRED_USER_EMAIL: "Email is mandatory!",
    REQUIRED_USER_PHONE: "Mobile is mandatory!",
    REQUIRED_USER_PHONE_LENGTH: "Mobile must be 10 numbers!",
    REQUIRED_DESIGNATION_ID: "Designation id is mandatory!",
    REQUIRED_SUPERVISOR_ID: "Supervisor id is mandatory!",
    REQUIRED_LANGUAGE_ID: "Language id is mandatory!",
    OTP_SENT: "OTP sent to your mobile number",
    USER_PIN_SUCCESS: "PIN has been saved successfully!",
    USER_OTP: "Incorrect OTP",
    USER_OTP_VERIFY: "OTP verified successfully!",
    USER_PIN: "Incorrect PIN",
    USER_PIN_VERIFY: "PIN verified successfully!",
    USER_LOGIN_SUCCESS: "Login successful!",
    USER_ACTIVATE_SUCCESS: "User activated successfully!",
    USER_LOGIN_FAILED: "Login Failed!",
    USER_NOT_EXISTS: "User does not exist in our records !",
}

const EventMessageType = {
    ALREADY_EXIST: "Event message type already exists",
    SAVE_SUCCESS: "Event message type has been saved successfully!",
    UPDATE_SUCCESS: "Event message type has been updated successfully!",
    DELETE_SUCCESS: "Event message status has been updated successfully!",
    REQUIRED_EVENT_MESSAGE_TYPE_NAME: "Event message type name is mandatory!",
    REQUIRED_EVENT_MESSAGE_TYPE_ID: "Event message type id is mandatory!"
};

const EventMessageSubType = {
    ALREADY_EXIST: "Event sub message type already exists",
    SAVE_SUCCESS: "Event sub message type has been saved successfully!",
    UPDATE_SUCCESS: "Event sub message type has been updated successfully!",
    DELETE_SUCCESS: "Event sub message type status has been updated successfully!",
    REQUIRED_EVENT_SUB_MESSAGE_TYPE_NAME: "Event sub message type name is mandatory!",
    REQUIRED_EVENT_SUB_MESSAGE_TYPE_ID: "Event sub message type id is mandatory!"
};

const EventType = {
    ALREADY_EXIST: "Event type already exists",
    SAVE_SUCCESS: "Event type has been saved successfully!",
    UPDATE_SUCCESS: "Event type has been updated successfully!",
    DELETE_SUCCESS: "Event type status has been updated successfully!",
    REQUIRED_EVENT_TYPE_NAME: "Event type name is mandatory!",
    REQUIRED_EVENT_TYPE_ID: "Event type id is mandatory!",
    REQUIRED_EVENT_ID: "Event id is mandatory!"
};

const EventStatusType = {
    ALREADY_EXIST: "Event status type already exists",
    SAVE_SUCCESS: "Event status type has been saved successfully!",
    UPDATE_SUCCESS: "Event status type has been updated successfully!",
    DELETE_SUCCESS: "Event status type status has been updated successfully!",
    REQUIRED_EVENT_STATUS_TYPE_NAME: "Event status type name is mandatory!",
    REQUIRED_EVENT_STATUS_TYPE_ID: "Event status type id is mandatory!",
    NOT_EXISTS: "Event status not exists in our records!",
}

const LanguageKey = {
    ALREADY_EXIST: "Language Key already exist",
    SAVE_SUCCESS: "Language Key has been saved successfully!",
    UPDATE_SUCCESS: "Language Key has been updated successfully!",
    DELETE_SUCCESS: "Language Key status has been updated successfully!",

};
const DocumentType = {
    ALREADY_EXIST: "Document Type already exist",
    SAVE_SUCCESS: "Document Type has been saved successfully!",
    UPDATE_SUCCESS: "Document Type has been updated successfully!",
    DELETE_SUCCESS: "Document Type status has been updated successfully!",

};
const DocumentUploadType = {
    ALREADY_EXIST: "Document Upload Type already exist",
    SAVE_SUCCESS: "Document Upload Type has been saved successfully!",
    UPDATE_SUCCESS: "Document Upload Type has been updated successfully!",
    DELETE_SUCCESS: "Document Upload Type status has been updated successfully!",

};

const ReportType = {
    ALREADY_EXIST: "Report Type Already Exist",
    SAVE_SUCCESS: "Report Type save successfully!",
    UPDATE_SUCCESS: "Report Type update successfully!",
    DELETE_SUCCESS: "Report Type status updated successfully!",

    ALREADY_EXIST: "Report Type already exist",
    SAVE_SUCCESS: "Report Type has been saved successfully!",
    UPDATE_SUCCESS: "Report Type has been updated successfully!",
    DELETE_SUCCESS: "Report Type status has been updated successfully!",
};

const Unit = {
    ALREADY_EXIST: "Unit already exist",
    SAVE_SUCCESS: "Unit has been saved successfully!",
    UPDATE_SUCCESS: "Unit has been updated successfully!",
    DELETE_SUCCESS: "Unit status has been updated successfully!",
    REQUIRED_UNIT_NAME: "Unit name is mandatory!",
    REQUIRED_UNIT_ID: "Unit id is mandatory!"
};

const UnitSectionDesk = {
    UNIT_NOT_FOUND: "Unit id is not exist in our records",
    PROFILE_TYPE_NOT_FOUND: "Profile type id is not exist in our records",
    SECTION_NOT_FOUND: "Section id is not exist in our records",
    DESK_NOT_FOUND: "Desk id is not exist in our records",
    ALREADY_EXIST: "Record already exist",
    SAVE_SUCCESS: "Record has been saved successfully!",
};

const Event = {
    ALREADY_EXIST: "Event Already Exist",
    SAVE_SUCCESS: "Event save successfully!",
    SUBMIT_SUCCESS: "Event submit successfully!",
    EVENT_NOT_EXISTS: "Event not exists in our records!",
    EVENT_TAG_CREATED: "Event tags has been created successfully!",
    EVENT_TAG_NOT_EXISTS: "Event tags not exists in our records!",
    EVENT_TAG_NOT_CREATE: "Something went wrong. Event tag has not been created.",
    EVENT_FORWARD_DUE_DATE: "Event due date should be greater than or equal to current date.",
    EVENT_FORWARD_NOT_CREATED: "Something went wrong. Event has not been forward.",
    EVENT_FORWARD_RECIPIENTS_NOT_CREATED: "Something went wrong. Event forward recipient has not been stored.",
    EVENT_FORWARD_RECIPIENTS_SUCCESS: "Event has been forwarded to recipients successfully.",
    EVENT_INVALID_DRAFT_ID: "Event draft not exists in our records!.",
    DRAFT_SAVE_SUCCESS: "Event draft save successfully!",
    EVENT_SUMMARY_UPDATED: "Event final summary has been updated successfully.",
    SAVE_SUCCESS_ATTACHMENT: "Event attachment save successfully!",
    EMPTY_EVENT_ID: "Event Id is required",
    EMPTY_EVENT_ATTACHMENT: "Please select at least one attachment!",
    EVENT_STATUS_NOT_EXISTS: "Event status not exists in our records!",
    EVENT_TYPE_NOT_EXISTS: "Event type not exists in our records!",
    EVENT_UPDATED: "Event details has been updated successfully!",
    EVENT_DRAFT_REMOVE: "Event draft remove successfully!",
    EVENT_DRAFT_UPDATE: "Event draft update successfully!",
    EVENT_REMOVE: "Event draft remove successfully!",
    EVENT_ATTACHMENT_REMOVE: "Event attachment remove successfully!",
    EVENT_INVALID_EVENT_ID: "Event  not exists in our records!.",
    EVENT_ATTACHMENT_INVALID_EVENT_ID: "Event attachment not exists in our records!.",
    UNIT_NOT_FOUND: "Unit id is not exist in our records.",
    DISTRICT_NOT_FOUND: "District id is not exist in our records.",
    ORGANIZATION_NOT_FOUND: "Organization id is not exist in our records.",
    LEADER_NOT_FOUND: "Leader id is not exist in our records.",
    DISTRICT_ID_NOT_FOUND: "District cannot be Empty",
    ORGANIZATION_ID_NOT_FOUND: "Organization cannot be Empty",
    LEADER_ID_NOT_FOUND: "Leader cannot be Empty",
    EVENT_DATE_NOT_FOUND: "Event date cannot be Empty",
    EVENT_TIME_NOT_FOUND: "Event time cannot be Empty",
    TAG_NOT_FOUND: "Event Tags cannot be Empty",
    SECTION_NOT_FOUND: "Section id is not exist in our records.",
    DESK_NOT_FOUND: "Desk id is not exist in our records.",
    GRADE_NOT_FOUND: "Grade id is not exist in our records.",
    RECIPIENT_TYPE_NOT_FOUND: "Recipient Type id is not exist in our records.",
    USER_NOT_FOUND: "User id is not exist in our records.",
    EVENT_MESSAGE_TYPE_NOT_FOUND: "Event Message Type id is not exist in our records.",
    EVENT_MESSAGE_SUB_TYPE_NOT_FOUND: "Event Message Type id is not exist in our records.",
    SOURCE_OF_MESSAGE_LIMIT: "Source of message  must be less than 200 characters",
    SUBJECT_LIMIT: "subject must be less than 200 characters",
    FARMER_LIMIT: "Farmer limit must be less than 100 characters",
    RESERVOIR_LEVEL_LIMIT: "Reservoir level limit must be less than 50 characters",
    GROSS_CAPACITY_LIMIT: "Gross capacity limit must be less than 50 characters",
    LIVE_CAPACITY_LIMIT: "Live capacity limit must be less than 50 characters",
    IN_FLOW_LIMIT: "In flow limit must be less than 50 characters",
    OUT_FLOW_LIMIT: "Out flow limit must be less than 50 characters",
    WATER_RELEASE_TO_CANAL_LIMIT: "Water release to canal limit must be less than 50 characters",
    RAIN_FALL_LIMIT: "Rain_fall_limit must be less than 50 characters",
    CC_SC_LIMIT: "Cc sc limit must be less than 100 characters",
    CASE_TYPE_LIMIT: "Case type limit must be less than 100 characters",
    VISITORS_LIMIT: "Visitors limit must be less than 200 characters",
    RELATION_LIMIT: "Relation limit must be less than 100 characters",
    VISITOR_MOBILE_NUMBER_LIMIT: "Visitor mobile number limit must be less than 50 characters",
    DEATH_LIMIT: "Death limit must be less than 20 characters",
    INJURY_LIMIT: "Injury limit must be less than 20 characters",
    CALL_RECEIVED_FROM_LIMIT: "Call received from limit must be less than 100 characters",
    UDR_NUMBER_LIMIT: "Udr number limit must be less than 100 characters",
    SECTION_OF_LAW_LIMIT: "Section of law mobile number limit must be less than 50 characters",
    VICTIM_LIMIT: "Victim limit must be less than 20 characters",
    COMPLAINANT_LIMIT: "Complainant limit must be less than 20 characters",
    OCCURANCE_PLACE_FROM_LIMIT: "Occurance Place from limit must be less than 100 characters",
    ACCUSED_LIMIT: "Accused limit must be less than 20 characters",
    POLICE_STATION_ID_NOT_FOUND: "Police station cannot be Empty",
    REPORT_DATE_NOT_FOUND: "Report Date cannot be Empty",
    OCCURANCE_DATE_NOT_FOUND: "Occurance Date cannot be Empty",
    OTHER_TAG_EMPTY: "Other Tags cannot be Empty",
    STATE_EMPTY: "State cannot be Empty",
    DISTRICT_EMPTY: "District cannot be Empty",
    TALUK_EMPTY: "Taluk cannot be Empty",
    ORGANIZATION_EMPTY: "Organization cannot be Empty",
    LEADER_EMPTY: "Leader cannot be Empty",
    DISTRICT_EMPTY: "District cannot be Empty",
    CROP_EMPTY: "Crop cannot be Empty",
    DAM_EMPTY: "Dam cannot be Empty",
    COURT_EMPTY: "Court cannot be Empty",
    POLITICAL_PARTY_EMPTY: "Political cannot be Empty",
    PERSON_EMPTY: "Person cannot be Empty",
    AGENCY_EMPTY: "Agency cannot be Empty",
    UNIT_EMPTY: "Unit cannot be Empty",
    SECTION_EMPTY: "Section cannot be Empty",
    DESK_EMPTY: "Desk cannot be Empty",
    EVENT_MESSAGE_TYPE_EMPTY: "Event message type cannot be Empty",
    EVENT_MESSAGE_SUB_TYPE_EMPTY: "Event message sub type cannot be Empty",
    GRADE_EMPTY: "Grade cannot be Empty",
    SOURCE_MESSAGE_EMPTY: "Source of message cannot be Empty",
    DESCRIPTION_MESSAGE_EMPTY: "Description cannot be Empty",
}

const RoleAccess = {
    ALREADY_EXIST: "Role Access already exist",
    SAVE_SUCCESS: "Role Access has been saved successfully!",
    SAVE_FAILED: "Failed to save Role Access",
    UPDATE_SUCCESS: "Role Access has been updated successfully!",
    DELETE_SUCCESS: "Role Access status has been updated successfully!",
}

const Profile = {
    ALREADY_EXIST: "User already exist",
    SAVE_SUCCESS: "User has been saved successfully!",
    UPDATE_SUCCESS: "User has been updated successfully!",
    DELETE_SUCCESS: "User status has been updated successfully!",
    UPDATE_SUCCESS: "User has been updated successfully!",
    REQUIRED_DESK_ID: "Desk id is mandatory!",
    REQUIRED_DISTRICT_ID: "District id is mandatory!",
    REQUIRED_DIVISION_ID: "Division id is mandatory!",
    REQUIRED_JURISDICTION_ID: "Jurisdiction id is mandatory!",
    REQUIRED_SECTON_ID: "Section id is mandatory!",
    REQUIRED_ROLE_ID: "Role id is mandatory!",
    REQUIRED_TALUK_ID: "Taluk id is mandatory!",
    REQUIRED_UNIT_ID: "Unit id is mandatory!",
}
const Designation = {
    ALREADY_EXIST: "Designation already exist",
    SAVE_SUCCESS: "Designation has been saved successfully!",
    UPDATE_SUCCESS: "Designation has been updated successfully!",
    DELETE_SUCCESS: "Designation status has been updated successfully!",
}
const ModuleSection = {
    ALREADY_EXIST: "Module Section already exist",
    SAVE_SUCCESS: "Module Section has been saved successfully!",
    UPDATE_SUCCESS: "Module Section has been updated successfully!",
    DELETE_SUCCESS: "Module Section status has been updated successfully!",
}

const UnitEventMessageType = {
    UNIT_NOT_FOUND: "Unit id is not exist in our records",
    DESK_NOT_FOUND: "Desk id is not exist in our records",
    EVENT_MESSAGE_TYPE_NOT_FOUND: "Event message type id is not exist in our records",
    EVENT_SUB_MESSAGE_TYPE_NOT_FOUND: "Event sub message type id is not exist in our records",
    ALREADY_EXIST: "Record already exist",
    SAVE_SUCCESS: "Record has been saved successfully!",
};
const Person = {
    ALREADY_EXIST: "Person already exist",
    SAVE_SUCCESS: "Person has been saved successfully!",
    UPDATE_SUCCESS: "Person has been updated successfully!",
    DELETE_SUCCESS: "Person status has been updated successfully!",

};

const Crop = {
    ALREADY_EXIST: "Crop already exist",
    SAVE_SUCCESS: "Crop has been saved successfully!",
    UPDATE_SUCCESS: "Crop has been updated successfully!",
    DELETE_SUCCESS: "Crop status has been updated successfully!",
    REQUIRED_CROP_NAME: "Crop name is mandatory!",
    REQUIRED_CROP_ID: "Crop id is mandatory!"
};

const Dam = {
    ALREADY_EXIST: "Dam already exist",
    SAVE_SUCCESS: "Dam has been saved successfully!",
    UPDATE_SUCCESS: "Dam has been updated successfully!",
    REQUIRED_DAM_NAME: "Dam name is mandatory!",
    REQUIRED_DAM_ID: "Dam id is mandatory!",
    CHECK_STATE: "State details not exixts in table!",
    CHECK_DISTRICT: "District details not exixts in table!",
    CHECK_TALUK: "Taluk details not exixts in table!",
    DELETE_SUCCESS: "Dam status has been updated successfully!",
};

const Court = {
    ALREADY_EXIST: "Court already exist",
    SAVE_SUCCESS: "Court has been saved successfully!",
    UPDATE_SUCCESS: "Court has been updated successfully!",
    DELETE_SUCCESS: "Court status has been updated successfully!",
    REQUIRED_COURT_NAME: "Court name is mandatory!",
    REQUIRED_COURT_ID: "Court id is mandatory!",
    CHECK_STATE: "State details not exixts in table!",
    CHECK_DISTRICT: "District details not exixts in table!",
    CHECK_TALUK: "Taluk details not exixts in table!",
};

const Prison = {
    ALREADY_EXIST: "Prison already exist",
    SAVE_SUCCESS: "Prison has been saved successfully!",
    UPDATE_SUCCESS: "Prison has been updated successfully!",
    REQUIRED_PRISON_NAME: "Prison name is mandatory!",
    REQUIRED_PRISON_ID: "Prison id is mandatory!",
    DELETE_SUCCESS: "Prison status has been updated successfully!",
    CHECK_STATE: "State details not exixts in table!",
    CHECK_DISTRICT: "District details not exixts in table!",
    CHECK_TALUK: "Taluk details not exixts in table!",
};

const PoliticalParty = {
    ALREADY_EXIST: "Political Party already exist",
    SAVE_SUCCESS: "Political Party has been saved successfully!",
    UPDATE_SUCCESS: "Political Party has been updated successfully!",
    DELETE_SUCCESS: "Political Party status has been updated successfully!",

};

const Agency = {
    ALREADY_EXIST: "Agency already exist",
    SAVE_SUCCESS: "Agency has been saved successfully!",
    UPDATE_SUCCESS: "Agency has been updated successfully!",
    DELETE_SUCCESS: "Agency status has been updated successfully!",
};

const Request = {
    REQUEST_TYPE_NOT_FOUND: "Request type id is not exist in our records.",
    REQUEST_STATUS_TYPE_NOT_FOUND: "Request status type id is not exist in our records.",
    REQUEST_MESSAGE_TYPE_NOT_FOUND: "Request message type id is not exist in our records.",
    RECIPIENT_TYPE_NOT_FOUND: "Recipient type id is not exist in our records.",
    REQUEST_MESSAGE_SUB_TYPE_NOT_FOUND: "Request message sub type id is not exist in our records.",
    PROFILE_TYPE_NOT_FOUND: "Profile type id is not exist in our records.",
    UNIT_NOT_FOUND: "Unit id is not exist in our records.",
    SECTION_NOT_FOUND: "Section id is not exist in our records.",
    DESK_NOT_FOUND: "Desk id is not exist in our records.",
    GRADE_NOT_FOUND: "Grade id is not exist in our records.",
    REQUEST_CREATED: "Request has been created successfully.",
    REQUEST_NOT_CREATED: "Something went wrong. Request has not been created.",
    REQUEST_RECIPIENT_NOT_CREATED: "Something went wrong. Request recipient has not been created.",
    REQUEST_ATTACHMENT_NOT_CREATED: "Something went wrong. Request attachment has not been created.",
    USERS_ID: "User ids cannot be Empty",
    REQUEST_FORWARD_DUE_DATE: "Request due date should be greater than or equal to current date.",
    REQUEST_FORWARD_NOT_CREATED: "Something went wrong. Request has not been forward.",
    REQUEST_FORWARD_RECIPIENTS_NOT_CREATED: "Something went wrong. Request forward recipient has not been stored.",
    REQUEST_FORWARD_RECIPIENTS_SUCCESS: "Request has been forwarded to recipients successfully.",

    REQUEST_INVALID_DRAFT_ID: "request draft not exists in our records!.",
    DRAFT_SAVE_SUCCESS: "Request draft save successfully!",
    SUBMIT_SAVE_SUCCESS: "Request submit successfully!",
    SAVE_SUCCESS_ATTACHMENT: "Request attachment save successfully!",
    EMPTY_REQUEST_ID: "Request Id is required",
    EMPTY_REQUEST_ATTACHMENT: "Please select at least one attachment!",

    REQUEST_NOT_EXISTS: "Request not exists in our records!",
    REQUEST_STATUS_NOT_EXISTS: "Request status not exists in our records!",
    UPDATE_SUCCESS: "Request status type has been updated successfully!",
    REQUEST_TAG_CREATED: "Request tags has been created successfully!",
    REQUEST_ID_REQUIRED: "Request ids cannot be Empty!",
    REQUEST_INVALID_ID: "request not exists in our records!.",
    REQUEST_ATTRIBUTE_RESPONSE_NOT_FOUND: "Request attribute responses cannot be Empty",
    REQUEST_MESSAGE_TYPE_EMPTY: " Request type cannot be Empty",
    REQUEST_MESSAGE_SUB_TYPE_EMPTY: "Request message sub type cannot be Empty",
    SUBJECT_EMPTY: "Subject cannot be Empty",
    REQUEST_ATTRIBUTE_RESPONSE_ALREADY_EXISTS: "You have submitted request attribute responses already.",
    REQUEST_ATTRIBUTE_RESPONSE_UPDATED: "Request attribute responses updated successfully.",
    REQUEST_ATTRIBUTE_RESPONSE_CREATED: "Request attribute responses submitted successfully.",
    REQUEST_ATTRIBUTE_RESPONSE_NOT_CREATE: "Request attribute responses submitted successfully.",
    REQUEST_RECIPIENT_NOT_FOUND: "Request recipient can not be empty.",
    REQUEST_RECIPIENT_UPDATED: "Request recipient has been updated successfully.",
};

const EventRequest = {
    STATUS_UPDATE: "Status has been updated successfully.",
    TYPE_UPDATE: "Type has been updated successfully.",
    TAGS_UPDATE: "Tags has been updated successfully.",
    All_UPDATE: "Event and request details has been updated successfully.",
    DRAFT_DELETE: "Drafts has been deleted successfully.",
};
const Document = {
    ALREADY_EXIST: "Document Already Exist",
    SAVE_SUCCESS: "Document save successfully!",
    DOCUMENT_NOT_EXISTS: "Document not exists in our records!",
    EMPTY_DOCUMENT_ID: "Document Id is required",
    EMPTY_DOCUMENT_ATTACHMENT: "Please select at least one attachment!",
    DOCUMENT_UPDATED: "Document has been updated successfully!",
    DOCUMENT_REMOVE: "Document remove successfully!",
    UNIT_NOT_FOUND: "Unit not exist in our records.",
    SECTION_NOT_FOUND: "Section not exist in our records",
    DESK_NOT_FOUND: "Desk id is not exist in our records.",
    TYPE_OF_DOCUMENT_NOT_FOUND: "Document Type not exist in our records",
    STATE_NOT_FOUND: "State not exist in our records.",
    DISTRICT_NOT_FOUND: "District not exist in our records.",
    CLASSIFICATION_LEVEL_NOT_FOUND: "Classification level not exist in our records.",
    SUB_CLASSIFICATION_LEVEL_NOT_FOUND: "Sub classification level  not exist in our records.",
    ORGANIZATION_NOT_FOUND: "Organization not exist in our records.",
    LEADER_NOT_FOUND: "Leader not exist in our records.",
    ACCUSED_ABS_TYPE_NOT_FOUND: "Accused absconding type not exist in our records.",
    DOCUMENT_ATTACHMENT_REMOVE: "Document attachment remove successfully!",
    DOCUMENT_ATTACHMENT_NOT_EXISTS: "Document attachment not exists in our records!",

};
const Periodicity = {
    ALREADY_EXIST: "Periodicity already exist",
    NOT_EXIST: "Periodicity not exists in our records!",
    REPORT_NOT_EXIST: "Report type not exists in our records!",
    SAVE_SUCCESS: "Periodicity has been saved successfully!",
    UPDATE_SUCCESS: "Periodicity has been updated successfully!",
    DELETE_SUCCESS: "Periodicity status has been updated successfully!",
    REQUIRED_NAME: "Periodicity name is mandatory!",
    REQUIRED_ID: "Periodicity id is mandatory!"
};

const Report = {
    DSR_GENERATED: "DSR report generate.",
    ADD_DSR_REPORT: "DSR report created successfully.",
    EMPTY_REPORT_ATTACHMENT: "Please select at least one attachment.",
    REPORT_NOT_EXISTS: "Report not exists in our records!",
    REPORT_REMOVE: "Report has been deleted successfully.",
    REPORT_TYPE_NOT_FOUND: "Report type not found.",
    REPORT_NOT_CREATED: "Something went wrong with save reprort.",
    REPORT_CREATED: "Report has been created successfully.",
}
const Reportinr = {
    SUBJECT_LIMIT: "Report name must be less than 500 characters",
    REPORT_CREATED: "INR Report created successfully!",
    REPORT_ADHOC_CREATED: "Adhoc Report created successfully!",
    REPORT_NOT_CREATED: "Something went wrong. Report has not been created.",
}

const profileCategory = {
    ALREADY_EXIST: "Profile Category already exist",
    SAVE_SUCCESS: "Profile Category has been saved successfully!",
    UPDATE_SUCCESS: "Profile Category has been updated successfully!",
    DELETE_SUCCESS: "Profile Category status has been updated successfully!",

};
const profileSubCategory = {
    ALREADY_EXIST: "Profile Sub Category already exist",
    SAVE_SUCCESS: "Profile Sub Category has been saved successfully!",
    UPDATE_SUCCESS: "Profile Sub Category has been updated successfully!",
    DELETE_SUCCESS: "Profile Sub Category status has been updated successfully!",

};
const DossierSocialAffair = {
    TALUK_NOT_FOUND: "Taluk not found in our record.",
    PROFILE_CAT_NOT_FOUND: "Profile category not found in our record.",
    PROFILE_SUB_CAT_NOT_FOUND: "Profile sub category not found in our record.",
    REQUEST_NOT_FOUND: "Request id not found in our record.",
    SAVE_SUCCESS: "Dossier Social Affair profile created successfully.",
    UPDATE_SUCCESS: "Dossier Social Affair profile updated successfully",
    INVALID_ID: "Dossier Social record  not found in our record. ",
    REMOVE_PROFILE: "Dossier Social record remove successfully.",
    LEADER_ADD: "Dossier Social Affair Leader added successfully.",
    LEADER_REMOVE: "Dossier Social Affair Leader remove successfully.",
    LEADER_EXIST: "You already added this leader."
}

const ProtecteeSecurity = {
    POLICE_STATION_NOT_FOUND: "Police station not found in our record.",
    DESIGNATION_NOT_FOUND: "Designation not found in our record.",
    POB_NOT_FOUND: "Protectee ordered by  not found in our record.",
    PSO_NOT_FOUND: "Protectee security ordered  not found in our record.",
    SAVE_SUCCESS: "Protectee security profile created successfully.",
    UPDATE_SUCCESS: "Protectee security profile updated successfully",
    INVALID_ID: "Protectee security record  not found in our record. ",
    REMOVE_PROFILE: "Protectee security record remove successfully."
}
const CourtMonitoringCurrentAffair = {
    CASE_TYPE_NOT_FOUND: "Case type not found in our record.",
    COURT_NOT_FOUND: "Court not found in our record.",
    PO_PARTY_NOT_FOUND: "Political party not found in our record.",
    SAVE_SUCCESS: "Court monitoring currentAffair profile created successfully.",
    UPDATE_SUCCESS: "Court monitoring currentAffair profile updated successfully",
    INVALID_ID: "Court monitoring currentAffair record  not found in our record. ",
    REMOVE_PROFILE: "Court monitoring currentAffair record remove successfully."
}
const OrganizationSocialAffair = {
    SAVE_SUCCESS: "Organization social affair profile created successfully.",
    UPDATE_SUCCESS: "Organization social affair profile updated successfully",
    INVALID_ID: "Organization social affair record  not found in our record. ",
    REMOVE_PROFILE: "Organization social affair record remove successfully.",
    ORGANIZATION_ADD: "Organization Social Affair Organization added successfully.",
    ORGANIZATION_REMOVE: "Organization Social Affair Organization remove successfully.",
    ORGANIZATION_EXIST: "You already added this Organization."
}
const OrganizationCurrentAffair = {
    SAVE_SUCCESS: "Organization current affair profile created successfully.",
    UPDATE_SUCCESS: "Organization current affair profile updated successfully",
    INVALID_ID: "Organization current affair record  not found in our record. ",
    REMOVE_PROFILE: "Organization current affair record remove successfully."
}


const TourPlan = {
    SAVE_FAIL: "Something went wrong with save tour plan.",
    SAVE_SUCCESS: "Tour plan has been created successfully.",
    TOUR_PLAN_UPDATE_FAIL: "Something went wrong with update tour plan.",
    TOUR_PLAN_UPDATE_SUCCESS: "Tour plan has been updated successfully.",
    PROTECTEE_SECURITY_NOT_FOUND: "Protectee security is not found.",
    DESIGNATION_NOT_FOUND: "Designation is not found.",
    TOUR_PLAN_ID_NOT_FOUND: "Tour plan record has not been found in our records.",
    TOUR_PLAN_DELETED: "Tour plan has been deleted successfully.",
    TOUR_PLAN_NOT_DELETED: "Something went wrong with delete tour plan.",
    TOUR_PLAN_NOT_FOUND: "Tour plan has not been found.",
};

const DisposalBdds = {
    SAVE_SUCCESS: "Disposal Bdds profile created successfully.",
    UPDATE_SUCCESS: "Disposal Bdds profile updated successfully",
    INVALID_ID: "Disposal Bdds record  not found in our record. ",
    REMOVE_PROFILE: "Disposal Bdds record remove successfully."
}
const PbiBdds = {
    SAVE_SUCCESS: "Pbi Bdds profile created successfully.",
    UPDATE_SUCCESS: "Pbi Bdds profile updated successfully",
    INVALID_ID: "Pbi Bdds record  not found in our record. ",
    REMOVE_PROFILE: "Pbi Bdds record remove successfully."
}
const EquipmentBdds = {
    SAVE_SUCCESS: "Equipment Bdds profile created successfully.",
    UPDATE_SUCCESS: "Equipment Bdds profile updated successfully",
    INVALID_ID: "Equipment Bdds record  not found in our record. ",
    REMOVE_PROFILE: "Equipment Bdds record remove successfully."
}
const TrainingBdds = {
    SAVE_SUCCESS: "Training Bdds profile created successfully.",
    UPDATE_SUCCESS: "Training Bdds profile updated successfully",
    INVALID_ID: "Training Bdds record  not found in our record. ",
    REMOVE_PROFILE: "Training Bdds record remove successfully."
}

const ThreatCallsBdds = {
    SAVE_SUCCESS: "Threat Calls Bdds profile created successfully.",
    UPDATE_SUCCESS: "Threat Calls Bdds profile updated successfully",
    INVALID_ID: "Threat Calls Bdds record  not found in our record. ",
    REMOVE_PROFILE: "Threat Calls Bdds record remove successfully."
}
const Transport = {
    ALREADY_EXIST: "Transport mode already exist",
    SAVE_SUCCESS: "Transport mode has been saved successfully!",
    UPDATE_SUCCESS: "Transport mode has been updated successfully!",
    DELETE_SUCCESS: "Transport mode status has been updated successfully!",

};
const Vehicle = {
    ALREADY_EXIST: "Vehicle type already exist",
    SAVE_SUCCESS: "Vehicle type has been saved successfully!",
    UPDATE_SUCCESS: "Vehicle type has been updated successfully!",
    DELETE_SUCCESS: "Vehicle type status has been updated successfully!",

};
const SecurityPersonal = {
    SAVE_SUCCESS: "Security Personal  profile created successfully.",
    UPDATE_SUCCESS: "Security Personal profile updated successfully",
    INVALID_ID: "Security Personal record  not found in our record. ",
    REMOVE_PROFILE: "Security Personal Bdds record remove successfully."
}
const OfficeBearer = {
    POLICE_STATION_NOT_FOUND: "Police station not found in our record.",
    DESIGNATION_NOT_FOUND: "Designation not found in our record.",
    POB_NOT_FOUND: "Office Bearer current affair ordered by  not found in our record.",
    PSO_NOT_FOUND: "Office Bearer current affair ordered  not found in our record.",
    SAVE_SUCCESS: "Office Bearer current affair profile created successfully.",
    UPDATE_SUCCESS: "Office Bearer current affair profile updated successfully",
    INVALID_ID: "Office Bearer current affair record  not found in our record. ",
    REMOVE_PROFILE: "Office Bearer current affair record remove successfully."
}
const MediaDebateImmc = {
    SAVE_SUCCESS: "Media Debate Immc profile created successfully.",
    UPDATE_SUCCESS: "Media Debate Immc profile updated successfully",
    INVALID_ID: "Media Debate Immc record  not found in our record. ",
    REMOVE_PROFILE: "Media Debate Immc Bdds record remove successfully."
}
const DossierPmmc = {
    SAVE_SUCCESS: "Dossier pmmc profile created successfully.",
    UPDATE_SUCCESS: "Dossier pmmc profile updated successfully",
    INVALID_ID: "Dossier pmmc record  not found in our record. ",
    REMOVE_PROFILE: "Dossier pmmc Bdds record remove successfully."
}
const Mockdrills = {
    SAVE_SUCCESS: "Mock drills bdds profile created successfully.",
    UPDATE_SUCCESS: "Mock drills bdds profile updated successfully",
    INVALID_ID: "Mock drills bdds record  not found in our record. ",
    REMOVE_PROFILE: "Mock drills bdds record remove successfully."
}
const AuditLog = {
    TYPE_NOT_FOUND: "Type is required.",
    PROFILE_NOT_FOUND: "Profile type is required.",
}

const TapalTracker = {
    ALREADY_EXIST: "Tapal tracker already exist",
    SAVE_SUCCESS: "Tapal tracker has been saved successfully!",
    UPDATE_SUCCESS: "Tapal tracker has been updated successfully!",
    DELETE_SUCCESS: "Tapal tracker status has been updated successfully!",
}
const VerificationCi = {
    SAVE_SUCCESS: "Verification Ci profile created successfully.",
    UPDATE_SUCCESS: "Verification Ci profile updated successfully",
    INVALID_ID: "Verification Ci record  not found in our record. ",
    REMOVE_PROFILE: "Verification Ci bdds record remove successfully."
}
const TapalTrackerSecurity = {
    SAVE_SUCCESS: "Tapal Tracker Security profile created successfully.",
    UPDATE_SUCCESS: "Tapal Tracker Security profile updated successfully",
    INVALID_ID: "Tapal Tracker Security record  not found in our record. ",
    REMOVE_PROFILE: "Tapal Tracker Security record remove successfully.",
    TAPAL_TRACKER_STATUS_ID: "Tapal Tracker Application Status id not found in our record.."
}
const LegalCellCi = {
    SAVE_SUCCESS: "Legal Cell Ci profile created successfully.",
    UPDATE_SUCCESS: "Legal Cell Ci profile updated successfully",
    INVALID_ID: "Legal Cell Ci record  not found in our record. ",
    REMOVE_PROFILE: "Legal Cell Ci bdds record remove successfully."
}
const DossierOrganizationCi = {
    SAVE_SUCCESS: "Dossier organization ci profile created successfully.",
    UPDATE_SUCCESS: "Dossier organization ci profile updated successfully",
    INVALID_ID: "Dossier organization ci record  not found in our record. ",
    REMOVE_PROFILE: "Dossier organization ci record remove successfully.",
    ORGANIZATION_ADD: "Dossier organization ci Organization added successfully.",
    ORGANIZATION_REMOVE: "Dossier organization ci Organization remove successfully.",
    ORGANIZATION_EXIST: "You already added this Organization."
}
const DossierPersonCi = {
    SAVE_SUCCESS: "Dossier Social Affair profile created successfully.",
    UPDATE_SUCCESS: "Dossier Social Affair profile updated successfully",
    INVALID_ID: "Dossier Social record  not found in our record. ",
    REMOVE_PROFILE: "Dossier Social record remove successfully.",
    LEADER_ADD: "Dossier Social Affair Leader added successfully.",
    LEADER_REMOVE: "Dossier Social Affair Leader remove successfully.",
    LEADER_EXIST: "You already added this leader."
}

const DepartmentMinistry = {
    ALREADY_EXIST: "Department ministry already exist",
    SAVE_SUCCESS: "Department ministry has been saved successfully!",
    UPDATE_SUCCESS: "Department ministry has been updated successfully!",
    DELETE_SUCCESS: "Department ministry status has been updated successfully!",

};
const JudgesSecurity = {
    SAVE_SUCCESS: "Judge Security profile created successfully.",
    UPDATE_SUCCESS: "Judge Security profile updated successfully",
    INVALID_ID: "Judge Security  not found in our record. ",
    REMOVE_PROFILE: "Judge Security remove successfully.",

}
module.exports = {
    Message,
    Role,
    District,
    State,
    Taluk,
    Place,
    Organization,
    OrganizationType,
    OrganizationUnitDesk,
    Leader,
    Jurisdiction,
    Division,
    Section,
    Desk,
    AdminnRole,

    Tag,
    Grade,
    ProfileType,
    AccusedAbscondingType,

    RequestMessageType,
    RequestMessageSubType,
    RecipientType,
    ForwardStatusType,

    RequestType,
    RequestStatusType,
    AttributeType,
    Language,

    User,

    EventMessageType,
    EventMessageSubType,
    EventType,
    EventStatusType,

    LanguageKey,
    DocumentType,
    DocumentUploadType,
    ReportType,
    Unit,
    UnitSectionDesk,
    RoleAccess,
    Event,
    Profile,
    Designation,
    UnitEventMessageType,
    Crop,
    Dam,
    Court,
    Prison,
    Person,
    Agency,
    PoliticalParty,
    ModuleSection,
    Person,
    PoliticalParty,
    ModuleSection,
    Request,
    EventRequest,
    Document,
    Periodicity,
    Report,
    Reportinr,

    profileCategory,
    profileSubCategory,
    DossierSocialAffair,
    ProtecteeSecurity,
    CourtMonitoringCurrentAffair,
    OrganizationSocialAffair,
    OrganizationCurrentAffair,

    TourPlan,
    Masters,

    DisposalBdds,
    PbiBdds,
    EquipmentBdds,
    TrainingBdds,
    ThreatCallsBdds,
    Transport,
    Vehicle,
    SecurityPersonal,
    OfficeBearer,
    SecurityPersonal,
    MediaDebateImmc,
    DossierPmmc,
    Mockdrills,
    AuditLog,
    VerificationCi,
    TapalTracker,
    VerificationCi,
    TapalTrackerSecurity,
    LegalCellCi,
    DossierOrganizationCi,
    DossierPersonCi,
    DepartmentMinistry,
    JudgesSecurity
}