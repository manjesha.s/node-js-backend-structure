const { body } = require('express-validator');
const { Validate } = require('../middlewares/validate.middleware');

const createCourtMonitoringCurrentAffairValidation = (req, res, next) => {
    return Validate([
        body("unit_id", "Unit cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("section_id", "Section cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("desk_id", "Desk cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("district_id", "District cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("taluk_id", "Taluk cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("profile_category_id", "Profile Category cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("profile_sub_category_id", "Profile Sub Category cannot be Empty").isInt().trim().exists().notEmpty(),
        body("status", "Status cannot be Empty").isString().trim().exists().notEmpty(),
    ])(req, res, next);
};
const updateCourtMonitoringCurrentAffairValidation = (req, res, next) => {
    return Validate([
        body("court_monitoring_current_affair_id", "Court monitoring current affair id cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("unit_id", "Unit cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("section_id", "Section cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("desk_id", "Desk cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("district_id", "District cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("taluk_id", "Taluk cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("profile_category_id", "Profile Category cannot be Empty").isInt().escape().trim().exists().notEmpty(),
        body("profile_sub_category_id", "Profile Sub Category cannot be Empty").isInt().trim().exists().notEmpty(),
        body("status", "Status cannot be Empty").isString().trim().exists().notEmpty(),
    ])(req, res, next);
};

module.exports = {
    createCourtMonitoringCurrentAffairValidation,
    updateCourtMonitoringCurrentAffairValidation
};