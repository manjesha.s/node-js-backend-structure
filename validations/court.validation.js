const { body } = require('express-validator');
const { Validate } = require('../middlewares/validate.middleware');

const createCourtValidation = (req, res, next) => {
    return Validate([
        body("court_name", "Court name can not be empty").isString().escape().trim().exists().notEmpty(),
        body("state_id", "State id can not be empty").isInt().escape().trim().exists().notEmpty(),
        body("district_id", "District id can not be empty").isInt().escape().trim().exists().notEmpty(),
        body("taluk_id", "Taluk id can not be empty").isInt().escape().trim().exists().notEmpty()
    ])(req, res, next);
};

const updateCourtValidation = (req, res, next) => {
    return Validate([
        body("court_id", "Court id can not be empty").isInt().escape().trim().exists().notEmpty(),
        body("court_name", "Court name can not be empty").isString().escape().trim().exists().notEmpty()
    ])(req, res, next);
};

module.exports = {
    createCourtValidation,
    updateCourtValidation
};