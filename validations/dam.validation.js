const { body } = require('express-validator');
const { Validate } = require('../middlewares/validate.middleware');

const createDamValidation = (req, res, next) => {
    return Validate([
        body("dam_name", "Dam name can not be empty").isString().escape().trim().exists().notEmpty(),
        body("state_id", "State id can not be empty").isInt().escape().trim().exists().notEmpty(),
        body("district_id", "District id can not be empty").isInt().escape().trim().exists().notEmpty(),
        body("taluk_id", "Taluk id can not be empty").isInt().escape().trim().exists().notEmpty(),
        body("place", "Place can not be empty").isString().escape().trim().exists().notEmpty(),
    ])(req, res, next);
};

const updateDamValidation = (req, res, next) => {
    return Validate([
        body("dam_id", "Dam id can not be empty").isInt().escape().trim().exists().notEmpty(),
        body("dam_name", "Dam name can not be empty").isString().escape().trim().exists().notEmpty()
    ])(req, res, next);
};

module.exports = {
    createDamValidation,
    updateDamValidation
};