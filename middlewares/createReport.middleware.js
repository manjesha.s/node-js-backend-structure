const { user } = require("../config/database.config");
let models = require("../models");
let commonResponse = require('../services/common-response.service');
let commonMessage = require('../services/common-api-message.service');
let commonHelper = require('../helpers/common.helper');

let Report = models.report;
let ReportAttachment = models.report_attachments;

module.exports.CreateReport = async (req, res, next) => {
    try {

        let body = req.body;

        //size validation
        if (body.report_name) {
            if (body.report_name.length > 500) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Reportinr.SUBJECT_LIMIT, null));
            }
        }

        // Check reference table id exists or not
        let checkUnit = await commonHelper.checkUnit(body.unit_id);
        if (checkUnit == false) {
            return res.json(await commonResponse.response(false, commonMessage.Request.UNIT_NOT_FOUND, null));
        }

        let checkSection = await commonHelper.checkSection(body.section_id);
        if (checkSection == false) {
            return res.json(await commonResponse.response(false, commonMessage.Request.SECTION_NOT_FOUND, null));
        }

        let checkDesk = await commonHelper.checkDesk(body.desk_id);
        if (checkDesk == false) {
            return res.json(await commonResponse.response(false, commonMessage.Request.DESK_NOT_FOUND, null));
        }

        let CheckPeriodicity = await commonHelper.CheckPeriodicity(body.periodicity_id);
        if (CheckPeriodicity == false) {
            return res.json(await commonResponse.response(false, commonMessage.Periodicity.NOT_EXIST, null));
        }

        let CheckReportType = await commonHelper.CheckReportType(body.report_type_id);
        if (CheckReportType == false) {
            return res.json(await commonResponse.response(false, commonMessage.Periodicity.REPORT_NOT_EXIST, null));
        }

        let reportObj = {
            report_name: body.report_name,
            periodicity_id: body.periodicity_id,
            report_type_id: body.report_type_id,
            unit_id: body.unit_id,
            section_id: body.section_id,
            desk_id: body.desk_id,
            requesterd_by: (body.requesterd_by) ? body.requesterd_by : null,
            order_id: (body.order_id) ? body.order_id : null,
            created_by: req.user_id,
            modified_by: req.user_id,
            is_active: "1"
        };

        let report = await Report.create(reportObj);

        if (report) {
            req.report_id = report.report_id;

            if (req.files.report_attachments && req.files.report_attachments.length > 0) {
                let report_attachments = req.files.report_attachments;
                let reportAttachmentArr = [];

                report_attachments.forEach(obj => {
                    reportAttachmentArr.push({
                        report_id: report.report_id,
                        attachment_name: obj.originalname,
                        attachment_extension: obj.originalname.split('.').pop(),
                        attachment_size: obj.size,
                        attachment_path: obj.location,
                        s3_key: obj.key,
                        created_by: req.user_id
                    });
                });

                // Store Report Attachments
                ReportAttachment.bulkCreate(reportAttachmentArr).then((attachment, err) => {
                    next();
                });
            } else {
                next();
            }
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Reportinr.REPORT_NOT_CREATED, null))
        }
    } catch (err) {
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Reportinr.REPORT_NOT_CREATED, null))
    }
};