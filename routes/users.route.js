const express = require('express');
const usersController = require('../controllers/users.controller');
const { checkAdminToken} = require('../middlewares/verifyAuth.middleware');
const router = express.Router();

// router.get('/', usersController.getUsers)

// router.get('/:id', usersController.getUserById)

router.post('/addUser', usersController.addUser)
router.post('/register', usersController.register)
router.post('/login', usersController.login)
router.post('/resend-otp', usersController.resendOtp)
router.post('/createUserPin', usersController.createUserPin)
router.post('/change-pin', usersController.createUserPin)
router.post('/validate-otp', usersController.verifyOtp)
router.post('/validate-pin', usersController.verifyPin)
router.post('/admin/login', usersController.adminLogin)
router.post('/admin/validate-otp', usersController.verifyAdminUserOtp)
router.post('/admin/validate-pin', usersController.verifyAdminUserPin)
router.post('/admin/change-pin', usersController.adminUserpin)
router.post('/admin/resend-otp', usersController.resendAdminUserOtp)
router.post('/admin/register', usersController.AdminRegister)
router.post('/admin/create-pin', usersController.adminUserpin)
router.post('/admin/activate/user',[checkAdminToken], usersController.activateUser)

module.exports = router;