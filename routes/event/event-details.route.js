const express = require('express');
let AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const awsConfig = require('../../config/aws.config');
const eventDetailsController = require('../../controllers/event/event-details.controllers');
const { createEventValidation, createEventDraftValidation, storeEventTagValidation, forwardEventValidation, updateEventSummaryValidation, updateEventUnitSectionDeskValidation } = require('../../validations/event.validation');
const { checkToken } = require('../../middlewares/verifyAuth.middleware');
const router = express.Router();

// AWS S3
const S3 = new AWS.S3({
    accessKeyId: awsConfig.key,
    secretAccessKey: awsConfig.accessKeyId,
    region: awsConfig.region
});

var upload = multer({
    storage: multerS3({
        s3: S3,
        bucket: awsConfig.bucket,
        acl: 'public-read',
        key: function (req, file, cb) {
            cb(null, `events/${file.originalname}`);
        },
        limits: { fileSize: '200MB' }
    })
});

router.get('/getUserList', [checkToken], eventDetailsController.getUserList);
router.post('/', [checkToken],
    upload.fields([{
        name: 'event_attachment',
        maxCount: 100
    }]), [createEventValidation],
    eventDetailsController.createEvent);

router.post('/draft', [checkToken],
    upload.fields([{
        name: 'event_attachment',
        maxCount: 100
    }]), [createEventDraftValidation],
    eventDetailsController.createEventDraft);

router.put('/draft', [checkToken],
    upload.fields([{
        name: 'event_attachment',
        maxCount: 100
    }]), [createEventDraftValidation],
    eventDetailsController.UpdateEventDraft);
router.post('/store/event/tags', [checkToken], [storeEventTagValidation], eventDetailsController.storeEventTags);
router.get('/getEventMetaDataTags', [checkToken], eventDetailsController.getEventMetaDataTags);

router.get('/get/pagination', [checkToken], eventDetailsController.getEventWithPagination);
router.get('/get/draft/pagination', [checkToken], eventDetailsController.getEventDraftWithPagination);

router.get('/get/draftby/:event_draft_id', [checkToken], eventDetailsController.getEventDraftById);

router.get('/get/:event_id', [checkToken], eventDetailsController.getEventById);
router.post('/forward', [checkToken], [upload.fields([{
    name: 'event_forward_attachments',
    maxCount: 100
}]), forwardEventValidation], eventDetailsController.forwardEvent);
router.post('/submit', multer().array(), [checkToken], eventDetailsController.submitEvent);
router.post('/summary/update', multer().array(), [checkToken], [updateEventSummaryValidation], eventDetailsController.updateEventSummary);

router.post('/addEventAttachment', [checkToken], upload.fields([{
    name: 'event_attachment',
    maxCount: 100
}]), eventDetailsController.addEventAttachment);

router.post('/updateOtherTags', multer().array(), [checkToken], eventDetailsController.updateOtherTags);
router.post('/updateEventsOtherTags', multer().array(), [checkToken], eventDetailsController.updateEventsOtherTags);
router.put('/updateEventStatus', multer().array(), [checkToken], eventDetailsController.updateEventStatus);
router.post('/unit/section/desk/update', multer().array(), [checkToken], [updateEventUnitSectionDeskValidation], eventDetailsController.updateEventUnitSectionDesk);

router.get('/print/:event_id', eventDetailsController.printPdf);
router.get('/get/inbox/pagination', [checkToken], eventDetailsController.getEventInboxWithPagination);
router.get('/get/sent/pagination', [checkToken], eventDetailsController.getEventSentWithPagination);
router.get('/get/archive/pagination', [checkToken], eventDetailsController.getEventArchiveWithPagination);
router.delete('/draft', [checkToken], multer().array(), eventDetailsController.deleteDraft);
router.delete('/attachment', [checkToken], multer().array(), eventDetailsController.deleteAttachment);
router.delete('/draft/attachment', [checkToken], multer().array(), eventDetailsController.deleteDraftAttachment);

router.get('/get/action/due/pagination', [checkToken], eventDetailsController.getEventActionDueWithPagination);
router.post('/update/tags', multer().array(), [checkToken], eventDetailsController.addEventTags);
router.post('/update/draft/tags', multer().array(), [checkToken], eventDetailsController.addEventDraftTags);
router.post('/get/filter/pagination', [checkToken], multer().array(), eventDetailsController.getEventWithFilter);

module.exports = router;