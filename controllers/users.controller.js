const dbQuery = require('../database/query.database');
let moment = require('moment');
let commonResponse = require('../services/common-response.service');
let commonMessage = require('../services/common-api-message.service');
const { TOTAL_LOGIN_ATTEMPTS } = require("../config/constant.config");
let { createUserLogs } = require('../helpers/audit_logs.helper');

let bcrypt = require("bcrypt")
let models = require("../models");
let { Op } = require("sequelize");
let sequelize = models.sequelize;
let jwt = require('jsonwebtoken');
let get_ip = require('ipware')().get_ip;

let Users = models.user;
let User_otp = models.user_otp;
let Admin_user = models.admin_user;
let Admin_user_otp = models.admin_user_otp;
let User_token = models.user_token;
let Admin_user_token = models.admin_user_token;
let User_district = models.user_district;
let User_taluk = models.user_taluk;
let User_unit = models.user_unit;
let User_section = models.user_section;
let User_desk = models.user_desk;


const user_jwt = require("../config/jwt.config");
const userotp = require("../services/msg91.service");
const user_otp = require('../models/user_otp');

const getUsers = async (req, res) => {
    const queryText = 'SELECT * FROM users ORDER BY name ASC';
    try {
        const { rows } = await dbQuery.query(queryText);
        if (rows.length != 0) {
            res.status(200).json({
                'success': true,
                'message': 'Data found',
                'data': rows
            });
        } else {
            res.status(200).json({
                'success': true,
                'message': 'No data found',
                'data': []
            });
        }
    } catch (error) {
        res.status(400).json({
            'success': false,
            'message': error,
            'data': {}
        });
    }
}

const getUserById = async (req, res) => {
    const id = parseInt(req.params.id);
    const queryText = 'SELECT * FROM users WHERE id = $1';
    try {
        const { rows } = await dbQuery.query(queryText, [id]);
        if (rows.length != 0) {
            res.status(200).json({
                'success': true,
                'message': 'Data found',
                'data': rows
            });
        } else {
            res.status(200).json({
                'success': true,
                'message': 'No data found',
                'data': []
            });
        }
    } catch (error) {
        res.status(400).json({
            'success': false,
            'message': error,
            'data': {}
        });
    }
}

const addUser = async (req, res) => {
    try {

        let objRole = req.body;
        if (!objRole.email) {
            return res.json(await commonResponse.response(true, commonMessage.User.REQUIRED_USER_EMAIL, null))
        }
        if (!objRole.phone) {
            return res.json(await commonResponse.response(true, commonMessage.User.REQUIRED_USER_PHONE, null))
        }
        let old_user = await Admin_user.findOne({
            where: {
                email: objRole.email,
                phone: objRole.phone,
                is_active: true

            }
        });
        let hashedPassword = await bcrypt.hash('perp@123', 10);
        objRole.full_name = "perp Admin";
        objRole.password = hashedPassword;
        objRole.profile_image_path = "/";
        objRole.created_by = 0;
        objRole.modified_by = 0;
        objRole.is_active = objRole.is_active ? objRole.is_active : "1";
        if (!old_user) {
            let create_user = await Admin_user.create(objRole);
            return res.json(await commonResponse.response(true, commonMessage.User.SAVE_SUCCESS, create_user))
        } else {
            return res.json(await commonResponse.response(false, commonMessage.User.ALREADY_EXIST, null))
        }
    } catch (err) {
        console.error(
            "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
            err.message
        );
        let url =
            "Location of Error : " +
            req.originalUrl +
            "  Method : " +
            req.method +
            "  Request Body : " +
            JSON.stringify(req.body);

        return res.json(await commonResponse.response(false, err.message, null));
    }
}

const updateUser = (req, res) => {
    res.send('updateUser')
}

const deleteUser = (req, res) => {
    res.send('deleteUser')
}

const register = async (req, res) => {
    try {

        let objRole = req.body;
        let randomOtp = Math.floor(100000 + Math.random() * 900000);
        let Userotp = randomOtp.toString();
        let hashedPassword = await bcrypt.hash(Userotp, 10);
        if (!objRole.user_phone) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.REQUIRED_USER_PHONE, null))
        }
        let admin_role = await Users.findOne({
            where: {
                user_phone: objRole.user_phone,
                is_active: true
            }
        });

        if (admin_role) {
            if (admin_role && admin_role.user_pin == null) {
                await userotp.sendOtp(objRole.user_phone, randomOtp)
                let token = jwt.sign({ user_id: admin_role.user_id }, user_jwt.jwt_constant, {
                    expiresIn: user_jwt.jwt_expire,
                });
                res.setHeader('Access-Token', token);
                let saveData = {
                    "user_id": admin_role.user_id,
                    "otp": hashedPassword,
                }
                await User_otp.create(saveData);
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT, {}))
            } else {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.MOBILE_ALREADY_REG, null))
            }
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.MOBILE_NOT_MAPPED, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }
}
const login = async (req, res) => {
    try {

        let objRole = req.body;
        let randomOtp = Math.floor(100000 + Math.random() * 900000);
        let Userotp = randomOtp.toString();
        let hashedPassword = await bcrypt.hash(Userotp, 10);
        if (!objRole.user_phone) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.REQUIRED_USER_PHONE, null))
        }
        let admin_role = await Users.findOne({
            where: {
                user_phone: objRole.user_phone,
                is_active: true
            }
        });

        if (admin_role) {
            if (admin_role && admin_role.user_pin == null) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.PIN_NOT_SET, null))
            } else {
                let verifyToken = await User_token.findOne({
                    where: { user_id: admin_role.user_id },
                    order: [
                        ['user_token_id', 'DESC']
                    ],
                })
                if (verifyToken == null) {
                    await userotp.sendOtp(objRole.user_phone, randomOtp)
                    let token = jwt.sign({ user_id: admin_role.user_id }, user_jwt.jwt_constant, {
                        expiresIn: user_jwt.jwt_expire,
                    });
                    res.setHeader('Access-Token', token);

                    await User_otp.create({
                        user_id: admin_role.user_id,
                        otp: hashedPassword,
                    });
                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT, { "sessionExpired": true, "user_id": '' }))
                } else {
                    jwt.verify(verifyToken.token, user_jwt.jwt_constant, async function (err, decoded) {
                        if (err) {
                            await userotp.sendOtp(objRole.user_phone, randomOtp)
                            let token = jwt.sign({ user_id: admin_role.user_id }, user_jwt.jwt_constant, {
                                expiresIn: user_jwt.jwt_expire,
                            });
                            res.setHeader('Access-Token', token);
                            await User_otp.create({
                                user_id: admin_role.user_id,
                                otp: hashedPassword,
                            });
                            return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT, { "sessionExpired": true, "user_id": '' }))
                        } else {

                            // Store activity log
                            let ip_info = get_ip(req);
                            let ip_address;

                            if (ip_info && ip_info.clientIp) {
                                ip_address = ip_info.clientIp;
                            } else {
                                ip_address = "127.0.0.1";
                            }

                            await createUserLogs({
                                ip_address,
                                created_by: admin_role.user_id
                            }, "Login");

                            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.ACTIVE, { "sessionExpired": false, "user_id": admin_role.user_id }))
                        }
                    });
                }
            }
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.MOBILE_NOT_MAPPED, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }
}
const createUserPin = async (req, res) => {
    try {

        let objRole = req.body;
        let admin_role = await Users.findOne({
            where: {
                user_id: objRole.user_id,
                is_active: true
            }
        });

        if (admin_role) {
            let hashedPassword = await bcrypt.hash(objRole.user_pin, 10);
            let saveData = {
                "user_pin": hashedPassword
            }
            await Users.update(saveData, {
                where: {
                    user_id: objRole.user_id,
                }
            });

            // Store activity log
            let ip_info = get_ip(req);
            let ip_address;

            if (ip_info && ip_info.clientIp) {
                ip_address = ip_info.clientIp;
            } else {
                ip_address = "127.0.0.1";
            }

            await createUserLogs({
                ip_address,
                created_by: objRole.user_id
            }, "Reset PIN");

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.USER_PIN_SUCCESS, {}))
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.NO_DATA_FOUND, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }
}

const verifyOtp = async (req, res, next) => {
    try {
        let objRole = req.body;
        let admin_role = await Users.findOne({
            include: [{
                model: User_district,
                attributes: ['district_id'],
            },
            {
                model: User_taluk,
                attributes: ['taluk_id'],
            }
            ],
            where: {
                user_phone: objRole.user_phone,
                is_active: true
            }
        });

        let destricID = admin_role.user_districts[0] ? admin_role.user_districts[0].district_id : null
        let talukID = admin_role.user_taluks[0] ? admin_role.user_taluks[0].taluk_id : null

        if (admin_role) {
            let userOtp = await User_otp.findOne({
                where: {
                    user_id: admin_role.user_id
                },
                order: [
                    ['user_otp_id', 'DESC']
                ],
            });
            bcrypt.compare(objRole.otp, userOtp.otp, async function (err, resultPassword) {
                if (err) {
                    if (admin_role.failed_login_count == TOTAL_LOGIN_ATTEMPTS) {

                        //blocking user
                        await Users.update({
                            is_active: "0",
                        }, {
                            where: {
                                user_id: admin_role.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.TO_MANY_ATTEMPTS, {}))
                    } else {

                        //updating fail login count
                        let count = TOTAL_LOGIN_ATTEMPTS - admin_role.failed_login_count;
                        let saveData = {
                            "failed_login_count": admin_role.failed_login_count + 1,
                        }
                        await Users.update(saveData, {
                            where: {
                                user_id: admin_role.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_OTP, { "Attemps_`left": count }))

                    }
                } else if (resultPassword) {
                    let saveData = {
                        "failed_login_count": 1,
                    }
                    await Users.update(saveData, {
                        where: {
                            user_id: admin_role.user_id,
                        }
                    });
                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.USER_OTP_VERIFY, {
                        "user_id": admin_role.user_id,
                        "state_id": admin_role.state_id,
                        "district_id": destricID,
                        "taluk_id": talukID,
                    }))
                } else {
                    if (admin_role.failed_login_count == TOTAL_LOGIN_ATTEMPTS) {
                        //blocking user
                        await Users.update({
                            is_active: "0",
                        }, {
                            where: {
                                user_id: admin_role.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.TO_MANY_ATTEMPTS, {}))
                    } else {

                        //updating fail login count
                        let count = TOTAL_LOGIN_ATTEMPTS - admin_role.failed_login_count;
                        let saveData = {
                            "failed_login_count": admin_role.failed_login_count + 1,
                        }
                        await Users.update(saveData, {
                            where: {
                                user_id: admin_role.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_OTP, { "Attemps_left": count }))
                    }
                }
            });
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.MOBILE_NOT_ALREADY_REG, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }


}
const verifyPin = async (req, res) => {
    try {

        let objRole = req.body;
        let orderBy = [];
        let whereClause = {
            is_default: true
        }
        orderBy.push([User_district, 'user_district_id', 'ASC']);
        orderBy.push([User_taluk, 'user_taluk_id', 'ASC']);
        // orderBy.push([User_unit, 'user_unit_id', 'ASC']);
        // orderBy.push([User_section, 'user_section_id', 'ASC']);
        // orderBy.push([User_desk, 'user_desk_id', 'ASC']);

        let admin_role = await Users.findOne({
            distinct: true,
            order: orderBy,
            include: [{
                model: User_district,
                attributes: ['district_id'],
            },
            {
                model: User_taluk,
                attributes: ['taluk_id'],
            },
            {
                model: User_unit,
                where: whereClause,
                attributes: ['unit_id'],
            },
            {
                model: User_section,
                where: whereClause,
                attributes: ['section_id'],
            },
            {
                model: User_desk,
                where: whereClause,
                attributes: ['desk_id'],
            },
            ],
            where: {
                user_id: objRole.user_id,
            }
        });

        //checking user exist or not
        if (admin_role && admin_role.is_active == true) {

            //user details
            let destricID = admin_role.user_districts[0] ? admin_role.user_districts[0].district_id : null
            let talukID = admin_role.user_taluks[0] ? admin_role.user_taluks[0].taluk_id : null
            let unitID = admin_role.user_units[0] ? admin_role.user_units[0].unit_id : null
            let sectionID = admin_role.user_sections[0] ? admin_role.user_sections[0].section_id : null
            let deskID = admin_role.user_desks[0] ? admin_role.user_desks[0].desk_id : null

            //checking user_pin
            bcrypt.compare(objRole.user_pin, admin_role.user_pin, async function (err, resultPassword) {
                if (err) {
                    if (admin_role.failed_login_count == TOTAL_LOGIN_ATTEMPTS) {

                        //blocking user
                        await Users.update({
                            is_active: "0",
                        }, {
                            where: {
                                user_id: objRole.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.TO_MANY_ATTEMPTS, {}))
                    } else {

                        //updating fail login count
                        let count = TOTAL_LOGIN_ATTEMPTS - admin_role.failed_login_count;
                        let saveData = {
                            "failed_login_count": admin_role.failed_login_count + 1,
                        }
                        await Users.update(saveData, {
                            where: {
                                user_id: objRole.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_PIN, { "Attemps_`left": count }))

                    }

                } else if (resultPassword) {

                    //login success
                    let token = jwt.sign({ user_id: objRole.user_id }, user_jwt.jwt_constant, {
                        expiresIn: user_jwt.jwt_expire,
                    });
                    res.setHeader('Access-Token', token);
                    if (token) {
                        await User_token.create({
                            user_id: objRole.user_id,
                            token: token
                        })
                    }
                    let saveData = {
                        "failed_login_count": 1,
                    }
                    await Users.update(saveData, {
                        where: {
                            user_id: objRole.user_id,
                        }
                    });

                    // Store activity log
                    let ip_info = get_ip(req);
                    let ip_address;

                    if (ip_info && ip_info.clientIp) {
                        ip_address = ip_info.clientIp;
                    } else {
                        ip_address = "127.0.0.1";
                    }

                    await createUserLogs({
                        ip_address,
                        created_by: objRole.user_id
                    }, "Login");

                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.USER_PIN_VERIFY, {
                        "user_id": objRole.user_id,
                        "state_id": admin_role.state_id,
                        "district_id": destricID,
                        "taluk_id": talukID,
                        "unit_id": unitID,
                        "section_id": sectionID,
                        "desk_id": deskID,
                        "token": token
                    }))
                } else {
                    if (admin_role.failed_login_count == TOTAL_LOGIN_ATTEMPTS) {
                        //blocking user
                        await Users.update({
                            is_active: "0",
                        }, {
                            where: {
                                user_id: objRole.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.TO_MANY_ATTEMPTS, {}))
                    } else {

                        //updating fail login count
                        let count = TOTAL_LOGIN_ATTEMPTS - admin_role.failed_login_count;
                        let saveData = {
                            "failed_login_count": admin_role.failed_login_count + 1,
                        }
                        await Users.update(saveData, {
                            where: {
                                user_id: objRole.user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_PIN, { "Attemps_left": count }))
                    }
                }
            });
        } else if (admin_role && admin_role.is_active == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.TO_MANY_ATTEMPTS, {}))
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.NO_DATA_FOUND, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }


}

const resendOtp = async (req, res) => {
    try {

        let objRole = req.body;
        let randomOtp = Math.floor(Math.random() * 900000) + 10000;
        let Userotp = randomOtp.toString();
        let hashedPassword = await bcrypt.hash(Userotp, 10);
        if (!objRole.user_phone) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.REQUIRED_USER_PHONE, null))
        }
        let admin_role = await Users.findOne({
            where: {
                user_phone: objRole.user_phone,
                is_active: true
            }
        });

        if (admin_role) {
            let current_date_time = moment().format('YYYY-MM-DD ');

            await userotp.sendOtp(objRole.user_phone, randomOtp)
            let saveData = {
                user_id: admin_role.user_id,
                otp: hashedPassword,
                created_date: current_date_time
            }
            await User_otp.create(saveData);

            // Store activity log
            let ip_info = get_ip(req);
            let ip_address;

            if (ip_info && ip_info.clientIp) {
                ip_address = ip_info.clientIp;
            } else {
                ip_address = "127.0.0.1";
            }

            await createUserLogs({
                ip_address,
                created_by: admin_role.user_id
            }, "Forgot PIN");

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT))
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.NO_DATA_FOUND, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }
}

const adminLogin = async (req, res) => {
    try {

        let objRole = req.body;
        let randomOtp = Math.floor(100000 + Math.random() * 900000);
        let Userotp = randomOtp.toString();
        let hashedPassword = await bcrypt.hash(Userotp, 10);
        if (!objRole.phone) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.REQUIRED_USER_PHONE, null))
        }
        let admin_role = await Admin_user.findOne({
            where: {
                phone: objRole.phone,
                is_active: true
            }
        });
        if (admin_role) {
            if (admin_role && admin_role.password == null) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.PIN_NOT_SET, { "sessionExpired": true, "admin_user_id": '' }))
            } else {
                let verifyToken = await Admin_user_token.findOne({
                    where: { admin_user_id: admin_role.admin_user_id },
                    order: [
                        ['admin_user_token_id', 'DESC']
                    ],
                })
                if (verifyToken == null) {
                    await userotp.sendOtp(objRole.phone, randomOtp)
                    let token = jwt.sign({ admin_user_id: admin_role.admin_user_id }, user_jwt.jwt_constant, {
                        expiresIn: user_jwt.jwt_expire,
                    });
                    res.setHeader('Access-Token', token);
                    await Admin_user_otp.create({
                        admin_user_id: admin_role.admin_user_id,
                        otp: hashedPassword,
                    });

                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT, { "sessionExpired": true, "admin_user_id": '' }))
                }
                else {
                    jwt.verify(verifyToken.token, user_jwt.jwt_constant, async function (err, decoded) {
                        if (err) {
                            await userotp.sendOtp(objRole.phone, randomOtp)
                            let token = jwt.sign({ admin_user_id: admin_role.admin_user_id }, user_jwt.jwt_constant, {
                                expiresIn: user_jwt.jwt_expire,
                            });
                            res.setHeader('Access-Token', token);
                            let saveData = {
                                "admin_user_id": admin_role.admin_user_id,
                                "otp": hashedPassword,
                            }
                            await Admin_user_otp.create(saveData);
                            return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT, { "sessionExpired": true, "admin_user_id": '' }))

                        } else {
                            await createUserLogs({
                                user_id: admin_role.admin_user_id,
                                created_by: admin_role.admin_user_id
                            }, "Logged In");

                            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.ACTIVE, { "sessionExpired": false, "admin_user_id": admin_role.admin_user_id }))
                        }
                    });
                }
            }
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.MOBILE_NOT_MAPPED, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }
}

const verifyAdminUserOtp = async (req, res) => {
    try {

        let objRole = req.body;
        let admin_role = await Admin_user.findOne({
            where: {
                phone: objRole.phone,
                is_active: true
            }
        });

        if (admin_role) {
            let userOtp = await Admin_user_otp.findOne({
                where: {
                    admin_user_id: admin_role.admin_user_id
                },
                order: [
                    ['admin_user_otp_id', 'DESC']
                ],
            });
            bcrypt.compare(objRole.otp, userOtp.otp, async function (err, resultPassword) {
                if (err) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_OTP, null))
                } else if (resultPassword) {
                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.USER_OTP_VERIFY, { "admin_user_id": admin_role.admin_user_id }))
                } else {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_OTP, null))
                }
            });
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.NO_DATA_FOUND, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }


}
const verifyAdminUserPin = async (req, res) => {
    try {

        let objRole = req.body;
        let admin_role = await Admin_user.findOne({
            where: {
                admin_user_id: objRole.admin_user_id,
                is_active: true
            }
        });

        if (admin_role) {
            bcrypt.compare(objRole.password, admin_role.password, async function (err, resultPassword) {
                if (err) {
                    if (admin_role.failed_login_count == TOTAL_LOGIN_ATTEMPTS) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.TO_MANY_ATTEMPTS, {}))
                    } else {
                        let count = TOTAL_LOGIN_ATTEMPTS - admin_role.failed_login_count;
                        let saveData = {
                            "failed_login_count": admin_role.failed_login_count + 1,
                        }
                        await Admin_user.update(saveData, {
                            where: {
                                admin_user_id: objRole.admin_user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_PIN, { "Attemps_`left": count }))

                    }

                } else if (resultPassword) {
                    let token = jwt.sign({ admin_user_id: objRole.admin_user_id }, user_jwt.jwt_constant, {
                        expiresIn: user_jwt.jwt_expire,
                    });
                    res.setHeader('Access-Token', token);
                    if (token) {
                        await Admin_user_token.create({
                            admin_user_id: objRole.admin_user_id,
                            token: token
                        })
                    }
                    let saveData = {
                        "failed_login_count": 1,
                    }
                    await Admin_user.update(saveData, {
                        where: {
                            admin_user_id: objRole.admin_user_id,
                        }
                    });
                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.USER_PIN_VERIFY, { "admin_user_id": objRole.admin_user_id, "token": token }))
                } else {
                    if (admin_role.failed_login_count == TOTAL_LOGIN_ATTEMPTS) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.TO_MANY_ATTEMPTS, {}))
                    } else {
                        let count = TOTAL_LOGIN_ATTEMPTS - admin_role.failed_login_count;
                        let saveData = {
                            "failed_login_count": admin_role.failed_login_count + 1,
                        }
                        await Admin_user.update(saveData, {
                            where: {
                                admin_user_id: objRole.admin_user_id,
                            }
                        });
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_PIN, { "Attemps_left": count }))
                    }
                }
            });
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.NO_DATA_FOUND, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }


}
const adminUserpin = async (req, res) => {
    try {

        let objRole = req.body;
        let admin_role = await Admin_user.findOne({
            where: {
                admin_user_id: objRole.admin_user_id,
                is_active: true
            }
        });

        if (admin_role) {
            let hashedPassword = await bcrypt.hash(objRole.password, 10);
            let saveData = {
                "password": hashedPassword
            }
            await Admin_user.update(saveData, {
                where: {
                    admin_user_id: objRole.admin_user_id,
                }
            });
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.USER_PIN_SUCCESS, {}))
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.NO_DATA_FOUND, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }


}
const resendAdminUserOtp = async (req, res) => {
    try {

        let objRole = req.body;
        let randomOtp = Math.floor(Math.random() * 900000) + 10000;
        let Userotp = randomOtp.toString();
        let hashedPassword = await bcrypt.hash(Userotp, 10);
        if (!objRole.phone) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.REQUIRED_USER_PHONE, null))
        }
        let admin_role = await Admin_user.findOne({
            where: {
                phone: objRole.phone,
                is_active: true
            }
        });

        if (admin_role) {
            await userotp.sendOtp(objRole.phone, randomOtp)
            let saveData = {
                "admin_user_id": admin_role.admin_user_id,
                "otp": hashedPassword,
            }
            await Admin_user_otp.create(saveData);

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT,))
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.NO_DATA_FOUND, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }


}
const AdminRegister = async (req, res) => {
    try {

        let objRole = req.body;
        let randomOtp = Math.floor(100000 + Math.random() * 900000);
        let Userotp = randomOtp.toString();
        let hashedPassword = await bcrypt.hash(Userotp, 10);
        if (!objRole.phone) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.REQUIRED_USER_PHONE, null))
        }
        let admin_role = await Admin_user.findOne({
            where: {
                phone: objRole.phone,
                is_active: true
            }
        });

        if (admin_role) {
            if (admin_role && admin_role.password == null) {
                await userotp.sendOtp(objRole.phone, randomOtp)
                let token = jwt.sign({ user_id: admin_role.user_id }, user_jwt.jwt_constant, {
                    expiresIn: user_jwt.jwt_expire,
                });
                res.setHeader('Access-Token', token);
                await Admin_user_otp.create({
                    admin_user_id: admin_role.admin_user_id,
                    otp: hashedPassword,
                });
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.OTP_SENT, {}))
            } else {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.MOBILE_ALREADY_REG, null))
            }
        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.MOBILE_NOT_MAPPED, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }
}
const activateUser = async (req, res) => {
    try {

        let objRole = req.body;
        //check user
        let admin_role = await Admin_user.findOne({
            where: {
                admin_user_id: req.admin_user_id,
                is_active: true
            }
        });

        if (admin_role) {
            //activate user
            await Users.update({
                is_active: objRole.status,
                failed_login_count: 1,
                modified_date: moment().format('YYYY-MM-DD hh:mm:ss'),
                modified_by: req.admin_user_id,
            }, {
                where: {
                    user_id: objRole.user_id,
                }
            });
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.User.DELETE_SUCCESS, {}))

        } else {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.User.USER_NOT_EXISTS, null))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null))
    }

}

module.exports = {
    getUsers,
    getUserById,
    addUser,
    updateUser,
    deleteUser,
    register,
    createUserPin,
    verifyOtp,
    verifyPin,
    resendOtp,
    adminLogin,
    verifyAdminUserOtp,
    verifyAdminUserPin,
    adminUserpin,
    resendAdminUserOtp,
    login,
    AdminRegister,
    activateUser
};