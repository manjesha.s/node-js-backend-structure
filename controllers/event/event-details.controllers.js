let models = require("../../models");
let { Op } = require("sequelize");
let moment = require('moment');
var htmlPdf = require('html-pdf');
const Promise = require('bluebird'),
    ejs = Promise.promisifyAll(require('ejs')),
    path = require('path');
let u = require('underscore');

let commonResponse = require('../../services/common-response.service');
let commonMessage = require('../../services/common-api-message.service');
const commonHelper = require('../../helpers/common.helper');
const { createEventLogs } = require('../../helpers/audit_logs.helper');
const s3 = require("../../services/delete-s3-image.service");
let EventTagJson = require('../../services/event-tag-data.json');

let sequelize = models.sequelize;
let User = models.user;
let UserSection = models.user_section;
let UserDesk = models.user_desk;
let UserUnit = models.user_unit;
let Event = models.event;
let Users = models.user;
let RecipientType = models.recipient_types;
let EventAttachment = models.event_attachment;
let Unit = models.units;
let Desk = models.desk;
let Section = models.section;
let EventMessageType = models.event_message_types;

let EventTag = models.event_tags;

let EventType = models.event_types;
let EventStatusType = models.event_status_types;
let EventMessageSubType = models.event_message_sub_types;

let Grade = models.grade;

let EventForward = models.event_forwards;
let EventForwardRecipient = models.event_forward_recipients;
let EventForwardAttachment = models.event_forward_attachments;
let FordStatusType = models.forward_status_types;


let EventDraft = models.event_draft;
let EventDraftAttachment = models.event_draft_attachment;
let EventDraftTag = models.event_draft_tag;
let Designation = models.designation;
let EventDraftRecipient = models.event_draft_recipient;
let EventRecipient = models.event_recipient;
let Tag = models.tag;

const getUserList = async (req, res) => {
    User.belongsTo(models.user_section, {
        foreignKey: 'user_id',
        targetKey: 'user_id',
        onDelete: 'CASCADE'
    })
    User.belongsTo(models.user_desk, {
        foreignKey: 'user_id',
        targetKey: 'user_id',
        onDelete: 'CASCADE'
    })
    User.belongsTo(models.user_unit, {
        foreignKey: 'user_id',
        targetKey: 'user_id',
        onDelete: 'CASCADE'
    })
    try {
        let users = await User.findAll({
            where: {
                is_active: true
            },
            attributes: ['user_firstname', 'user_lastname', 'user_email', 'user_id', 'designation_id'],
            include: [{
                model: Designation,
                as: 'designation',
                attributes: ['designation_name'],
            }]
            // include: [{
            //         model: UserSection,
            //         attributes: ['user_id', 'section_id'],
            //         where: {
            //             section_id: req.query.section_id
            //         },
            //     },
            //     {
            //         model: UserDesk,
            //         attributes: ['user_id', 'desk_id'],
            //         where: {
            //             desk_id: req.query.desk_id
            //         },
            //     },
            //     {
            //         model: UserUnit,
            //         attributes: ['user_id', 'unit_id'],
            //         where: {
            //             unit_id: req.query.unit_id
            //         },
            //     },
            //     {
            //         model: Designation,
            //         as: 'designation',
            //         attributes: ['designation_name'],

            //     }
            // ]
        })
        users = JSON.parse(JSON.stringify(users));
        for (let index = 0; index < users.length; index++) {
            const element = users[index];
            element.designation_name = "";
            if (element.designation) {
                element.designation_name = element.designation.designation_name;
            }
            delete element.designation;

        }

        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, users))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }
        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

const createEvent = async (req, res) => {
    try {
        let objEvent = req.body;
        //size validation
        if (objEvent.source_of_message) {
            if (objEvent.source_of_message.length > 200) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SOURCE_OF_MESSAGE_LIMIT, null));
            }
        }
        if (objEvent.subject) {
            if (objEvent.subject.length > 200) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SUBJECT_LIMIT, null));
            }
        }

        let event_attachment = [];

        if (req.files && req.files.event_attachment && req.files.event_attachment.length) {
            event_attachment = req.files && req.files.event_attachment && req.files.event_attachment.length ? req.files.event_attachment : [];
        }
        try {
            objEvent.event_type_id = await EventType.findOne({ where: { is_default: true } });
            objEvent.event_type_id = objEvent.event_type_id ? objEvent.event_type_id.event_type_id : null;
        } catch (e) { }
        try {
            objEvent.event_status_type_id = await EventStatusType.findOne({ where: { is_default: true } });
            objEvent.event_status_type_id = objEvent.event_status_type_id ? objEvent.event_status_type_id.event_status_type_id : null;
        } catch (e) { }
        if (!objEvent.event_type_id) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_TYPE_NOT_EXISTS, null));
        }
        if (!objEvent.event_status_type_id) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_STATUS_NOT_EXISTS, null));
        }

        let checkUnit = await commonHelper.checkUnit(objEvent.unit_id);
        if (checkUnit == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UNIT_NOT_FOUND, null));
        }

        let checkSection = await commonHelper.checkSection(objEvent.section_id);
        if (checkSection == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_NOT_FOUND, null));
        }

        let checkDesk = await commonHelper.checkDesk(objEvent.desk_id);
        if (checkDesk == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DESK_NOT_FOUND, null));
        }

        let checkGrade = await commonHelper.checkGrade(objEvent.grade_id);
        if (checkGrade == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.GRADE_NOT_FOUND, null));
        }

        let checkEventMessageType = await commonHelper.checkEventMessageType(objEvent.event_message_type_id);
        if (checkEventMessageType == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_TYPE_NOT_FOUND, null));
        }
        let checkEventMessageSubType = await commonHelper.checkEventMessageSubType(objEvent.event_message_sub_type_id);
        if (checkEventMessageSubType == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_SUB_TYPE_NOT_FOUND, null));
        }
        if (objEvent.event_recipient) {
            objEvent.event_recipient = JSON.parse(objEvent.event_recipient);
            for (let index = 0; index < objEvent.event_recipient.length; index++) {
                const element = objEvent.event_recipient[index];
                let checkRecipientType = await commonHelper.checkRecipientType(element.recipient_type_id);
                if (checkRecipientType == false) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RECIPIENT_TYPE_NOT_FOUND, null));
                    break;
                }
                let checkUser = await commonHelper.checkUser(element.user_id);
                if (checkUser == false) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.USER_NOT_FOUND, null));
                    break;
                }
            }
        } else {
            objEvent.event_recipient = [];
        }
        getEventTagsUsingParam(objEvent.unit_id, objEvent.section_id, objEvent.desk_id, objEvent.event_message_type_id).then(async function (result) {
            if (objEvent.event_tags) {
                try {
                    objEvent.event_tags = JSON.parse(objEvent.event_tags);

                    let requiredTags = u.where(result, { is_required: "1" });
                    console.log(requiredTags)
                    for (let index = 0; index < requiredTags.length; index++) {
                        const obj = requiredTags[index];
                        let column_name = obj['column_name'];
                        if (column_name) {
                            if (column_name == 'organization_id' || column_name == 'leader_id') {
                                if (!objEvent.event_tags[column_name] || objEvent.event_tags[column_name].length == 0) {
                                    let msg = capitalizeFirstLetter(column_name.replace('_', ' ').replace('id', '').trim() + " cannot be Empty")
                                    return res.json(await commonResponse.responseReturn(false, req, msg, null));
                                    break;
                                }
                            } else {
                                if (!objEvent.event_tags[column_name]) {
                                    let msg = capitalizeFirstLetter(column_name.replace('_', ' ').replace('id', '').trim() + " cannot be Empty")
                                    return res.json(await commonResponse.responseReturn(false, req, msg, null));
                                    break;
                                }
                            }
                        }
                    }

                    // if (!objEvent.event_tags.state_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.STATE_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.district_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.DISTRICT_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.taluk_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.TALUK_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.organization_id || !objEvent.event_tags.organization_id.length) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.ORGANIZATION_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.leader_id || !objEvent.event_tags.leader_id.length) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.LEADER_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.crop_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.CROP_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.dam_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.DAM_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.court_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.COURT_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.political_party_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.POLITICAL_PARTY_EMPTY, null));
                    // }
                    // if (!objEvent.event_tags.person_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.PERSON_EMPTY, null));
                    // }

                    // if (!objEvent.event_tags.agency_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.AGENCY_EMPTY, null));
                    // }
                    if (objEvent.event_tags.district_id) {
                        let checkDistrict = await commonHelper.checkDistrict(objEvent.event_tags.district_id);
                        if (checkDistrict == false) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DISTRICT_NOT_FOUND, null));
                        }
                    }
                    if (objEvent.event_tags.farmer_name) {
                        if (objEvent.event_tags.farmer_name.length > 100) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.FARMER_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.reservoir_level) {
                        if (objEvent.event_tags.reservoir_level.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RESERVOIR_LEVEL_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.gross_capacity) {
                        if (objEvent.event_tags.gross_capacity.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.GROSS_CAPACITY_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.live_capacity) {
                        if (objEvent.event_tags.live_capacity.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.LIVE_CAPACITY_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.in_flow) {
                        if (objEvent.event_tags.in_flow.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.IN_FLOW_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.out_flow) {
                        if (objEvent.event_tags.out_flow.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OUT_FLOW_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.water_release_to_canal) {
                        if (objEvent.event_tags.water_release_to_canal.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.WATER_RELEASE_TO_CANAL_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.rain_fall) {
                        if (objEvent.event_tags.rain_fall.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RAIN_FALL_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.cc_sc) {
                        if (objEvent.event_tags.cc_sc.length > 100) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CC_SC_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.case_type) {
                        if (objEvent.event_tags.case_type.length > 100) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CASE_TYPE_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.visitors) {
                        if (objEvent.event_tags.visitors.length > 200) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VISITORS_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.relation) {
                        if (objEvent.event_tags.relation.length > 100) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RELATION_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.visitor_mobile_number) {
                        if (objEvent.event_tags.visitor_mobile_number.length > 50) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VISITOR_MOBILE_NUMBER_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.death) {
                        if (objEvent.event_tags.death.length > 20) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DEATH_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.injury) {
                        if (objEvent.event_tags.injury.length > 20) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.INJURY_LIMIT, null));
                        }
                    }
                    if (objEvent.event_tags.call_received_from) {
                        if (objEvent.event_tags.call_received_from.length > 100) {
                            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CALL_RECEIVED_FROM_LIMIT, null));
                        }
                    }
                } catch (e) {
                    console.log(e)
                    return res.json(await commonResponse.responseReturn(false, req, e.message, null));
                }
            } else {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.TAG_NOT_FOUND, null));
            }
            if (objEvent.event_fir) {
                objEvent.event_fir = JSON.parse(objEvent.event_fir);
                if (!objEvent.event_fir.police_station_id) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.POLICE_STATION_ID_NOT_FOUND, null));
                }
                if (!objEvent.event_fir.report_date) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.REPORT_DATE_NOT_FOUND, null));
                }
                if (!objEvent.event_fir.occurance_date) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_DATE_NOT_FOUND, null));
                }
                if (objEvent.event_fir.udr_number) {
                    if (objEvent.event_fir.udr_number.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UDR_NUMBER_LIMIT, null));
                    }
                }
                if (objEvent.event_fir.section_of_law) {
                    if (objEvent.event_fir.section_of_law.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_OF_LAW_LIMIT, null));
                    }
                }
                if (objEvent.event_fir.victim) {
                    if (objEvent.event_fir.victim.length > 200) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VICTIM_LIMIT, null));
                    }
                }
                if (objEvent.event_fir.complainant) {
                    if (objEvent.event_fir.complainant.length > 200) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.COMPLAINANT_LIMIT, null));
                    }
                }
                if (objEvent.event_fir.occurance_place) {
                    if (objEvent.event_fir.occurance_place.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_PLACE_FROM_LIMIT, null));
                    }
                }
            }
            if (objEvent.event_udr) {
                objEvent.event_udr = JSON.parse(objEvent.event_udr);
                if (!objEvent.event_udr.police_station_id) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.POLICE_STATION_ID_NOT_FOUND, null));
                }
                if (!objEvent.event_udr.report_date) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.REPORT_DATE_NOT_FOUND, null));
                }
                if (!objEvent.event_udr.occurance_date) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_DATE_NOT_FOUND, null));
                }
                if (objEvent.event_udr.udr_number) {
                    if (objEvent.event_udr.udr_number.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UDR_NUMBER_LIMIT, null));
                    }
                }
                if (objEvent.event_udr.section_of_law) {
                    if (objEvent.event_udr.section_of_law.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_OF_LAW_LIMIT, null));
                    }
                }
                if (objEvent.event_udr.accused) {
                    if (objEvent.event_udr.accused.length > 200) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.ACCUSED_LIMIT, null));
                    }
                }
                if (objEvent.event_udr.complainant) {
                    if (objEvent.event_udr.complainant.length > 200) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.COMPLAINANT_LIMIT, null));
                    }
                }
                if (objEvent.event_udr.occurance_place) {
                    if (objEvent.event_udr.occurance_place.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_PLACE_FROM_LIMIT, null));
                    }
                }
            }
            objEvent.event_firudr_type_id = null;
            // let event = await Event.findOne({
            //     where: {
            //         event_type_id: objEvent.event_type_id,
            //         unit_id: objEvent.unit_id,
            //         section_id: objEvent.section_id,
            //         desk_id: objEvent.desk_id
            //     }
            // });

            // if (!event) {    

            objEvent.created_by = req.user_id;
            objEvent.modified_by = req.user_id;
            generateEventNumber(objEvent.unit_id, objEvent.section_id, objEvent.desk_id).then(async function (event_number) {
                objEvent.event_number = event_number;
                if (objEvent.event_tags) {
                    if (objEvent.event_tags.other_tags) {
                        objEvent.other_tags = objEvent.event_tags.other_tags;
                        delete objEvent.event_tags.other_tags;
                    } else {
                        objEvent.other_tags = null;
                    }
                } else {
                    objEvent.other_tags = null;
                }
                let create_event = await Event.create(objEvent);
                for (let index = 0; index < event_attachment.length; index++) {
                    const obj = event_attachment[index];
                    await EventAttachment.create({
                        event_id: create_event.event_id,
                        attachment_name: obj.originalname,
                        attachment_extension: obj.originalname.split('.').pop(),
                        attachment_size: obj.size,
                        attachment_path: obj.location,
                        created_by: req.user_id,
                    });

                }
                if (objEvent.event_recipient) {
                    let objEventRecipient = [];
                    for (let index = 0; index < objEvent.event_recipient.length; index++) {
                        const element = objEvent.event_recipient[index];
                        objEventRecipient.push({
                            event_id: create_event.event_id,
                            recipient_type_id: element.recipient_type_id,
                            user_id: element.user_id,
                            created_by: req.user_id,
                        })
                    }

                    await EventRecipient.bulkCreate(objEventRecipient);
                }
                let event_firudr_type = null;
                try {
                    event_firudr_type = await models.event_firudr_type.findOne({
                        where: {
                            event_firudr_type_name: objEvent.event_firudr_type_name
                        }
                    });
                    if (event_firudr_type) {
                        await Event.update({
                            event_firudr_type_id: event_firudr_type.event_firudr_type_id
                        }, {
                            where: {
                                event_id: create_event.event_id
                            }
                        })

                        if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'FIR') {
                            if (!objEvent.event_fir.report_date) {
                                delete objEvent.event_fir.report_date;
                            }
                            if (!objEvent.event_fir.report_time) {
                                delete objEvent.event_fir.report_time;
                            }
                            if (!objEvent.event_fir.occurance_date) {
                                delete objEvent.event_fir.occurance_date;
                            }
                            if (!objEvent.event_fir.occurance_time) {
                                delete objEvent.event_fir.occurance_time;
                            }
                            objEvent.event_fir.event_id = create_event.event_id;
                            await models.event_fir.create(objEvent.event_fir);
                        } else if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'UDR') {
                            objEvent.event_udr.event_id = create_event.event_id;
                            if (!objEvent.event_udr.report_date) {
                                delete objEvent.event_udr.report_date;
                            }
                            if (!objEvent.event_udr.report_time) {
                                delete objEvent.event_udr.report_time;
                            }
                            if (!objEvent.event_udr.occurance_date) {
                                delete objEvent.event_udr.occurance_date;
                            }
                            if (!objEvent.event_udr.occurance_time) {
                                delete objEvent.event_udr.occurance_time;
                            }

                            await models.event_udr.create(objEvent.event_udr);
                        }
                    }
                } catch (e) {
                    console.log(e)
                }
                create_event = JSON.parse(JSON.stringify(create_event));
                saveEventTags(objEvent.event_tags, create_event).then(async function (result) {
                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.SAVE_SUCCESS, create_event))
                })
                // } else {
                //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.ALREADY_EXIST, null))
                // }
            })
        })
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }

        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const getEventMetaDataTags = async (req, res) => {
    try {
        let query = req.query;
        getEventTagsUsingParam(query.unit_id, query.section_id, query.desk_id, query.message_id).then(async function (result) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, result));
        })
    } catch (e) {

    }
}

const storeEventTags = async (req, res) => {
    try {
        let body = req.body;

        // Check event_id is exists or not
        let event = await Event.findOne({
            where: {
                event_id: body.event_id,
            }
        });

        if (!event && event == null) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else {
            let eventTags = event.other_tags ? event.other_tags : "";
            let newTags = eventTags.length ? eventTags + ',' + body.other_tags : body.other_tags;
            delete body.other_tags;
            let create_event_tag = await EventTag.create(body);
            if (create_event_tag) {
                await Event.update({
                    other_tags: newTags
                }, {
                    where: {
                        event_id: body.event_id,
                    }
                });

                await createEventLogs({
                    event_id: body.event_id,
                    event_forward_id: null,
                    user_id: req.user_id,
                    created_by: req.user_id
                }, "Update");

                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_CREATED, create_event_tag));
            } else {
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_NOT_CREATE, null));
            }
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }

        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

async function saveEventTags(objTags, event) {
    try {
        objTags.event_id = event.event_id;
        let saveTags = u.clone(objTags);
        saveTags.organization_id = null;
        saveTags.leader_id = null;
        let create_event_tag = await EventTag.create(saveTags);
        if (create_event_tag) {
            if (objTags.organization_id.length) {
                for (let index = 0; index < objTags.organization_id.length; index++) {
                    const organization_id = objTags.organization_id[index];
                    let org = await models.organizations.findByPk(organization_id);
                    if (org) {
                        await models.event_tag_organization.create({
                            event_id: event.event_id,
                            organization_id: organization_id,
                            is_active: true,
                            created_by: event.created_by,
                            modified_by: event.modified_by,
                            created_date: new Date(),
                            modified_date: new Date()
                        })
                    }
                }
            }
            if (objTags.leader_id.length) {
                for (let index = 0; index < objTags.leader_id.length; index++) {
                    const leader_id = objTags.leader_id[index];
                    let leader = await models.leader.findByPk(leader_id);
                    if (leader) {
                        await models.event_tag_leader.create({
                            event_id: event.event_id,
                            leader_id: leader_id,
                            is_active: true,
                            created_by: event.created_by,
                            modified_by: event.modified_by,
                            created_date: new Date(),
                            modified_date: new Date()
                        })
                    }
                }
            }

            return create_event_tag;
        } else {
            return null;
        }
    } catch (e) {
        console.log(e)
    }
}

async function getEventTagsUsingParam(unit_id, section_id, desk_id, message_id) {
    try {
        let result = [];
        let unit = "";
        let section = "";
        let desk = "";
        let event_message_type = "";
        try {
            unit = await Unit.findByPk(unit_id);
            unit = unit.unit_name;
        } catch (e) {

        }

        try {
            section = await Section.findByPk(section_id);
            section = section.section_name;
        } catch (e) {

        }
        try {
            desk = await Desk.findByPk(desk_id);
            desk = desk.desk_name;
        } catch (e) {

        }
        try {
            event_message_type = await EventMessageType.findByPk(message_id);
            event_message_type = event_message_type.event_message_type_name;
        } catch (e) {

        }

        if (unit != "" && section != "" && desk != "" && event_message_type != "") {
            for (let index = 0; index < EventTagJson.length; index++) {
                let element = EventTagJson[index];
                for (let index1 = 0; index1 < element.message.length; index1++) {
                    element.message[index1] = element.message[index1].trim().toLowerCase();
                }

            }

            let data = u.filter(EventTagJson, function (j) {
                j.desk = j.desk.toString().split(',');
                for (let index1 = 0; index1 < j.desk.length; index1++) {
                    j.desk[index1] = j.desk[index1].trim().toLowerCase();
                }
                if (j.unit.trim().toLowerCase() == unit.trim().toLowerCase() &&
                    j.section.trim().toLowerCase() == section.trim().toLowerCase() &&
                    (j.desk.indexOf(desk.trim().toLowerCase()) != -1) &&
                    (j.message.indexOf(event_message_type.trim().toLowerCase()) != -1)
                ) {
                    return j;
                }

            })
            if (data.length) {
                result = data[0].tags;
            }
        }
        return result;
    } catch (e) {
        return [];
    }
}

const getEventWithPagination = async (req, res) => {
    try {
        let orderBy = 'created_date DESC';
        if (req.query.filed_name && req.query.order_by) {
            orderBy = (req.query.filed_name + " " + req.query.order_by);
        }
        let where = {};
        where[Op.and] = [];
        if (req.query.event_type_id) {
            where[Op.and].push({
                event_type_id: {
                    [Op.eq]: req.query.event_type_id
                }
            });
        }
        if (req.query.event_status_type_id) {
            where[Op.and].push({
                event_status_type_id: {
                    [Op.eq]: req.query.event_status_type_id
                }
            });
        }
        if (req.query.unit_id) {
            where[Op.and].push({
                unit_id: {
                    [Op.eq]: req.query.unit_id
                }
            });
        }
        if (req.query.section_id) {
            where[Op.and].push({
                section_id: {
                    [Op.eq]: req.query.section_id
                }
            });
        }
        if (req.query.desk_id) {
            where[Op.and].push({
                desk_id: {
                    [Op.eq]: req.query.desk_id
                }
            });
        }
        if (req.query.event_message_type_id) {
            where[Op.and].push({
                event_message_type_id: {
                    [Op.eq]: req.query.event_message_type_id
                }
            });
        }
        if (req.query.event_message_sub_type_id) {
            where[Op.and].push({
                event_message_sub_type_id: {
                    [Op.eq]: req.query.event_message_sub_type_id
                }
            });
        }
        if (req.query.grade_id) {
            where[Op.and].push({
                grade_id: {
                    [Op.eq]: req.query.grade_id
                }
            });
        }
        if (req.query.subject) {
            where[Op.and].push({
                subject: {
                    [Op.iLike]: '%' + req.query.subject + '%'
                }
            });
        }
        if (req.query.from_date) {
            where[Op.and].push({
                created_date: {
                    [Op.gte]: moment(req.query.from_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        if (req.query.to_date) {
            where[Op.and].push({
                created_date: {
                    [Op.lte]: moment(req.query.to_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }

        let event = await Event.findAndCountAll({
            where: where,
            order: sequelize.literal(orderBy),
            offset: parseInt(req.query.page - 1) * parseInt(req.query.per_page),
            limit: parseInt(req.query.per_page),
            include: [{
                model: EventRecipient,
                include: [{
                    model: User,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                }],
                required: true
            },
            {
                model: EventType,
                required: false
            }, {
                model: EventStatusType,
                required: false
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            },
            {
                model: EventTag,
                required: false
            }
            ]
        });

        let resultArr = [];

        event.rows = JSON.parse(JSON.stringify(event.rows))
        for (let index = 0; index < event.rows.length; index++) {
            const element = event.rows[index];
            let from = '';
            let designation = '';
            try {
                from = await User.findByPk(element.created_by);
                designation = await Designation.findByPk(from.designation_id)


            } catch (e) {

            }
            let To = [];
            let To_Designation = [];
            for (let index1 = 0; index1 < element.event_recipients.length; index1++) {
                const element1 = element.event_recipients[index1];
                if (element1.user) {
                    To.push(element1.user.user_email);
                    if (element1.user.designation) {
                        To_Designation.push(element1.user.designation.designation_name);
                    }
                }
            }

            resultArr.push({
                event_id: element.event_id,
                event_message_type: element.event_message_type ? element.event_message_type.event_message_type_name : "",
                type: element.event_type ? element.event_type.event_type_name : "",
                from: from ? from.user_email : '',
                to: To.toString(),
                to_designation: To_Designation.toString(),
                from_designation: designation ? designation.designation_name : "",
                subject: element.subject,
                place: element.event_tag ? element.event_tag.place : "",
                grading: element.grade ? element.grade.grade_name : "",
                date: element.created_date,
                pdf: await EventAttachment.findAll({ where: { event_id: element.event_id } })
            })

        }
        let result = new Object();
        result.data = resultArr;
        result.total = event.count;
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, result))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const getEventById = async (req, res) => {
    try {
        let event_id = req.params.event_id;
        let where = {};
        where[Op.and] = [];
        where[Op.and].push({
            event_id: {
                [Op.eq]: req.params.event_id
            }
        });

        let event = await Event.findOne({
            where: where,
            include: [{
                model: EventRecipient,
                include: [{
                    model: Users,
                    required: false,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                },

                {
                    model: RecipientType,
                    required: false
                }
                ],
                required: false
            },
            {
                model: EventType,
                required: false
            }, {
                model: EventStatusType,
                required: false
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            }, {
                model: EventForward,
                required: false,
                include: [{
                    model: FordStatusType,
                    attributes: ['forward_status_name'],
                    required: false,
                },
                    //  {
                    //     model: EventForwardAttachment,
                    //     required: false,
                    // }
                ]
            },
            {
                model: models.event_firudr_type,
                required: false
            },
            {
                model: models.event_fir,
                required: false
            },
            {
                model: models.event_udr,
                required: false
            },

            ]
        });
        event = event ? JSON.parse(JSON.stringify(event)) : null;
        if (!event) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        }

        try {
            event.from = await User.findByPk(event.created_by);
            event.from_designation = await Designation.findOne({
                where: {
                    designation_id: event.from.designation_id
                }
            })

            event.from_designation = event.from_designation ? event.from_designation.designation_name : "";

        } catch (e) {
            event.from = "";
            event.from_designation = "";
        }
        event.from = event.from ? event.from.user_email : "";
        if (event && event.event_forwards.length) {
            for (let index = 0; index < event.event_forwards.length; index++) {
                const element = event.event_forwards[index];
                element.event_forward_attachment = await EventForwardAttachment.findAll({ where: { event_forward_id: element.event_forward_id } })
                element.event_forward_recipients = await EventForwardRecipient.findAll({
                    where: {
                        event_forward_id: element.event_forward_id

                    },
                    include: [{
                        model: Users,
                        required: false,
                        include: [{
                            model: Designation,
                            as: 'designation'
                        }]
                    },

                    {
                        model: RecipientType,
                        required: false
                    }
                    ],
                })
                element.event_forward_recipients = JSON.parse(JSON.stringify(element.event_forward_recipients));
                for (let j = 0; j < element.event_forward_recipients.length; j++) {
                    const element1 = element.event_forward_recipients[j];
                    element1.to = element1.user ? element1.user.user_email : "";
                    element1.designation = element1.user && element1.user.designation ? element1.user.designation.designation_name : "";
                    element1.recipient_type_name = element1.recipient_type ? element1.recipient_type.recipient_type_name : ""
                    delete element1.recipient_type;
                    delete element1.user;
                }
                element.user = await User.findOne({
                    where: {
                        user_id: element.created_by
                    },
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                });
                element.created = element.user ? element.user.user_firstname + " " + element.user.user_lastname : '';
                element.designation = element.user && element.user.designation ? element.user.designation.designation_name : "";
                delete element.user;
            }

        }
        if (event) {
            event.event_attachment = await EventAttachment.findAll({ where: { event_id: event.event_id } })
            event.event_tags = await EventTag.findOne({ where: { event_id: event.event_id } });

            if (event.event_tags) {
                event.event_tags = JSON.parse(JSON.stringify(event.event_tags));
                event.event_tags.organization_lst = await models.event_tag_organization.findAll({
                    where: {
                        event_id: event.event_id
                    }
                })

                event.event_tags.organization_id = u.unique(u.pluck(event.event_tags.organization_lst, 'organization_id'));

                event.event_tags.leader_lst = await models.event_tag_leader.findAll({
                    where: {
                        event_id: event.event_id
                    }
                })

                event.event_tags.leader_id = u.unique(u.pluck(event.event_tags.leader_lst, 'leader_id'));
            }
        }
        if (event && event.event_recipients.length) {
            for (let index = 0; index < event.event_recipients.length; index++) {
                const element = event.event_recipients[index];
                element.to = element.user ? element.user.user_email : "";
                element.designation = element.user && element.user.designation ? element.user.designation.designation_name : "";
                element.recipient_type_name = element.recipient_type ? element.recipient_type.recipient_type_name : ""
                delete element.recipient_type;
                delete element.user;
            }

        }

        await createEventLogs({
            event_id: event_id,
            event_forward_id: null,
            user_id: req.user_id,
            created_by: req.user_id
        }, "View");

        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, event))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const getEventDraftById = async (req, res) => {
    try {

        let where = {};
        where[Op.and] = [];
        where[Op.and].push({
            event_draft_id: {
                [Op.eq]: req.params.event_draft_id
            }
        });

        let event = await EventDraft.findOne({
            where: where,
            include: [{
                model: EventDraftRecipient,
                include: [{
                    model: Users,
                    required: false,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                },

                {
                    model: RecipientType,
                    required: false
                }
                ],
                required: false
            },
            {
                model: EventType,
                required: false
            }, {
                model: EventStatusType,
                required: false
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            },
            {
                model: models.event_firudr_type,
                required: false
            },
            {
                model: models.event_fir_draft,
                required: false
            },
            {
                model: models.event_udr_draft,
                required: false
            },
            ]
        });
        event = event ? JSON.parse(JSON.stringify(event)) : null;
        if (!event) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        }

        if (event) {
            event.event_attachment = await EventDraftAttachment.findAll({ where: { event_draft_id: event.event_draft_id } })
            event.event_tags = await EventDraftTag.findOne({ where: { event_draft_id: event.event_draft_id } })
            if (event.event_tags) {
                event.event_tags = JSON.parse(JSON.stringify(event.event_tags));
                event.event_tags.organization_lst = await models.event_tag_organization_draft.findAll({
                    where: {
                        event_draft_id: event.event_draft_id
                    }
                })

                event.event_tags.organization_id = u.unique(u.pluck(event.event_tags.organization_lst, 'organization_id'));

                event.event_tags.leader_lst = await models.event_tag_leader_draft.findAll({
                    where: {
                        event_draft_id: event.event_draft_id
                    }
                })

                event.event_tags.leader_id = u.unique(u.pluck(event.event_tags.leader_lst, 'leader_id'));
            }
        }
        if (event && event.event_draft_recipients.length) {
            for (let index = 0; index < event.event_draft_recipients.length; index++) {
                const element = event.event_draft_recipients[index];
                element.to = element.user ? element.user.user_email : "";
                element.designation = element.user && element.user.designation ? element.user.designation.designation_name : "";
                element.recipient_type_name = element.recipient_type ? element.recipient_type.recipient_type_name : ""
                delete element.recipient_type;
                delete element.user;
            }

        }

        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, event))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }

        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const forwardEvent = async (req, res) => {
    try {
        let { event_id, event_due_date, event_instructions, forward_status_id, user_ids, recipient_type_id, forwarded_by_user_id } = req.body;

        user_ids = JSON.parse(user_ids);
        let event_forward_attachments = [];
        let current_date = moment().format('YYYY-MM-DD');
        let current_date_time = moment().format('YYYY-MM-DD hh:mm:ss');
        event_due_date = moment(event_due_date).format('YYYY-MM-DD');

        if (current_date > event_due_date) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_FORWARD_DUE_DATE))
        }

        let event_forward_object = {
            event_id,
            event_due_date,
            event_instructions,
            forward_status_id: forward_status_id,
            created_by: forwarded_by_user_id,
            created_at: current_date_time
        };

        // Create Forward Event
        let create_event_forward = await EventForward.create(event_forward_object);
        if (create_event_forward) {
            let { event_forward_id } = create_event_forward;

            await createEventLogs({
                event_id: event_id,
                event_forward_id: event_forward_id,
                user_id: req.user_id,
                created_by: req.user_id
            }, "View");

            let eventForwardRecipientsArr = [];

            // Fetch recipient and store it in table
            user_ids.forEach(user_id => {
                eventForwardRecipientsArr.push({
                    event_forward_id,
                    recipient_type_id,
                    user_id,
                    created_by: forwarded_by_user_id,
                    created_at: current_date_time
                });
            });

            // Store bulk recipient at a time.
            EventForwardRecipient.bulkCreate(eventForwardRecipientsArr).then(async (recipientData, err) => {
                if (err) {
                    return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_FORWARD_RECIPIENTS_NOT_CREATED));
                } else {
                    // Checking attached files
                    if (req.files && req.files.event_forward_attachments && req.files.event_forward_attachments.length) {
                        event_forward_attachments = req.files && req.files.event_forward_attachments && req.files.event_forward_attachments.length ? req.files.event_forward_attachments : [];

                        let eventForwardAttachmentsData = [];

                        // Fetch and gather event forward attachment data
                        event_forward_attachments.forEach((fileElement) => {
                            let attachmentObj = {
                                event_forward_id,
                                attachment_name: fileElement.originalname,
                                attachment_extension: fileElement.originalname.split('.').pop(),
                                attachment_size: fileElement.size,
                                attachment_path: fileElement.location,
                                created_by: forwarded_by_user_id,
                                created_at: current_date_time
                            };

                            eventForwardAttachmentsData.push(attachmentObj);
                        });

                        // Store attachments in tables
                        EventForwardAttachment.bulkCreate(eventForwardAttachmentsData).then(async (attachmentData, err) => {
                            if (err) {
                                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_FORWARD_RECIPIENTS_NOT_CREATED));
                            } else {
                                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_FORWARD_RECIPIENTS_SUCCESS));
                            }
                        });
                    } else {
                        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_FORWARD_RECIPIENTS_SUCCESS));
                    }
                }
            });
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_FORWARD_NOT_CREATED))
        }

    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const createEventDraft = async (req, res) => {
    console.log(req.body)
    try {
        let objEvent = req.body;
        //size validation
        if (objEvent.source_of_message) {
            if (objEvent.source_of_message.length > 200) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SOURCE_OF_MESSAGE_LIMIT, null));
            }
        }
        if (objEvent.subject) {
            if (objEvent.subject.length > 200) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SUBJECT_LIMIT, null));
            }
        }


        let event_draft_attachment = [];

        if (req.files && req.files.event_attachment && req.files.event_attachment.length) {
            event_draft_attachment = req.files && req.files.event_attachment && req.files.event_attachment.length ? req.files.event_attachment : [];
        }

        try {
            objEvent.event_type_id = await EventType.findOne({ where: { is_default: true } });
            objEvent.event_type_id = objEvent.event_type_id ? objEvent.event_type_id.event_type_id : null;
        } catch (e) { }
        try {
            objEvent.event_status_type_id = await EventStatusType.findOne({ where: { is_default: true } });
            objEvent.event_status_type_id = objEvent.event_status_type_id ? objEvent.event_status_type_id.event_status_type_id : null;
        } catch (e) { }
        // let event = await EventDraft.findOne({
        //     where: {
        //         event_type_id: objEvent.event_type_id,
        //         unit_id: objEvent.unit_id,
        //         section_id: objEvent.section_id,
        //         desk_id: objEvent.desk_id
        //     }
        // });

        // if (!event) {

        let checkUnit = await commonHelper.checkUnit(objEvent.unit_id);
        if (checkUnit == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UNIT_NOT_FOUND, null));
        }

        let checkSection = await commonHelper.checkSection(objEvent.section_id);
        if (checkSection == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_NOT_FOUND, null));
        }

        let checkDesk = await commonHelper.checkDesk(objEvent.desk_id);
        if (checkDesk == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DESK_NOT_FOUND, null));
        }

        if (!objEvent.event_type_id) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_TYPE_NOT_EXISTS, null));
        }
        if (!objEvent.event_status_type_id) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_STATUS_NOT_EXISTS, null));
        }



        if (objEvent.grade_id) {
            let checkGrade = await commonHelper.checkGrade(objEvent.grade_id);
            if (checkGrade == false) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.GRADE_NOT_FOUND, null));
            }
        }
        if (objEvent.event_message_type_id) {
            let checkEventMessageType = await commonHelper.checkEventMessageType(objEvent.event_message_type_id);
            if (checkEventMessageType == false) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_TYPE_NOT_FOUND, null));
            }
        }
        if (objEvent.event_message_sub_type_id) {
            let checkEventMessageSubType = await commonHelper.checkEventMessageSubType(objEvent.event_message_sub_type_id);
            if (checkEventMessageSubType == false) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_SUB_TYPE_NOT_FOUND, null));
            }
        }


        if (objEvent.event_recipient) {
            objEvent.event_recipient = JSON.parse(objEvent.event_recipient);
            for (let index = 0; index < objEvent.event_recipient.length; index++) {
                const element = objEvent.event_recipient[index];
                let checkRecipientType = await commonHelper.checkRecipientType(element.recipient_type_id);
                if (checkRecipientType == false) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RECIPIENT_TYPE_NOT_FOUND, null));
                    break;
                }
                let checkUser = await commonHelper.checkUser(element.user_id);
                if (checkUser == false) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.USER_NOT_FOUND, null));
                    break;
                }
            }
        } else {
            objEvent.event_recipient = [];
        }

        if (objEvent.event_tags) {
            try {
                objEvent.event_tags = JSON.parse(objEvent.event_tags);

                if (objEvent.event_tags.district_id) {
                    let checkDistrict = await commonHelper.checkDistrict(objEvent.event_tags.district_id);
                    if (checkDistrict == false) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DISTRICT_NOT_FOUND, null));
                    }
                }
                if (objEvent.event_tags.farmer_name) {
                    if (objEvent.event_tags.farmer_name.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.FARMER_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.reservoir_level) {
                    if (objEvent.event_tags.reservoir_level.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RESERVOIR_LEVEL_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.gross_capacity) {
                    if (objEvent.event_tags.gross_capacity.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.GROSS_CAPACITY_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.live_capacity) {
                    if (objEvent.event_tags.live_capacity.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.LIVE_CAPACITY_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.in_flow) {
                    if (objEvent.event_tags.in_flow.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.IN_FLOW_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.out_flow) {
                    if (objEvent.event_tags.out_flow.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OUT_FLOW_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.water_release_to_canal) {
                    if (objEvent.event_tags.water_release_to_canal.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.WATER_RELEASE_TO_CANAL_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.rain_fall) {
                    if (objEvent.event_tags.rain_fall.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RAIN_FALL_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.cc_sc) {
                    if (objEvent.event_tags.cc_sc.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CC_SC_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.case_type) {
                    if (objEvent.event_tags.case_type.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CASE_TYPE_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.visitors) {
                    if (objEvent.event_tags.visitors.length > 200) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VISITORS_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.relation) {
                    if (objEvent.event_tags.relation.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RELATION_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.visitor_mobile_number) {
                    if (objEvent.event_tags.visitor_mobile_number.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VISITOR_MOBILE_NUMBER_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.death) {
                    if (objEvent.event_tags.death.length > 20) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DEATH_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.injury) {
                    if (objEvent.event_tags.injury.length > 20) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.INJURY_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.call_received_from) {
                    if (objEvent.event_tags.call_received_from.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CALL_RECEIVED_FROM_LIMIT, null));
                    }
                }
            } catch (e) {
                return res.json(await commonResponse.responseReturn(false, req, e.message, null));
            }
        }

        if (objEvent.event_fir) {
            objEvent.event_fir = JSON.parse(objEvent.event_fir);
            if (!objEvent.event_fir.police_station_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.POLICE_STATION_ID_NOT_FOUND, null));
            }

            if (objEvent.event_fir.udr_number) {
                if (objEvent.event_fir.udr_number.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UDR_NUMBER_LIMIT, null));
                }
            }
            if (objEvent.event_fir.section_of_law) {
                if (objEvent.event_fir.section_of_law.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_OF_LAW_LIMIT, null));
                }
            }
            if (objEvent.event_fir.victim) {
                if (objEvent.event_fir.victim.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VICTIM_LIMIT, null));
                }
            }
            if (objEvent.event_fir.complainant) {
                if (objEvent.event_fir.complainant.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.COMPLAINANT_LIMIT, null));
                }
            }
            if (objEvent.event_fir.occurance_place) {
                if (objEvent.event_fir.occurance_place.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_PLACE_FROM_LIMIT, null));
                }
            }
        }
        if (objEvent.event_udr) {
            objEvent.event_udr = JSON.parse(objEvent.event_udr);
            if (!objEvent.event_udr.police_station_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.POLICE_STATION_ID_NOT_FOUND, null));
            }
            if (objEvent.event_udr.udr_number) {
                if (objEvent.event_udr.udr_number.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UDR_NUMBER_LIMIT, null));
                }
            }
            if (objEvent.event_udr.section_of_law) {
                if (objEvent.event_udr.section_of_law.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_OF_LAW_LIMIT, null));
                }
            }
            if (objEvent.event_udr.accused) {
                if (objEvent.event_udr.accused.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.ACCUSED_LIMIT, null));
                }
            }
            if (objEvent.event_udr.complainant) {
                if (objEvent.event_udr.complainant.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.COMPLAINANT_LIMIT, null));
                }
            }
            if (objEvent.event_udr.occurance_place) {
                if (objEvent.event_udr.occurance_place.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_PLACE_FROM_LIMIT, null));
                }
            }
        }
        objEvent.created_by = req.user_id;
        objEvent.modified_by = req.user_id;
        if (objEvent.event_tags) {
            if (objEvent.event_tags.other_tags) {
                objEvent.other_tags = objEvent.event_tags.other_tags;
                delete objEvent.event_tags.other_tags;
            } else {
                objEvent.other_tags = null;
            }
        } else {
            objEvent.other_tags = null;
        }
        let create_event = await EventDraft.create(objEvent);
        for (let index = 0; index < event_draft_attachment.length; index++) {
            const obj = event_draft_attachment[index];
            await EventDraftAttachment.create({
                event_draft_id: create_event.event_draft_id,
                attachment_name: obj.originalname,
                attachment_extension: obj.originalname.split('.').pop(),
                attachment_size: obj.size,
                attachment_path: obj.location,
                created_by: req.user_id,
            });

        }
        if (objEvent.event_recipient) {
            let objEventRecipient = [];
            for (let index = 0; index < objEvent.event_recipient.length; index++) {
                const element = objEvent.event_recipient[index];
                objEventRecipient.push({
                    event_draft_id: create_event.event_draft_id,
                    recipient_type_id: element.recipient_type_id,
                    user_id: element.user_id,
                    created_by: req.user_id,
                })
            }
            await EventDraftRecipient.bulkCreate(objEventRecipient);
        }
        let event_firudr_type = null;
        try {
            event_firudr_type = await models.event_firudr_type.findOne({
                where: {
                    event_firudr_type_name: objEvent.event_firudr_type_name
                }
            });
            if (event_firudr_type) {
                await EventDraft.update({
                    event_firudr_type_id: event_firudr_type.event_firudr_type_id
                }, {
                    where: {
                        event_draft_id: create_event.event_draft_id
                    }
                })
                if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'FIR') {
                    objEvent.event_fir.event_draft_id = create_event.event_draft_id;
                    if (!objEvent.event_fir.report_date) {
                        delete objEvent.event_fir.report_date;
                    }
                    if (!objEvent.event_fir.report_time) {
                        delete objEvent.event_fir.report_time;
                    }
                    if (!objEvent.event_fir.occurance_date) {
                        delete objEvent.event_fir.occurance_date;
                    }
                    if (!objEvent.event_fir.occurance_time) {
                        delete objEvent.event_fir.occurance_time;
                    }
                    await models.event_fir_draft.create(objEvent.event_fir);
                } else if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'UDR') {
                    objEvent.event_udr.event_draft_id = create_event.event_draft_id;
                    if (!objEvent.event_udr.report_date) {
                        delete objEvent.event_udr.report_date;
                    }
                    if (!objEvent.event_udr.report_time) {
                        delete objEvent.event_udr.report_time;
                    }
                    if (!objEvent.event_udr.occurance_date) {
                        delete objEvent.event_udr.occurance_date;
                    }
                    if (!objEvent.event_udr.occurance_time) {
                        delete objEvent.event_udr.occurance_time;
                    }
                    await models.event_udr_draft.create(objEvent.event_udr);
                }
            }
        } catch (e) {

        }
        create_event = JSON.parse(JSON.stringify(create_event));
        if (objEvent.event_tags) {
            saveEventDraftTags(objEvent.event_tags, create_event).then(async function (result) {
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.SAVE_SUCCESS, create_event))
            })
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.SAVE_SUCCESS, create_event))
        }
        // } else {
        //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.ALREADY_EXIST, null))
        // }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const UpdateEventDraft = async (req, res) => {
    try {
        let objEvent = req.body;
        if (objEvent.source_of_message) {
            if (objEvent.source_of_message.length > 200) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SOURCE_OF_MESSAGE_LIMIT, null));
            }
        }
        if (objEvent.subject) {
            if (objEvent.subject.length > 200) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SUBJECT_LIMIT, null));
            }
        }


        let event_draft_attachment = [];

        if (req.files && req.files.event_attachment && req.files.event_attachment.length) {
            event_draft_attachment = req.files && req.files.event_attachment && req.files.event_attachment.length ? req.files.event_attachment : [];
        }
        try {
            objEvent.event_type_id = await EventType.findOne({ where: { is_default: true } });
            objEvent.event_type_id = objEvent.event_type_id ? objEvent.event_type_id.event_type_id : null;
        } catch (e) { }
        try {
            objEvent.event_status_type_id = await EventStatusType.findOne({ where: { is_default: true } });
            objEvent.event_status_type_id = objEvent.event_status_type_id ? objEvent.event_status_type_id.event_status_type_id : null;
        } catch (e) { }
        let checkUnit = await commonHelper.checkUnit(objEvent.unit_id);
        if (checkUnit == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UNIT_NOT_FOUND, null));
        }

        let checkSection = await commonHelper.checkSection(objEvent.section_id);
        if (checkSection == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_NOT_FOUND, null));
        }

        let checkDesk = await commonHelper.checkDesk(objEvent.desk_id);
        if (checkDesk == false) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DESK_NOT_FOUND, null));
        }

        if (!objEvent.event_type_id) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_TYPE_NOT_EXISTS, null));
        }
        if (!objEvent.event_status_type_id) {
            return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_STATUS_NOT_EXISTS, null));
        }
        if (objEvent.grade_id) {
            let checkGrade = await commonHelper.checkGrade(objEvent.grade_id);
            if (checkGrade == false) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.GRADE_NOT_FOUND, null));
            }
        }
        if (objEvent.event_message_type_id) {
            let checkEventMessageType = await commonHelper.checkEventMessageType(objEvent.event_message_type_id);
            if (checkEventMessageType == false) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_TYPE_NOT_FOUND, null));
            }
        }
        if (objEvent.event_message_sub_type_id) {
            let checkEventMessageSubType = await commonHelper.checkEventMessageSubType(objEvent.event_message_sub_type_id);
            if (checkEventMessageSubType == false) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_SUB_TYPE_NOT_FOUND, null));
            }
        }


        if (objEvent.event_recipient) {
            objEvent.event_recipient = JSON.parse(objEvent.event_recipient);
            for (let index = 0; index < objEvent.event_recipient.length; index++) {
                const element = objEvent.event_recipient[index];
                let checkRecipientType = await commonHelper.checkRecipientType(element.recipient_type_id);
                if (checkRecipientType == false) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RECIPIENT_TYPE_NOT_FOUND, null));
                    break;
                }
                let checkUser = await commonHelper.checkUser(element.user_id);
                if (checkUser == false) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.USER_NOT_FOUND, null));
                    break;
                }
            }
        } else {
            objEvent.event_recipient = [];
        }

        if (objEvent.event_tags) {
            try {
                objEvent.event_tags = JSON.parse(objEvent.event_tags);
                if (objEvent.event_tags.district_id) {
                    let checkDistrict = await commonHelper.checkDistrict(objEvent.event_tags.district_id);
                    if (checkDistrict == false) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DISTRICT_NOT_FOUND, null));
                    }
                }
                if (objEvent.event_tags.farmer_name) {
                    if (objEvent.event_tags.farmer_name.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.FARMER_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.reservoir_level) {
                    if (objEvent.event_tags.reservoir_level.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RESERVOIR_LEVEL_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.gross_capacity) {
                    if (objEvent.event_tags.gross_capacity.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.GROSS_CAPACITY_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.live_capacity) {
                    if (objEvent.event_tags.live_capacity.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.LIVE_CAPACITY_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.in_flow) {
                    if (objEvent.event_tags.in_flow.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.IN_FLOW_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.out_flow) {
                    if (objEvent.event_tags.out_flow.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OUT_FLOW_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.water_release_to_canal) {
                    if (objEvent.event_tags.water_release_to_canal.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.WATER_RELEASE_TO_CANAL_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.rain_fall) {
                    if (objEvent.event_tags.rain_fall.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RAIN_FALL_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.cc_sc) {
                    if (objEvent.event_tags.cc_sc.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CC_SC_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.case_type) {
                    if (objEvent.event_tags.case_type.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CASE_TYPE_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.visitors) {
                    if (objEvent.event_tags.visitors.length > 200) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VISITORS_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.relation) {
                    if (objEvent.event_tags.relation.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.RELATION_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.visitor_mobile_number) {
                    if (objEvent.event_tags.visitor_mobile_number.length > 50) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VISITOR_MOBILE_NUMBER_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.death) {
                    if (objEvent.event_tags.death.length > 20) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DEATH_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.injury) {
                    if (objEvent.event_tags.injury.length > 20) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.INJURY_LIMIT, null));
                    }
                }
                if (objEvent.event_tags.call_received_from) {
                    if (objEvent.event_tags.call_received_from.length > 100) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.CALL_RECEIVED_FROM_LIMIT, null));
                    }
                }
            } catch (e) {

            }
        }
        if (objEvent.event_fir) {
            objEvent.event_fir = JSON.parse(objEvent.event_fir);
            if (!objEvent.event_fir.police_station_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.POLICE_STATION_ID_NOT_FOUND, null));
            }
            if (objEvent.event_fir.udr_number) {
                if (objEvent.event_fir.udr_number.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UDR_NUMBER_LIMIT, null));
                }
            }
            if (objEvent.event_fir.section_of_law) {
                if (objEvent.event_fir.section_of_law.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_OF_LAW_LIMIT, null));
                }
            }
            if (objEvent.event_fir.victim) {
                if (objEvent.event_fir.victim.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.VICTIM_LIMIT, null));
                }
            }
            if (objEvent.event_fir.complainant) {
                if (objEvent.event_fir.complainant.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.COMPLAINANT_LIMIT, null));
                }
            }
            if (objEvent.event_fir.occurance_place) {
                if (objEvent.event_fir.occurance_place.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_PLACE_FROM_LIMIT, null));
                }
            }
        }
        if (objEvent.event_udr) {
            objEvent.event_udr = JSON.parse(objEvent.event_udr);
            if (!objEvent.event_udr.police_station_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.POLICE_STATION_ID_NOT_FOUND, null));
            }
            if (objEvent.event_udr.udr_number) {
                if (objEvent.event_udr.udr_number.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UDR_NUMBER_LIMIT, null));
                }
            }
            if (objEvent.event_udr.section_of_law) {
                if (objEvent.event_udr.section_of_law.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_OF_LAW_LIMIT, null));
                }
            }
            if (objEvent.event_udr.accused) {
                if (objEvent.event_udr.accused.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.ACCUSED_LIMIT, null));
                }
            }
            if (objEvent.event_udr.complainant) {
                if (objEvent.event_udr.complainant.length > 200) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.COMPLAINANT_LIMIT, null));
                }
            }
            if (objEvent.event_udr.occurance_place) {
                if (objEvent.event_udr.occurance_place.length > 100) {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.OCCURANCE_PLACE_FROM_LIMIT, null));
                }
            }
        }
        if (!objEvent.event_firudr_type_id) {
            objEvent.event_firudr_type_id = null;
        }

        objEvent.created_by = req.user_id;
        objEvent.modified_by = req.user_id;
        if (objEvent.event_tags) {
            if (objEvent.event_tags.other_tags) {
                objEvent.other_tags = objEvent.event_tags.other_tags;
                delete objEvent.event_tags.other_tags;
            } else {
                objEvent.other_tags = null;
            }
        } else {
            objEvent.other_tags = null;
        }
        let event_update = await EventDraft.update(objEvent, {
            where: {
                event_draft_id: objEvent.event_draft_id
            }
        });
        let create_event = await EventDraft.findByPk(objEvent.event_draft_id);
        // await EventDraftAttachment.destroy({ where: { event_draft_id: objEvent.event_draft_id } });
        for (let index = 0; index < event_draft_attachment.length; index++) {
            const obj = event_draft_attachment[index];
            await EventDraftAttachment.create({
                event_draft_id: create_event.event_draft_id,
                attachment_name: obj.originalname,
                attachment_extension: obj.originalname.split('.').pop(),
                attachment_size: obj.size,
                attachment_path: obj.location,
                created_by: req.user_id,
            });

        }
        await EventDraftRecipient.destroy({ where: { event_draft_id: objEvent.event_draft_id } });
        if (objEvent.event_recipient) {
            let objEventRecipient = [];
            for (let index = 0; index < objEvent.event_recipient.length; index++) {
                const element = objEvent.event_recipient[index];
                objEventRecipient.push({
                    event_draft_id: create_event.event_draft_id,
                    recipient_type_id: element.recipient_type_id,
                    user_id: element.user_id,
                    created_by: req.user_id
                })
            }
            await EventDraftRecipient.bulkCreate(objEventRecipient);
        }
        await models.event_fir_draft.destroy({ where: { event_draft_id: objEvent.event_draft_id } });
        await models.event_udr_draft.destroy({ where: { event_draft_id: objEvent.event_draft_id } });
        let event_firudr_type = null;
        try {
            event_firudr_type = await models.event_firudr_type.findOne({
                where: {
                    event_firudr_type_name: objEvent.event_firudr_type_name
                }
            });
            if (event_firudr_type) {
                await EventDraft.update({
                    event_firudr_type_id: event_firudr_type.event_firudr_type_id
                }, {
                    where: {
                        event_draft_id: create_event.event_draft_id
                    }
                })
                if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'FIR') {
                    objEvent.event_fir.event_draft_id = create_event.event_draft_id;
                    if (!objEvent.event_fir.report_date) {
                        delete objEvent.event_fir.report_date;
                    }
                    if (!objEvent.event_fir.report_time) {
                        delete objEvent.event_fir.report_time;
                    }
                    if (!objEvent.event_fir.occurance_date) {
                        delete objEvent.event_fir.occurance_date;
                    }
                    if (!objEvent.event_fir.occurance_time) {
                        delete objEvent.event_fir.occurance_time;
                    }
                    await models.event_fir_draft.create(objEvent.event_fir);
                } else if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'UDR') {
                    objEvent.event_udr.event_draft_id = create_event.event_draft_id;
                    if (!objEvent.event_udr.report_date) {
                        delete objEvent.event_udr.report_date;
                    }
                    if (!objEvent.event_udr.report_time) {
                        delete objEvent.event_udr.report_time;
                    }
                    if (!objEvent.event_udr.occurance_date) {
                        delete objEvent.event_udr.occurance_date;
                    }
                    if (!objEvent.event_udr.occurance_time) {
                        delete objEvent.event_udr.occurance_time;
                    }
                    await models.event_udr_draft.create(objEvent.event_udr);
                }
            }
        } catch (e) {
            console.log(e)
        }
        create_event = JSON.parse(JSON.stringify(create_event));


        await EventDraftTag.destroy({ where: { event_draft_id: objEvent.event_draft_id } });
        await models.event_tag_organization_draft.destroy({ where: { event_draft_id: objEvent.event_draft_id } });
        await models.event_tag_leader_draft.destroy({ where: { event_draft_id: objEvent.event_draft_id } });
        if (objEvent.event_tags) {
            saveEventDraftTags(objEvent.event_tags, create_event).then(async function (result) {
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_DRAFT_UPDATE, create_event))
            })
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_DRAFT_UPDATE, create_event))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const submitEvent = async (req, res) => {
    try {
        let objEvent = req.body;
        let event_draft = await EventDraft.findOne({
            where: {
                event_draft_id: objEvent.event_draft_id,
            }
        });
        if (event_draft) {
            event_draft = JSON.parse(JSON.stringify(event_draft));
            if (!event_draft.unit_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.UNIT_EMPTY, null));
            }
            if (!event_draft.section_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SECTION_EMPTY, null));
            }
            if (!event_draft.desk_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DESK_EMPTY, null));
            }
            if (!event_draft.event_message_type_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_TYPE_EMPTY, null));
            }
            if (!event_draft.event_message_sub_type_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.EVENT_MESSAGE_SUB_TYPE_EMPTY, null));
            }
            if (!event_draft.grade_id) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.GRADE_EMPTY, null));

            }
            if (!event_draft.subject) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SUBJECT_EMPTY, null));
            }
            if (!event_draft.source_of_message) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.SOURCE_MESSAGE_EMPTY, null));
            }

            if (!event_draft.description) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.DESCRIPTION_MESSAGE_EMPTY, null));
            }

            getEventTagsUsingParam(event_draft.unit_id, event_draft.section_id, event_draft.desk_id, event_draft.event_message_type_id).then(async function (result) {
                let event_draft_tags = await EventDraftTag.findAll({
                    where: {
                        event_draft_id: objEvent.event_draft_id,
                    }
                })
                event_draft_tags = JSON.parse(JSON.stringify(event_draft_tags));
                let event_tag_organizations = await models.event_tag_organization_draft.findAll({
                    where: {
                        event_draft_id: objEvent.event_draft_id,
                    }
                })
                event_tag_organizations = JSON.parse(JSON.stringify(event_tag_organizations));
                let event_tag_leaders = await models.event_tag_leader_draft.findAll({
                    where: {
                        event_draft_id: objEvent.event_draft_id,
                    }
                })
                event_tag_leaders = JSON.parse(JSON.stringify(event_tag_leaders));
                console.log(event_draft_tags)
                if (event_draft_tags.length) {
                    let requiredTags = u.where(result, { is_required: "1" });
                    for (let index = 0; index < requiredTags.length; index++) {
                        const obj = requiredTags[index];
                        let column_name = obj['column_name'];
                        if (column_name) {
                            if (column_name == 'organization_id' || column_name == 'leader_id') {
                                console.log("event_tag_organizations.length", event_tag_organizations.length)
                                if (column_name == 'organization_id') {
                                    if (event_tag_organizations.length == 0) {
                                        let msg = capitalizeFirstLetter(column_name.replace('_', ' ').replace('id', '').trim() + " cannot be Empty")
                                        return res.json(await commonResponse.responseReturn(false, req, msg, null));
                                        break;
                                    }
                                }
                                if (column_name == 'leader_id') {
                                    if (event_tag_leaders.length == 0) {
                                        let msg = capitalizeFirstLetter(column_name.replace('_', ' ').replace('id', '').trim() + " cannot be Empty")
                                        return res.json(await commonResponse.responseReturn(false, req, msg, null));
                                        break;
                                    }
                                }
                            } else {
                                if (!event_draft_tags[0][column_name]) {
                                    let msg = capitalizeFirstLetter(column_name.replace('_', ' ').replace('id', '').trim() + " cannot be Empty")
                                    return res.json(await commonResponse.responseReturn(false, req, msg, null));
                                    break;
                                }
                            }
                        }
                    }
                    // if (!event_draft_tags[0].state_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.STATE_EMPTY, null));
                    // }
                    // if (!event_draft_tags[0].district_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.DISTRICT_EMPTY, null));
                    // }
                    // if (!event_draft_tags[0].taluk_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.TALUK_EMPTY, null));
                    // }

                    // if (!event_draft_tags[0].crop_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.CROP_EMPTY, null));
                    // }
                    // if (!event_draft_tags[0].dam_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.DAM_EMPTY, null));
                    // }
                    // if (!event_draft_tags[0].court_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.COURT_EMPTY, null));
                    // }
                    // if (!event_draft_tags[0].political_party_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.POLITICAL_PARTY_EMPTY, null));
                    // }
                    // if (!event_draft_tags[0].person_id) {
                    //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.PERSON_EMPTY, null));
                    // }
                } else {
                    return res.json(await commonResponse.responseReturn(false, req, commonMessage.Event.TAG_NOT_FOUND, null));
                }

                // if (!event_tag_organizations.length) {
                //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.ORGANIZATION_EMPTY, null));
                // }
                // if (!event_tag_leaders.length) {
                //     return res.json(await commonResponse.responseReturn(false,req, commonMessage.Event.LEADER_EMPTY, null));
                // }

                objEvent.created_by = req.user_id;
                objEvent.modified_by = req.user_id;

                delete event_draft.event_draft_id;
                generateEventNumber(event_draft.unit_id, event_draft.section_id, event_draft.desk_id).then(async function (event_number) {
                    event_draft.event_number = event_number;
                    let event_status = await EventStatusType.findOne({ where: { event_status_type_name: 'Sent' } });
                    event_draft.event_status_type_id = event_status ? event_status.event_status_type_id : 0;
                    let create_event = await Event.create(event_draft);
                    let event_draft_attachment = await EventDraftAttachment.findAll({
                        where: {
                            event_draft_id: objEvent.event_draft_id,
                        }
                    });

                    let attachment = [];
                    for (let index = 0; index < event_draft_attachment.length; index++) {
                        const element = event_draft_attachment[index];
                        attachment.push({
                            event_id: create_event.event_id,
                            attachment_name: element.attachment_name,
                            attachment_extension: element.attachment_extension,
                            attachment_size: element.attachment_size,
                            attachment_path: element.attachment_path,
                            created_by: req.user_id,
                        })
                    }
                    await EventAttachment.bulkCreate(attachment);
                    let event_tags = [];
                    for (let index = 0; index < event_draft_tags.length; index++) {
                        const element = event_draft_tags[index];
                        delete element.event_draft_tag_id;
                        delete element.event_draft_id;
                        element.event_id = create_event.event_id;
                        event_tags.push(element);
                    }
                    await EventTag.bulkCreate(event_tags);

                    for (let index = 0; index < event_tag_organizations.length; index++) {
                        await models.event_tag_organization.create({
                            event_id: create_event.event_id,
                            organization_id: event_tag_organizations[index].organization_id,
                            is_active: true,
                            created_by: req.user_id,
                            modified_by: req.user_id,
                            created_date: new Date(),
                            modified_date: new Date()
                        })
                    }


                    for (let index = 0; index < event_tag_leaders.length; index++) {
                        await models.event_tag_leader.create({
                            event_id: create_event.event_id,
                            leader_id: event_tag_leaders[index].leader_id,
                            is_active: true,
                            created_by: req.user_id,
                            modified_by: req.user_id,
                            created_date: new Date(),
                            modified_date: new Date()
                        })
                    }
                    let event_draft_recipient = await EventDraftRecipient.findAll({
                        where: {
                            event_draft_id: objEvent.event_draft_id,
                        }
                    })
                    event_draft_recipient = JSON.parse(JSON.stringify(event_draft_recipient));
                    for (let index = 0; index < event_draft_recipient.length; index++) {
                        const element = event_draft_recipient[index];
                        element.event_id = create_event.event_id;
                        delete event_draft_id;
                        delete event_draft_recipient_id;
                        element.created_by = req.user_id;
                        element.created_date = new Date();
                    }
                    await EventRecipient.bulkCreate(event_draft_recipient);

                    let event_firudr_type = null;
                    try {
                        event_firudr_type = await models.event_firudr_type.findOne({
                            where: {
                                event_firudr_type_id: event_draft.event_firudr_type_id
                            }
                        });
                        if (event_firudr_type) {
                            await Event.update({
                                event_firudr_type_id: event_firudr_type.event_firudr_type_id
                            }, {
                                where: {
                                    event_id: create_event.event_id
                                }
                            })
                            if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'FIR') {
                                let event_fir = await models.event_fir_draft.findOne({ where: { event_draft_id: objEvent.event_draft_id } });
                                if (event_fir) {
                                    event_fir = JSON.parse(JSON.stringify(event_fir));
                                }
                                event_fir.event_id = create_event.event_id
                                await models.event_fir.create(event_fir);
                            } else if (event_firudr_type.event_firudr_type_name.toUpperCase() == 'UDR') {
                                let event_udr = await models.event_udr_draft.findOne({ where: { event_draft_id: objEvent.event_draft_id } });
                                if (event_udr) {
                                    event_udr = JSON.parse(JSON.stringify(event_udr));
                                }
                                event_udr.event_id = create_event.event_id
                                await models.event_udr.create(event_udr);
                            }

                            await createEventLogs({
                                event_id: create_event.event_id,
                                event_forward_id: null,
                                user_id: req.user_id,
                                created_by: req.user_id
                            }, "Create");
                        }
                    } catch (e) {
                        console.log(e)
                    }
                    DeleteDraft(objEvent).then(async function () {
                        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.SUBMIT_SUCCESS, create_event))
                    })
                })
            })
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_INVALID_DRAFT_ID, null));
        }

    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const getEventDraftWithPagination = async (req, res) => {
    try {
        let orderBy = []
        if (req.query.filed_name && req.query.order_by) {
            if (req.query.filed_name == 'type') {
                orderBy.push([EventType, 'event_type_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'grading') {
                orderBy.push([Grade, 'grade_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'place') {
                orderBy.push([EventDraftTag, 'place', req.query.order_by]);
            }
            if (req.query.filed_name == 'date') {
                orderBy.push(['created_date', req.query.order_by]);
            }
            if (req.query.filed_name == 'subject') {
                orderBy.push(['subject', req.query.order_by]);
            }
            if (req.query.filed_name == 'from') {
                orderBy.push([User, 'user_firstname', req.query.order_by]);
            }
        } else {
            orderBy.push(['created_date', req.query.order_by]);
        }
        let where = {};
        where[Op.and] = [];
        if (req.query.event_type_id) {
            where[Op.and].push({
                event_type_id: {
                    [Op.eq]: req.query.event_type_id
                }
            });
        }
        if (req.query.event_status_type_id) {
            where[Op.and].push({
                event_status_type_id: {
                    [Op.eq]: req.query.event_status_type_id
                }
            });
        }
        if (req.query.unit_id) {
            where[Op.and].push({
                unit_id: {
                    [Op.eq]: req.query.unit_id
                }
            });
        }
        if (req.query.section_id) {
            where[Op.and].push({
                section_id: {
                    [Op.eq]: req.query.section_id
                }
            });
        }
        if (req.query.desk_id) {
            where[Op.and].push({
                desk_id: {
                    [Op.eq]: req.query.desk_id
                }
            });
        }
        if (req.query.event_message_type_id) {
            where[Op.and].push({
                event_message_type_id: {
                    [Op.eq]: req.query.event_message_type_id
                }
            });
        }
        if (req.query.event_message_sub_type_id) {
            where[Op.and].push({
                event_message_sub_type_id: {
                    [Op.eq]: req.query.event_message_sub_type_id
                }
            });
        }
        if (req.query.grade_id) {
            where[Op.and].push({
                grade_id: {
                    [Op.eq]: req.query.grade_id
                }
            });
        }
        if (req.query.subject) {
            where[Op.and].push({
                subject: {
                    [Op.iLike]: '%' + req.query.subject + '%'
                }
            });
        }
        if (req.query.from_date) {
            where[Op.and].push({
                created_date: {
                    [Op.gte]: moment(req.query.from_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        if (req.query.to_date) {
            where[Op.and].push({
                created_date: {
                    [Op.lte]: moment(req.query.to_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        where[Op.and].push({
            created_by: {
                [Op.eq]: req.user_id
            }
        });


        let event = await EventDraft.findAndCountAll({
            where: where,
            // order: sequelize.literal(orderBy),
            offset: parseInt(req.query.page - 1) * parseInt(req.query.per_page),
            limit: parseInt(req.query.per_page),
            distinct: true,
            order: orderBy,
            include: [{
                model: EventDraftRecipient,
                include: [{
                    model: User,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                }],
                required: false
            },
            {
                model: EventType,
                required: false,

            }, {
                model: EventStatusType,
                required: false
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            },
            {
                model: EventDraftTag,
                required: false
            },
            {
                model: User,
                required: false,
                include: [{
                    model: Designation,
                    as: 'designation',
                    required: false,
                }]
            }
            ]
        });
        let resultArr = [];
        event.rows = JSON.parse(JSON.stringify(event.rows));
        for (let index = 0; index < event.rows.length; index++) {
            const element = event.rows[index];
            let To = [];
            let To_Designation = [];

            for (let index1 = 0; index1 < element.event_draft_recipients.length; index1++) {
                const element1 = element.event_draft_recipients[index1];
                if (element1.user) {
                    To.push(element1.user.user_firstname + " " + element1.user.user_firstname);
                    if (element1.user.designation) {
                        To_Designation.push(element1.user.designation.designation_name);
                    }
                }
            }
            resultArr.push({
                event_draft_id: element.event_draft_id,
                event_message_type: element.event_message_type ? element.event_message_type.event_message_type_name : "",
                type: element.event_type ? element.event_type.event_type_name : "",
                from: element.user ? element.user.user_firstname + " " + element.user.user_lastname : '',
                to: To.toString(),
                to_designation: To_Designation.toString(),
                from_designation: element.user && element.user.designation ? element.user.designation.designation_name : "",
                subject: element.subject,
                place: element.event_draft_tag ? element.event_draft_tag.place : "",
                grading: element.grade ? element.grade.grade_name : "",
                date: element.event_draft_tag && element.event_draft_tag.event_date ? element.event_draft_tag.event_date : element.created_date,
                pdf: await EventDraftAttachment.findAll({ where: { event_draft_id: element.event_draft_id } })
            })
        }
        let result = new Object();
        result.data = resultArr;
        result.total = event.count;
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, result))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
async function generateEventNumber(unit_id, section_id, desk_id) {
    let event_last = await Event.findOne({
        order: [
            [
                'event_id', 'DESC'
            ]
        ],
        attributes: ['event_id']
    });

    return event_last ? "EV" + event_last.event_id + 1 : 1;
}

async function DeleteDraft(objEvent) {
    let event_draft = await EventDraft.findAll({
        where: {
            event_draft_id: {
                [Op.eq]: objEvent.event_draft_id
            },
        }
    });
    if (event_draft.length) {
        await EventDraftAttachment.destroy({
            where: {
                event_draft_id: {
                    [Op.eq]: objEvent.event_draft_id
                },
            }
        })
        await EventDraftRecipient.destroy({
            where: {
                event_draft_id: {
                    [Op.eq]: objEvent.event_draft_id
                },
            }
        })
        await EventDraftTag.destroy({
            where: {
                event_draft_id: {
                    [Op.eq]: objEvent.event_draft_id
                },
            }
        })
        await EventDraft.destroy({
            where: {
                event_draft_id: {
                    [Op.eq]: objEvent.event_draft_id
                },
            }
        });
        await models.event_fir_draft.destroy({
            where: {
                event_draft_id: {
                    [Op.eq]: objEvent.event_draft_id
                },
            }

        });
        await models.event_udr_draft.destroy({
            where: {
                event_draft_id: {
                    [Op.eq]: objEvent.event_draft_id
                },
            }

        });
        return true;
    } else {
        return true;
    }
}

function padLeadingZeros(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

async function saveEventDraftTags(objTags, event) {
    try {
        objTags.event_draft_id = event.event_draft_id;
        let saveTags = u.clone(objTags);
        saveTags.organization_id = null;
        saveTags.leader_id = null;
        let create_event_tag = await EventDraftTag.create(saveTags);
        if (create_event_tag) {
            if (objTags.organization_id.length) {
                for (let index = 0; index < objTags.organization_id.length; index++) {
                    const organization_id = objTags.organization_id[index];
                    let org = await models.organizations.findByPk(organization_id);
                    if (org) {
                        await models.event_tag_organization_draft.create({
                            event_draft_id: event.event_draft_id,
                            organization_id: organization_id,
                            is_active: true,
                            created_by: event.created_by,
                            modified_by: event.modified_by,
                            created_date: new Date(),
                            modified_date: new Date()
                        })
                    }
                }
            }
            if (objTags.leader_id.length) {
                for (let index = 0; index < objTags.leader_id.length; index++) {
                    const leader_id = objTags.leader_id[index];
                    let leader = await models.leader.findByPk(leader_id);
                    if (leader) {
                        await models.event_tag_leader_draft.create({
                            event_draft_id: event.event_draft_id,
                            leader_id: leader_id,
                            is_active: true,
                            created_by: event.created_by,
                            modified_by: event.modified_by,
                            created_date: new Date(),
                            modified_date: new Date()
                        })
                    }
                }
            }
            return create_event_tag;
        } else {
            return null;
        }
    } catch (e) {
        console.log(e)

    }
}

const updateEventSummary = async (req, res) => {
    try {
        let { event_id, final_summary } = req.body;

        // Check event_id is exists or not
        let event = await Event.findOne({
            where: {
                event_id: event_id,
            }
        });

        if (!event && event == null) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else {
            event.update({
                final_summary: final_summary
            });

            await createEventLogs({
                event_id: event_id,
                event_forward_id: null,
                user_id: req.user_id,
                created_by: req.user_id
            }, "Update");

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_SUMMARY_UPDATED, event))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
};
const addEventAttachment = async (req, res) => {
    try {
        let event_attachment = [];
        if (!req.body.event_id) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EMPTY_EVENT_ID, null))
        }
        if (req.files && req.files.event_attachment && req.files.event_attachment.length) {
            event_attachment = req.files && req.files.event_attachment && req.files.event_attachment.length ? req.files.event_attachment : [];
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EMPTY_EVENT_ATTACHMENT, null))
        }

        for (let index = 0; index < event_attachment.length; index++) {
            const obj = event_attachment[index];
            await EventAttachment.create({
                event_id: req.body.event_id,
                attachment_name: obj.originalname,
                attachment_extension: obj.originalname.split('.').pop(),
                attachment_size: obj.size,
                attachment_path: obj.location,
                created_by: 0,
            });
        }

        await createEventLogs({
            event_id: req.body.event_id,
            event_forward_id: null,
            user_id: req.user_id,
            created_by: req.user_id
        }, "Upload");

        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.SAVE_SUCCESS_ATTACHMENT, null));
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }

}

const updateOtherTags = async (req, res) => {
    try {
        let body = req.body;
        // Check event_id is exists or not
        let event = await Event.findOne({
            where: {
                event_id: body.event_id,
            }
        });

        if (!event && event == null) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else {
            // let eventTags = event.other_tags ? event.other_tags : "";
            // let newTags = eventTags.length ? eventTags + ',' + body.other_tags : body.other_tags;
            try {
                await event.update({
                    other_tags: body.other_tags
                }).catch(function (err) { console.log(err) });
            } catch (ex) {
                console.log(ex)
            }
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_CREATED, null));
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const updateEventStatus = async (req, res) => {
    try {
        let body = req.body;
        // Check event_id is exists or not
        let event = await Event.findOne({
            where: {
                event_id: body.event_id,
            }
        });
        var event_status = await EventStatusType.findOne({
            where: {
                event_status_type_name: body.event_status_type_name
            }
        })

        if (!event && event == null) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else if (!event_status) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_STATUS_NOT_EXISTS, null));
        } else {
            await Event.update({
                event_status_type_id: event_status.event_status_type_id,
                modified_date: new Date(),
            }, {
                where: {
                    event_id: body.event_id,
                }
            });

            await createEventLogs({
                event_id: body.event_id,
                event_forward_id: null,
                user_id: req.user_id,
                created_by: req.user_id
            }, "Update");

            return res.json(await commonResponse.responseReturn(true, req, "Event moved to " + body.event_status_type_name + " Successfully!!", null));
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const printPdf = async (req, res) => {
    try {

        let where = {};
        where[Op.and] = [];
        where[Op.and].push({
            event_id: {
                [Op.eq]: req.params.event_id
            }
        });

        let event = await Event.findOne({
            where: where,
            include: [{
                model: EventRecipient,
                include: [{
                    model: Users,
                    required: false,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                },
                {
                    model: RecipientType,
                    required: false
                }
                ],
                required: false
            },
            {
                model: EventType,
                required: false
            }, {
                model: EventStatusType,
                required: false
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            }, {
                model: EventForward,
                required: false,
                include: [{
                    model: FordStatusType,
                    attributes: ['forward_status_name'],
                    required: false,
                },
                    //  {
                    //     model: EventForwardAttachment,
                    //     required: false,
                    // }
                ]
            }
            ]
        });
        if (!event) {
            return res.json({ message: 'Event not exist in our records!' });
        }
        event = event ? JSON.parse(JSON.stringify(event)) : null;
        try {
            event.from = await User.findByPk(event.created_by);

        } catch (e) {
            event.from = "";
        }
        try {
            if (event.from) {
                event.from_designation = await Designation.findByPk(event.from.designation_id)
                event.from_designation = event.from_designation ? event.from_designation.designation_name : ""
            }
        } catch (e) {

        }
        event.from = event.from ? event.from.user_email : "";
        if (event && event.event_forwards.length) {
            for (let index = 0; index < event.event_forwards.length; index++) {
                const element = event.event_forwards[index];
                element.event_forward_attachment = await EventForwardAttachment.findAll({ where: { event_forward_id: element.event_forward_id } })
                element.event_forward_recipients = await EventForwardRecipient.findAll({
                    where: {
                        event_forward_id: element.event_forward_id

                    },
                    include: [{
                        model: Users,
                        required: false,
                        include: [{
                            model: Designation,
                            as: 'designation'
                        }]
                    },
                    {
                        model: RecipientType,
                        required: false
                    }
                    ],
                });

                element.event_forward_recipients = JSON.parse(JSON.stringify(element.event_forward_recipients));

                for (let j = 0; j < element.event_forward_recipients.length; j++) {
                    const element1 = element.event_forward_recipients[j];
                    element1.to = element1.user ? element1.user.user_email : "";
                    element1.designation = element1.user && element1.user.designation ? element1.user.designation.designation_name : "";
                    element1.recipient_type_name = element1.recipient_type ? element1.recipient_type.recipient_type_name : ""
                    delete element1.recipient_type;
                    delete element1.user;

                }
            }
        }

        event.event_tags = await EventTag.findOne({
            where: {
                event_id: event.event_id,
            },
            include: [{
                model: models.district
            }, {
                model: models.leader
            }, {
                model: models.organizations
            }],
        })
        let To = [];
        let To_Designation = [];
        let CC_Designation = [];
        for (let index1 = 0; index1 < event.event_recipients.length; index1++) {
            const element1 = event.event_recipients[index1];

            if (element1.user && element1.recipient_type && element1.recipient_type.recipient_type_name.toLowerCase() == "to") {
                To.push(element1.user.user_email);
                if (element1.user.designation) {
                    To_Designation.push(element1.user.designation.designation_name);
                }
            }
            if (element1.user && element1.recipient_type && element1.recipient_type.recipient_type_name.toLowerCase() == "cc") {
                if (element1.user.designation) {
                    CC_Designation.push(element1.user.designation.designation_name);
                }
            }
        }

        event.ccDesignation = CC_Designation.join(', ');
        event.toDesignation = To_Designation.join(', ');
        event.createdAt = moment(event.created_date).format('DD MMM YYYY hh:mm');
        event.eventDate = (event.event_tags && event.event_tags.event_date) ? moment(event.event_tags.event_date).format('DD-MM-YYYY') : '---';
        event.eventTime = (event.event_tags && event.event_tags.event_time) ? moment(event.event_tags.event_time, 'hh:mm:ss').format('hh:mm A') : '---';

        let forwardHtml = ``;

        for (let index = 0; index < event.event_forwards.length; index++) {
            let event_forward = event.event_forwards[index];
            let event_forward_des = u.pluck(event_forward.event_forward_recipients, 'designation');

            forwardHtml += ` <!-- Forword List -->
                <div class="forword-list" style="padding: 0px; overflow-y: auto; max-height: 100%; border-top: 0px;">
                    <div class="forword-list-item" style="border-top: 0px; padding: 18px 0px;">
                        <div class="d-flex">
                            <div class="inbox-profile mb-20"
                                style="margin-bottom: 20px; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-align: center; -ms-flex-align: center; align-items: center; min-width: 300px;">
                                <svg width="50" height="50" viewBox="0 0 50 50"
                                    style="height: 50px; width: 50px; -o-object-fit: cover; object-fit: cover; border-radius: 50%;">
                                    <defs>
                                        <style>
                                            .a {
                                                fill: #ebeef6;
                                            }
                
                                            .b {
                                                fill: #181c32;
                                            }
                                        </style>
                                    </defs>
                                    <g transform="translate(-326 -202)">
                                        <rect class="a" width="50" height="50" rx="5" transform="translate(326 202)" />
                                        <g transform="translate(341.083 215.113)">
                                            <path class="b"
                                                d="M92.758,11.452a5.54,5.54,0,0,0,4.049-1.678,5.541,5.541,0,0,0,1.677-4.048,5.541,5.541,0,0,0-1.677-4.048,5.724,5.724,0,0,0-8.1,0,5.54,5.54,0,0,0-1.678,4.048,5.541,5.541,0,0,0,1.678,4.049A5.542,5.542,0,0,0,92.758,11.452ZM89.694,2.663a4.331,4.331,0,0,1,6.127,0A4.14,4.14,0,0,1,97.09,5.726a4.139,4.139,0,0,1-1.269,3.063,4.331,4.331,0,0,1-6.127,0,4.139,4.139,0,0,1-1.27-3.063,4.139,4.139,0,0,1,1.27-3.063Zm0,0"
                                                transform="translate(-82.991 0)" />
                                            <path class="b"
                                                d="M19.785,254.113a14.142,14.142,0,0,0-.193-1.5,11.834,11.834,0,0,0-.369-1.51,7.46,7.46,0,0,0-.621-1.409,5.31,5.31,0,0,0-.936-1.22,4.128,4.128,0,0,0-1.345-.845,4.648,4.648,0,0,0-1.717-.311,1.743,1.743,0,0,0-.931.395c-.279.182-.605.392-.969.625a5.555,5.555,0,0,1-1.254.553,4.871,4.871,0,0,1-3.068,0,5.539,5.539,0,0,1-1.253-.552c-.361-.231-.687-.441-.97-.625a1.741,1.741,0,0,0-.93-.395,4.642,4.642,0,0,0-1.717.311,4.124,4.124,0,0,0-1.345.845,5.311,5.311,0,0,0-.936,1.22A7.474,7.474,0,0,0,.609,251.1a11.863,11.863,0,0,0-.369,1.51,14.046,14.046,0,0,0-.193,1.5c-.032.454-.048.926-.048,1.4A3.947,3.947,0,0,0,1.172,258.5a4.22,4.22,0,0,0,3.021,1.1H15.64a4.22,4.22,0,0,0,3.021-1.1,3.946,3.946,0,0,0,1.173-2.987c0-.479-.016-.951-.048-1.4ZM17.7,257.495a2.841,2.841,0,0,1-2.061.718H4.193a2.841,2.841,0,0,1-2.061-.718,2.578,2.578,0,0,1-.74-1.977c0-.445.015-.885.044-1.308a12.669,12.669,0,0,1,.174-1.353,10.463,10.463,0,0,1,.325-1.331,6.085,6.085,0,0,1,.505-1.145,3.936,3.936,0,0,1,.688-.9,2.738,2.738,0,0,1,.895-.556,3.206,3.206,0,0,1,1.1-.212c.049.026.136.075.276.167.286.187.616.4.981.633a6.9,6.9,0,0,0,1.574.7,6.263,6.263,0,0,0,3.925,0,6.909,6.909,0,0,0,1.575-.7c.373-.239.694-.445.981-.632.141-.092.228-.141.276-.167a3.208,3.208,0,0,1,1.1.212,2.742,2.742,0,0,1,.895.556,3.926,3.926,0,0,1,.688.9,6.064,6.064,0,0,1,.505,1.145,10.438,10.438,0,0,1,.325,1.331,12.777,12.777,0,0,1,.174,1.353h0c.03.421.044.86.045,1.307a2.577,2.577,0,0,1-.74,1.977Zm0,0"
                                                transform="translate(0 -235.833)" />
                                        </g>
                                    </g>
                                </svg>
                                <div class="inbox-profile-detail" style="margin-left: 10px;">
                                    <h5 class="fnts-18" style="font-size: 15px;color: #3A7AFE;margin-bottom: 0px;">${event.from_designation}</h5>
                                    <p style="font-size: 12px;margin-bottom: 0px;">${moment(event.created_date).format('DD MMM YYYY hh:mm')}</p>
                                </div>
                            </div>
                            <div class="ml-auto status" style="min-width: auto; margin-bottom: 10px;">
                                <label class="ftw-500 mb-0" style="font-weight: 500;">Status: ${event_forward.forward_status_type.forward_status_name}</label>
                            </div>
                        </div>
                
                        <div class="mb-20" style="margin-bottom: 10px;">
                            <label class="ftw-500 mb-0" style="font-weight: 500;">To: ${event_forward_des}</label>
                        </div>
                
                        <div>
                            <label class="ftw-500 mb-0" style="font-weight: 500;">Due Date: ${moment(event_forward.event_due_date).format('DD-MM-YYYY')}</label>
                        </div>
                
                        <p class="mb-0">${event_forward.event_instructions}</p>
                    </div>
                </div>`;
        }

        event.forwardHtml = forwardHtml;

        // Generate HTML from here
        await ejs.renderFile(path.join(__dirname, '../../templates/', "event_details.template.ejs"), event, async (err, html) => {
            if (err) {
                return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
            } else {
                let options = {
                    "format": "A4", // allowed units: A3, A4, A5, Legal, Letter, Tabloid
                    "orientation": "portrait", // portrait or landscape
                    "header": {
                        "height": "30px",
                    },
                    "footer": {
                        "height": "30px",
                        "contents": {
                            default: '<hr><div style="text-align: center;">Secret and Confidential - State Intelligence, Karnataka</div>', // fallback value
                        }
                    },
                };

                htmlPdf.create(html, options).toStream(async (err, stream) => {
                    if (err) {
                        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
                    } else {

                        await createEventLogs({
                            event_id: req.params.event_id,
                            event_forward_id: null,
                            user_id: req.user_id,
                            created_by: req.user_id
                        }, "Download");

                        res.set('Content-type', 'application/pdf');
                        stream.pipe(res);
                    }
                });
            }
        });
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const getEventInboxWithPagination = async (req, res) => {
    try {
        let orderBy = []
        for (var i in req.query) {
            req.query[i] = req.query[i] == 'null' || req.query[i] == 'undefined' ? null : req.query[i];
        }

        if (req.query.filed_name && req.query.order_by) {
            if (req.query.filed_name == 'type') {
                orderBy.push([EventType, 'event_type_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'grading') {
                orderBy.push([Grade, 'grade_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'place') {
                orderBy.push([EventTag, 'place', req.query.order_by]);
            }
            if (req.query.filed_name == 'date') {
                orderBy.push(['created_date', req.query.order_by]);
            }
            if (req.query.filed_name == 'subject') {
                orderBy.push(['subject', req.query.order_by]);
            }
            if (req.query.filed_name == 'from') {
                orderBy.push([User, 'user_firstname', req.query.order_by]);
            }
        } else {
            orderBy.push(['created_date', req.query.order_by]);
        }
        let where = {};
        where[Op.and] = [];
        if (req.query.event_type_id) {
            where[Op.and].push({
                event_type_id: {
                    [Op.eq]: req.query.event_type_id
                }
            });
        }
        if (req.query.event_status_type_id) {
            where[Op.and].push({
                event_status_type_id: {
                    [Op.eq]: req.query.event_status_type_id
                }
            });
        }
        if (req.query.unit_id) {
            where[Op.and].push({
                unit_id: {
                    [Op.eq]: req.query.unit_id
                }
            });
        }
        if (req.query.section_id) {
            where[Op.and].push({
                section_id: {
                    [Op.eq]: req.query.section_id
                }
            });
        }
        if (req.query.desk_id) {
            where[Op.and].push({
                desk_id: {
                    [Op.eq]: req.query.desk_id
                }
            });
        }
        if (req.query.event_message_type_id) {
            where[Op.and].push({
                event_message_type_id: {
                    [Op.eq]: req.query.event_message_type_id
                }
            });
        }
        if (req.query.event_message_sub_type_id) {
            where[Op.and].push({
                event_message_sub_type_id: {
                    [Op.eq]: req.query.event_message_sub_type_id
                }
            });
        }
        if (req.query.grade_id) {
            where[Op.and].push({
                grade_id: {
                    [Op.eq]: req.query.grade_id
                }
            });
        }
        if (req.query.subject) {
            where[Op.and].push({
                subject: {
                    [Op.iLike]: '%' + req.query.subject + '%'
                }
            });
        }
        if (req.query.from_date) {
            where[Op.and].push({
                created_date: {
                    [Op.gte]: moment(req.query.from_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        if (req.query.to_date) {
            where[Op.and].push({
                created_date: {
                    [Op.lte]: moment(req.query.to_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        // let whereTags = {};
        // whereTags[Op.and] = [];
        // let tags_required = false;
        // if (req.query.place) {
        //     tags_required = true;
        //     whereTags[Op.and].push(sequelize.literal('event_tag.place like "%' + req.query.state_id + '"%'));
        // }
        // if (req.query.district_id) {
        //     tags_required = true;
        //     whereTags[Op.and].push(sequelize.literal('event_tag.district_id = ' + req.query.district_id));
        // }
        // if (req.query.state_id) {
        //     tags_required = true;
        //     whereTags[Op.and].push(sequelize.literal('event_tag.state_id = ' + req.query.state_id));
        // }

        let event = await Event.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(req.query.page - 1) * parseInt(req.query.per_page),
            limit: parseInt(req.query.per_page),
            distinct: true,
            include: [{
                model: EventRecipient,
                where: {
                    user_id: req.user_id
                },
                include: [{
                    model: User,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                }],
                required: true
            },
            {

                model: EventType,
                required: false
            }, {
                model: EventStatusType,
                where: {
                    event_status_type_name: {
                        [Op.notIn]: ["Deleted", "Archive"]
                    }
                },
                required: true
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            },
            {
                model: EventTag,
                required: false,
            },
            {
                model: User,
                required: false,
                include: [{
                    model: Designation,
                    as: 'designation',
                    required: false,
                }]
            }
            ]
        });

        let resultArr = [];
        event.rows = JSON.parse(JSON.stringify(event.rows));
        for (let index = 0; index < event.rows.length; index++) {


            const element = event.rows[index];
            console.log(element.event_tag)
            let To = [];
            let To_Designation = [];

            for (let index1 = 0; index1 < element.event_recipients.length; index1++) {
                const element1 = element.event_recipients[index1];
                if (element1.user) {
                    To.push(element1.user.user_email);
                    if (element1.user.designation) {
                        To_Designation.push(element1.user.designation.designation_name);
                    }
                }
            }
            resultArr.push({
                event_id: element.event_id,
                event_message_type: element.event_message_type ? element.event_message_type.event_message_type_name : "",
                type: element.event_type ? element.event_type.event_type_name : "",
                from: element.user ? element.user.user_firstname + " " + element.user.user_lastname : '',
                to: To.toString(),
                to_designation: To_Designation.toString(),
                from_designation: element.user && element.user.designation ? element.user.designation.designation_name : "",
                subject: element.subject,
                place: element.event_tag ? element.event_tag.place : "",
                grading: element.grade ? element.grade.grade_name : "",
                date: element.created_date,
                pdf: await EventAttachment.findAll({ where: { event_id: element.event_id } })
            })
        }
        let result = new Object();
        result.data = resultArr;
        result.total = event.count;
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, result))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
};

const updateEventUnitSectionDesk = async (req, res) => {
    try {
        let { event_id, unit_id, section_id, desk_id } = req.body;

        // Check event_id is exists or not
        let event = await Event.findOne({
            where: {
                event_id: event_id,
            }
        });

        if (!event && event == null) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else {
            event.update({ unit_id, section_id, desk_id });

            await createEventLogs({
                event_id: event_id,
                event_forward_id: null,
                user_id: req.user_id,
                created_by: req.user_id
            }, "Update");

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_UPDATED, event))
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const getEventSentWithPagination = async (req, res) => {
    try {
        let orderBy = []
        for (var i in req.query) {
            req.query[i] = req.query[i] == 'null' || req.query[i] == 'undefined' ? null : req.query[i];
        }

        if (req.query.filed_name && req.query.order_by) {
            if (req.query.filed_name == 'type') {
                orderBy.push([EventType, 'event_type_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'grading') {
                orderBy.push([Grade, 'grade_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'place') {
                orderBy.push([EventTag, 'place', req.query.order_by]);
            }
            if (req.query.filed_name == 'date') {
                orderBy.push(['created_date', req.query.order_by]);
            }
            if (req.query.filed_name == 'subject') {
                orderBy.push(['subject', req.query.order_by]);
            }
            if (req.query.filed_name == 'from') {
                orderBy.push([User, 'user_firstname', req.query.order_by]);
            }
        } else {
            orderBy.push(['created_date', req.query.order_by]);
        }
        let where = {};
        where[Op.and] = [];
        if (req.query.event_type_id) {
            where[Op.and].push({
                event_type_id: {
                    [Op.eq]: req.query.event_type_id
                }
            });
        }
        if (req.query.event_status_type_id) {
            where[Op.and].push({
                event_status_type_id: {
                    [Op.eq]: req.query.event_status_type_id
                }
            });
        }
        if (req.query.unit_id) {
            where[Op.and].push({
                unit_id: {
                    [Op.eq]: req.query.unit_id
                }
            });
        }
        if (req.query.section_id) {
            where[Op.and].push({
                section_id: {
                    [Op.eq]: req.query.section_id
                }
            });
        }
        if (req.query.desk_id) {
            where[Op.and].push({
                desk_id: {
                    [Op.eq]: req.query.desk_id
                }
            });
        }
        if (req.query.event_message_type_id) {
            where[Op.and].push({
                event_message_type_id: {
                    [Op.eq]: req.query.event_message_type_id
                }
            });
        }
        if (req.query.event_message_sub_type_id) {
            where[Op.and].push({
                event_message_sub_type_id: {
                    [Op.eq]: req.query.event_message_sub_type_id
                }
            });
        }
        if (req.query.grade_id) {
            where[Op.and].push({
                grade_id: {
                    [Op.eq]: req.query.grade_id
                }
            });
        }
        if (req.query.subject) {
            where[Op.and].push({
                subject: {
                    [Op.iLike]: '%' + req.query.subject + '%'
                }
            });
        }
        if (req.query.from_date) {
            where[Op.and].push({
                created_date: {
                    [Op.gte]: moment(req.query.from_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        if (req.query.to_date) {
            where[Op.and].push({
                created_date: {
                    [Op.lte]: moment(req.query.to_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }

        let whereTags = {};
        whereTags[Op.and] = [];
        let tags_required = false;
        if (req.query.place) {
            tags_required = true;
            whereTags[Op.and].push({
                place: {
                    [Op.eq]: req.query.place
                }
            });
        }
        if (req.query.district_id) {
            tags_required = true;
            whereTags[Op.and].push({
                district_id: {
                    [Op.eq]: req.query.district_id
                }
            });
        }
        if (req.query.state_id) {
            tags_required = true;
            whereTags[Op.and].push({
                state_id: {
                    [Op.eq]: req.query.state_id
                }
            });
        }

        let event = await Event.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(req.query.page - 1) * parseInt(req.query.per_page),
            limit: parseInt(req.query.per_page),
            distinct: true,
            include: [{
                model: EventRecipient,
                where: {
                    created_by: req.user_id
                },
                include: [{
                    model: User,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                }],
                required: true
            },
            {

                model: EventType,
                required: false
            }, {
                model: EventStatusType,
                where: {
                    event_status_type_name: {
                        [Op.eq]: "Sent"
                    }
                },
                required: true
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            },
            {
                model: EventTag,
                where: whereTags,
                required: tags_required
            },
            {
                model: User,
                required: false,
                include: [{
                    model: Designation,
                    as: 'designation',
                    required: false,
                }]
            }
            ]
        });

        let resultArr = [];
        event.rows = JSON.parse(JSON.stringify(event.rows));
        for (let index = 0; index < event.rows.length; index++) {
            const element = event.rows[index];
            let To = [];
            let To_Designation = [];

            for (let index1 = 0; index1 < element.event_recipients.length; index1++) {
                const element1 = element.event_recipients[index1];
                if (element1.user) {
                    To.push(element1.user.user_email);
                    if (element1.user.designation) {
                        To_Designation.push(element1.user.designation.designation_name);
                    }
                }
            }
            resultArr.push({
                event_id: element.event_id,
                event_message_type: element.event_message_type ? element.event_message_type.event_message_type_name : "",
                type: element.event_type ? element.event_type.event_type_name : "",
                from: element.user ? element.user.user_firstname + " " + element.user.user_lastname : '',
                to: To.toString(),
                to_designation: To_Designation.toString(),
                from_designation: element.user && element.user.designation ? element.user.designation.designation_name : "",
                subject: element.subject,
                place: element.event_tag ? element.event_tag.place : "",
                grading: element.grade ? element.grade.grade_name : "",
                date: element.created_date,
                pdf: await EventAttachment.findAll({ where: { event_id: element.event_id } })
            })
        }
        let result = new Object();
        result.data = resultArr;
        result.total = event.count;
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, result))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const deleteDraft = async (req, res) => {
    try {
        let objEvent = req.body;
        let event_draft = await EventDraft.findAll({
            where: {
                event_draft_id: {
                    [Op.in]: objEvent.event_draft_id
                },
            }
        });
        if (event_draft.length) {
            await EventDraftAttachment.destroy({
                where: {
                    event_draft_id: {
                        [Op.in]: objEvent.event_draft_id
                    },
                }
            })
            await EventDraftRecipient.destroy({
                where: {
                    event_draft_id: {
                        [Op.in]: objEvent.event_draft_id
                    },
                }
            })
            await EventDraftTag.destroy({
                where: {
                    event_draft_id: {
                        [Op.in]: objEvent.event_draft_id
                    },
                }
            })
            await EventDraft.destroy({
                where: {
                    event_draft_id: {
                        [Op.in]: objEvent.event_draft_id
                    },
                }
            });
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_DRAFT_REMOVE, null));
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_INVALID_DRAFT_ID, null));
        }

    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const getEventActionDueWithPagination = async (req, res) => {
    try {
        let orderBy = 'created_date DESC';
        if (req.query.filed_name && req.query.order_by) {
            orderBy = (req.query.filed_name + " " + req.query.order_by);
        }
        let where = {};
        where[Op.and] = [];
        if (req.query.event_type_id) {
            where[Op.and].push({
                event_type_id: {
                    [Op.eq]: req.query.event_type_id
                }
            });
        }
        if (req.query.event_status_type_id) {
            where[Op.and].push({
                event_status_type_id: {
                    [Op.eq]: req.query.event_status_type_id
                }
            });
        }
        if (req.query.unit_id) {
            where[Op.and].push({
                unit_id: {
                    [Op.eq]: req.query.unit_id
                }
            });
        }
        if (req.query.section_id) {
            where[Op.and].push({
                section_id: {
                    [Op.eq]: req.query.section_id
                }
            });
        }
        if (req.query.desk_id) {
            where[Op.and].push({
                desk_id: {
                    [Op.eq]: req.query.desk_id
                }
            });
        }
        if (req.query.event_message_type_id) {
            where[Op.and].push({
                event_message_type_id: {
                    [Op.eq]: req.query.event_message_type_id
                }
            });
        }
        if (req.query.event_message_sub_type_id) {
            where[Op.and].push({
                event_message_sub_type_id: {
                    [Op.eq]: req.query.event_message_sub_type_id
                }
            });
        }
        if (req.query.grade_id) {
            where[Op.and].push({
                grade_id: {
                    [Op.eq]: req.query.grade_id
                }
            });
        }
        if (req.query.subject) {
            where[Op.and].push({
                subject: {
                    [Op.iLike]: '%' + req.query.subject + '%'
                }
            });
        }
        if (req.query.from_date) {
            where[Op.and].push({
                created_date: {
                    [Op.gte]: moment(req.query.from_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        if (req.query.to_date) {
            where[Op.and].push({
                created_date: {
                    [Op.lte]: moment(req.query.to_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }


        let event = await EventForwardRecipient.findAndCountAll({
            where: {
                [Op.or]: {
                    created_by: req.query.user_id,
                    user_id: req.query.user_id,
                }
            },
            order: sequelize.literal(orderBy),
            offset: parseInt(req.query.page - 1) * parseInt(req.query.per_page),
            limit: parseInt(req.query.per_page),
            include: [{
                model: EventForward,
                required: true,
                include: [{
                    model: Event,
                    where: where,
                    required: true,
                    include: [{
                        model: EventType,
                        required: false
                    }, {
                        model: EventStatusType,
                        required: false
                    }, {
                        model: Unit,
                        required: false
                    }, {
                        model: Section,
                        required: false
                    }, {
                        model: Desk,
                        required: false
                    }, {
                        model: EventMessageType,
                        required: false
                    }, {
                        model: EventMessageSubType,
                        required: false
                    }, {
                        model: Grade,
                        required: false
                    }]
                }]
            }]
        });

        let resultArr = [];
        for (let index = 0; index < event.rows.length; index++) {
            const element = event.rows[index];
            let username = await Users.findOne({ where: { user_id: element.event_forward.event.created_by } })
            resultArr.push({
                event_id: element.event_forward.event.event_id,
                type: element.event_forward.event.event_type ? element.event_forward.event.event_type.event_type_name : "",
                from: username ? username.user_firstname + " " + username.user_lastname : "",
                // from: "",
                status: element.user_id == req.query.user_id ? "Inword" : "Outword",
                forward_to: element.user_id,
                subject: element.event_forward.event.subject,
                place: "",
                grading: element.event_forward.event.grade ? element.event_forward.event.grade.grade_name : "",
                event_due_date: element.event_forward.event_due_date,
                date: element.event_forward.event.created_date,
                pdf: await EventForwardAttachment.findAll({ where: { event_forward_id: element.event_forward.event_forward_id } })
            })
        }
        let result = new Object();
        result.data = resultArr;
        result.total = event.count;
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, result))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const deleteSentEvent = async (req, res) => {
    try {
        let objEvent = req.params;
        let event = await Event.findOne({
            where: {
                event_id: objEvent.event_id
            }
        });
        if (event) {
            await EventAttachment.destroy({
                where: {
                    event_id: objEvent.event_id
                }
            })
            await EventRecipient.destroy({
                where: {
                    event_id: objEvent.event_id
                }
            })
            await EventTag.destroy({
                where: {
                    event_id: objEvent.event_id
                }
            })

            let event_forward = await EventForward.findAll({
                where: {
                    event_id: objEvent.event_id
                },
                attributes: ['event_forward_id']
            })
            let ids = [];
            for (let index = 0; index < event_forward.length; index++) {
                ids.push(event_forward[index].event_forward_id);

            }
            if (ids.length) {
                await EventForwardAttachment.destroy({
                    where: {
                        event_forward_id: {
                            [Op.in]: ids
                        }
                    }
                })
                await EventForwardRecipient.destroy({
                    where: {
                        event_forward_id: {
                            [Op.in]: ids
                        }
                    }
                })
            }
            await EventForward.destroy({
                where: {
                    event_id: objEvent.event_id
                }
            })
            await event.destroy({
                where: {
                    event_id: objEvent.event_id
                }
            });

            await createEventLogs({
                event_id: objEvent.event_id,
                event_forward_id: null,
                user_id: req.user_id,
                created_by: req.user_id
            }, "Delete");

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_REMOVE, null));
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_INVALID_EVENT_ID, null));
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const addEventTags = async (req, res) => {
    try {
        let body = req.body;
        // Check event_id is exists or not
        let event = await Event.findAll({
            where: {
                event_id: {
                    [Op.in]: body.event_id
                },
            },
            attributes: ['event_id']
        });
        if (!event.length) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else {
            event = JSON.parse(JSON.stringify(event));
            event = u.pluck(event, 'event_id');
            let tags = await Tag.findAll({
                where: {
                    tag_id: {
                        [Op.in]: body.tag_id
                    },
                    tag_name: {
                        [Op.ne]: null
                    }
                },
                attributes: ['tag_name']
            })
            let other_tags_append = [];
            if (tags && tags.length) {
                other_tags_append = u.pluck(tags, 'tag_name');

                for (let index = 0; index < event.length; index++) {
                    const element = event[index];

                    let event_find = await Event.findOne({
                        where: {
                            event_id: element
                        },
                        attributes: ['event_id', 'other_tags']
                    })
                    if (event_find) {
                        if (!event_find.other_tags) {
                            event_find.other_tags = "";
                        }
                        let other_tags = [];
                        if (event_find.other_tags != "" && event_find.other_tags != null) {
                            other_tags = event_find.other_tags.split(',');
                        }
                        for (let index = 0; index < other_tags_append.length; index++) {
                            const element = other_tags_append[index];
                            if (element) {
                                other_tags.push(element);
                            }
                        }
                        other_tags = u.unique(other_tags);
                        await event_find.update({
                            other_tags: other_tags.join(',')
                        })
                    }
                }

                await createEventLogs({
                    event_id: body.event_id,
                    event_forward_id: null,
                    user_id: req.user_id,
                    created_by: req.user_id
                }, "Update");

                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_CREATED, null));
            } else {
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_NOT_EXISTS, null));
            }
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }

        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const addEventDraftTags = async (req, res) => {
    try {
        let body = req.body;
        // Check event_id is exists or not
        let event = await EventDraft.findAll({
            where: {
                event_draft_id: {
                    [Op.in]: body.event_draft_id
                },
            },
            attributes: ['event_draft_id']
        });
        if (!event.length) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else {
            event = JSON.parse(JSON.stringify(event));
            event = u.pluck(event, 'event_draft_id');
            let tags = await Tag.findAll({
                where: {
                    tag_id: {
                        [Op.in]: body.tag_id
                    },
                    tag_name: {
                        [Op.ne]: null
                    }
                },
                attributes: ['tag_name']
            })
            let other_tags_append = [];
            if (tags && tags.length) {
                other_tags_append = u.pluck(tags, 'tag_name');

                for (let index = 0; index < event.length; index++) {
                    const element = event[index];

                    let event_find = await EventDraft.findOne({
                        where: {
                            event_draft_id: element
                        },
                        attributes: ['event_draft_id', 'other_tags']
                    })
                    if (event_find) {
                        if (!event_find.other_tags) {
                            event_find.other_tags = "";
                        }
                        let other_tags = event_find.other_tags.split(',');
                        for (let index = 0; index < other_tags_append.length; index++) {
                            const element = other_tags_append[index];
                            other_tags.push(element);
                        }
                        other_tags = u.unique(other_tags);
                        console.log(element, other_tags)
                        await event_find.update({
                            other_tags: other_tags.join(',')
                        })
                    }
                }
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_CREATED, null));
            } else {
                return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_NOT_EXISTS, null));
            }
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const deleteInboxEvent = async (req, res) => {
    let objEvent = req.params;
    let event = await EventRecipient.findOne({
        where: {
            event_id: objEvent.event_id,
            user_id: objEvent.user_id
        }
    });
    if (event) {
        await EventRecipient.destroy({
            where: {
                event_id: objEvent.event_id
            }
        });

        await createEventLogs({
            event_id: objEvent.event_id,
            event_forward_id: null,
            user_id: req.user_id,
            created_by: req.user_id
        }, "Delete");

        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_REMOVE, null));
    } else {
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_INVALID_EVENT_ID, null));
    }
}
const updateEventsOtherTags = async (req, res) => {
    try {
        let body = req.body;
        // Check event_id is exists or not
        let event = await Event.findAll({
            where: {
                event_id: {
                    [Op.in]: body.event_id,
                },
            }
        });

        if (!event && !event.length) {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_NOT_EXISTS, null));
        } else {
            event = JSON.parse(JSON.stringify(event));
            for (let index = 0; index < event.length; index++) {
                const element = event[index];
                // let eventTags = element.other_tags ? element.other_tags : "";
                // let newTags = eventTags.length ? eventTags + ',' + body.other_tags : body.other_tags;
                await Event.update({
                    other_tags: body.other_tags
                }, {
                    where: {
                        event_id: {
                            [Op.eq]: element.event_id,
                        },
                    }
                });
            }

            await createEventLogs({
                event_id: body.event_id,
                event_forward_id: null,
                user_id: req.user_id,
                created_by: req.user_id
            }, "Update");

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_TAG_CREATED, null));
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const getEventArchiveWithPagination = async (req, res) => {
    try {
        for (var i in req.query) {
            req.query[i] = req.query[i] == 'null' || req.query[i] == 'undefined' ? null : req.query[i];
        }
        let orderBy = []
        if (req.query.filed_name && req.query.order_by) {
            if (req.query.filed_name == 'type') {
                orderBy.push([EventType, 'event_type_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'grading') {
                orderBy.push([Grade, 'grade_name', req.query.order_by]);
            }
            if (req.query.filed_name == 'place') {
                orderBy.push([EventTag, 'place', req.query.order_by]);
            }
            if (req.query.filed_name == 'date') {
                orderBy.push(['created_date', req.query.order_by]);
            }
            if (req.query.filed_name == 'subject') {
                orderBy.push(['subject', req.query.order_by]);
            }
            if (req.query.filed_name == 'from') {
                orderBy.push([User, 'user_firstname', req.query.order_by]);
            }
        } else {
            orderBy.push(['created_date', req.query.order_by]);
        }
        let where = {};
        where[Op.and] = [];
        if (req.query.event_type_id) {
            where[Op.and].push({
                event_type_id: {
                    [Op.eq]: req.query.event_type_id
                }
            });
        }
        if (req.query.event_status_type_id) {
            where[Op.and].push({
                event_status_type_id: {
                    [Op.eq]: req.query.event_status_type_id
                }
            });
        }
        if (req.query.unit_id) {
            where[Op.and].push({
                unit_id: {
                    [Op.eq]: req.query.unit_id
                }
            });
        }
        if (req.query.section_id) {
            where[Op.and].push({
                section_id: {
                    [Op.eq]: req.query.section_id
                }
            });
        }
        if (req.query.desk_id) {
            where[Op.and].push({
                desk_id: {
                    [Op.eq]: req.query.desk_id
                }
            });
        }
        if (req.query.event_message_type_id) {
            where[Op.and].push({
                event_message_type_id: {
                    [Op.eq]: req.query.event_message_type_id
                }
            });
        }
        if (req.query.event_message_sub_type_id) {
            where[Op.and].push({
                event_message_sub_type_id: {
                    [Op.eq]: req.query.event_message_sub_type_id
                }
            });
        }
        if (req.query.grade_id) {
            where[Op.and].push({
                grade_id: {
                    [Op.eq]: req.query.grade_id
                }
            });
        }
        if (req.query.subject) {
            where[Op.and].push({
                subject: {
                    [Op.iLike]: '%' + req.query.subject + '%'
                }
            });
        }
        if (req.query.from_date) {
            where[Op.and].push({
                created_date: {
                    [Op.gte]: moment(req.query.from_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }
        if (req.query.to_date) {
            where[Op.and].push({
                created_date: {
                    [Op.lte]: moment(req.query.to_date).format('YYYY-MM-DD hh:mm:ss')
                }
            });
        }

        let whereTags = {};
        whereTags[Op.and] = [];
        let tags_required = false;
        if (req.query.place) {
            tags_required = true;
            whereTags[Op.and].push({
                place: {
                    [Op.eq]: req.query.place
                }
            });
        }
        if (req.query.district_id) {
            tags_required = true;
            whereTags[Op.and].push({
                district_id: {
                    [Op.eq]: req.query.district_id
                }
            });
        }
        if (req.query.state_id) {
            tags_required = true;
            whereTags[Op.and].push({
                state_id: {
                    [Op.eq]: req.query.state_id
                }
            });
        }

        let event = await Event.findAndCountAll({
            where: where,
            order: orderBy,
            offset: parseInt(req.query.page - 1) * parseInt(req.query.per_page),
            limit: parseInt(req.query.per_page),
            distinct: true,
            include: [{
                model: EventRecipient,
                where: {
                    created_by: {
                        [Op.eq]: req.user_id
                    }
                },
                include: [{
                    model: User,
                    include: [{
                        model: Designation,
                        as: 'designation'
                    }]
                }],
                required: true
            },
            {

                model: EventType,
                required: false
            }, {
                model: EventStatusType,
                where: {
                    event_status_type_name: 'Archive'
                },
                required: true
            }, {
                model: Unit,
                required: false
            }, {
                model: Section,
                required: false
            }, {
                model: Desk,
                required: false
            }, {
                model: EventMessageType,
                required: false
            }, {
                model: EventMessageSubType,
                required: false
            }, {
                model: Grade,
                required: false
            },
            {
                model: EventTag,
                where: whereTags,
                required: tags_required
            },
            {
                model: User,
                required: false,
                include: [{
                    model: Designation,
                    as: 'designation',
                    required: false,
                }]
            }

            ]
        });

        let resultArr = [];
        event.rows = JSON.parse(JSON.stringify(event.rows));
        for (let index = 0; index < event.rows.length; index++) {
            const element = event.rows[index];
            let To = [];
            let To_Designation = [];

            for (let index1 = 0; index1 < element.event_recipients.length; index1++) {
                const element1 = element.event_recipients[index1];
                if (element1.user) {
                    To.push(element1.user.user_email);
                    if (element1.user.designation) {
                        To_Designation.push(element1.user.designation.designation_name);
                    }
                }
            }
            resultArr.push({
                event_id: element.event_id,
                event_message_type: element.event_message_type ? element.event_message_type.event_message_type_name : "",
                type: element.event_type ? element.event_type.event_type_name : "",
                from: element.user ? element.user.user_firstname + " " + element.user.user_lastname : '',
                to: To.toString(),
                to_designation: To_Designation.toString(),
                from_designation: element.user && element.user.designation ? element.user.designation.designation_name : "",
                subject: element.subject,
                place: element.event_tag ? element.event_tag.place : "",
                grading: element.grade ? element.grade.grade_name : "",
                date: element.created_date,
                pdf: await EventAttachment.findAll({ where: { event_id: element.event_id } })
            })
        }
        let result = new Object();
        result.data = resultArr;
        result.total = event.count;
        return res.json(await commonResponse.responseReturn(true, req, commonMessage.Message.DATA_FOUND, result))
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
};


const deleteAttachment = async (req, res) => {
    try {
        let objEvent = req.body;

        let event = await EventAttachment.findOne({
            where: {
                event_attachment_id: objEvent.event_attachment_id
            }
        });
        if (event) {
            await s3.deleteImage("events/" + event.attachment_name);
            await EventAttachment.destroy({
                where: {
                    event_attachment_id: objEvent.event_attachment_id
                }
            })

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_ATTACHMENT_REMOVE, null));
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_ATTACHMENT_INVALID_EVENT_ID, null));
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}
const deleteDraftAttachment = async (req, res) => {
    try {
        let objEvent = req.body;
        let event = await EventDraftAttachment.findOne({
            where: {
                event_draft_attachment_id: objEvent.event_draft_attachment_id
            }
        });
        if (event) {
            await s3.deleteImage("events/" + event.attachment_name);
            await EventDraftAttachment.destroy({
                where: {
                    event_draft_attachment_id: objEvent.event_draft_attachment_id
                }
            })

            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_ATTACHMENT_REMOVE, null));
        } else {
            return res.json(await commonResponse.responseReturn(true, req, commonMessage.Event.EVENT_ATTACHMENT_INVALID_EVENT_ID, null));
        }
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }


        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

const getEventWithFilter = async (req, res) => {
    try {
        let {
            from_date,
            to_date,
            per_page,
            page,
            order_by,
            field_name,
            crop_id,
            suicide_type,
            organization_id,
            leader_id,
            event_message_type_id,
            event_message_sub_type_id,
            unit_id,
            section_id,
            desk_id,
            grade_id,
            state_id,
            district_id,
            taluk_id,
            place,
            injury,
            death,
            news_papers,
            agency_id,
            prison_id,
            case_type,
            court_id,
            other_tags,
            police_station_id,
            section_of_law,
            dam_id,
        } = req.body;
        let whereEventQuery = [];
        page = parseInt(page);
        per_page = parseInt(per_page);

        let pageNo = 0;
        pageNo = per_page * page;
        pageNo = pageNo - per_page;
        if (from_date && to_date) {
            let fromDate = moment(from_date).format('YYYY-MM-DD');
            let toDate = moment(to_date).format('YYYY-MM-DD');
            whereEventQuery.push(` e.created_date::date BETWEEN date '${fromDate}' AND date '${toDate}' `);
        }
        // Check unit_id
        if (unit_id) {
            console.log((unit_id))
            whereEventQuery.push(` e.unit_id IN( ${unit_id}) `);

        }

        // Check section_id
        if (section_id) {
            whereEventQuery.push(` e.section_id IN(${section_id}) `);

        }

        // Check desk_id
        if (desk_id) {
            whereEventQuery.push(` e.desk_id IN(${desk_id}) `);

        }

        // Check grade_id
        if (grade_id) {
            whereEventQuery.push(` e.grade_id = '${grade_id}' `);
        }

        // Check state_id for event
        if (state_id) {
            whereEventQuery.push(` etgs.state_id = '${state_id}' `);
        }

        // Check district_id for event
        if (district_id) {
            whereEventQuery.push(` etgs.district_id = '${district_id}' `);
        }

        // Check taluk_id for event
        if (taluk_id) {
            whereEventQuery.push(` etgs.taluk_id = '${taluk_id}' `);
        }

        if (dam_id) {
            whereEventQuery.push(` etgs.dam_id = '${dam_id}' `);
        }

        // Check place for event
        if (place) {
            whereEventQuery.push(` lower(etgs.place) LIKE '%${place.toLowerCase()}%' `);
        }
        if (event_message_type_id) {
            whereEventQuery.push(` e.event_message_type_id = '${event_message_type_id}' `);
        }

        if (event_message_sub_type_id) {
            whereEventQuery.push(` e.event_message_sub_type_id = '${event_message_sub_type_id}' `);
        }

        if (organization_id) {
            whereEventQuery.push(` etgo.organization_id = '${organization_id}' `);
        }

        if (leader_id) {
            whereEventQuery.push(` etgl.leader_id = '${leader_id}' `);
        }
        if (crop_id) {
            whereEventQuery.push(` etgs.crop_id = '${crop_id}' `);
        }
        if (suicide_type) {
            whereEventQuery.push(` lower(etgs.suicide_type) LIKE '%${suicide_type.toLowerCase()}%' `);
        }
        if (other_tags) {
            whereEventQuery.push(` lower(e.other_tags) LIKE '%${other_tags.toLowerCase()}%' `);
        }
        if (court_id) {
            whereEventQuery.push(` etgs.court_id = '${court_id}' `);
        }
        if (case_type) {
            whereEventQuery.push(` etgs.case_type LIKE '%${case_type.toLowerCase()}%' `);
        }
        if (prison_id) {
            whereEventQuery.push(` etgs.prison_id = '${prison_id}' `);
        }
        if (agency_id) {
            whereEventQuery.push(` etgs.agency_id = '${agency_id}' `);
        }

        if (news_papers) {
            whereEventQuery.push(` lower(etgs.news_papers) LIKE '%${news_papers.toLowerCase()}%' `);
        }
        if (death) {
            whereEventQuery.push(` lower(etgs.death) LIKE '%${death.toLowerCase()}%' `);
        }
        if (injury) {
            whereEventQuery.push(` lower(etgs.injury) LIKE '%${injury.toLowerCase()}%' `);
        }
        let eventWhereClause = ``;
        if (whereEventQuery && whereEventQuery.length > 0) {
            eventWhereClause = ` WHERE ` + whereEventQuery.join(' AND ');
        }


        if (police_station_id) {
            if (eventWhereClause) {
                eventWhereClause += (`AND (eudr.police_station_id = '${police_station_id}' OR efir.police_station_id = '${police_station_id}')`)
            } else {
                eventWhereClause += (`WHERE (eudr.police_station_id = '${police_station_id}' OR efir.police_station_id = '${police_station_id}')`)
            }

        }
        if (section_of_law) {
            if (eventWhereClause) {
                eventWhereClause += (`AND (lower(eudr.section_of_law) LIKE '%${section_of_law.toLowerCase()}%' OR lower(efir.section_of_law) LIKE '%${section_of_law.toLowerCase()}%')`)
            } else {
                eventWhereClause += (`WHERE (lower(eudr.section_of_law) LIKE '%${section_of_law.toLowerCase()}%' OR lower(efir.section_of_law) LIKE '%${section_of_law.toLowerCase()}%')`)
            }

        }


        let orderByQuery = `e.created_date DESC`;
        if (field_name && order_by) {
            if (filed_name == "event_type_name") {
                orderByQuery = ("et.event_type_name " + order_by);
            }
            if (filed_name == "from") {
                from = ("CONCAT(u.user_firstname,' ',u.user_lastname) " + order_by);
            }
            if (filed_name == "subject") {
                orderByQuery = ("e.subject " + order_by);
            }
            if (filed_name == "grade_name") {
                orderByQuery = ("gr.grade_name " + order_by);
            }
            if (filed_name == "place") {
                orderByQuery = ("etgs.place " + order_by);
            }
            if (filed_name == "created_date") {
                orderByQuery = ("e.created_date " + order_by);
            }
        }

        let query = `
            SELECT 
            DISTINCT e.event_id,
            et.event_type_name ,CONCAT(u.user_firstname,' ',u.user_lastname) As From,e.subject,gr.grade_name,
            etgs.place,e.created_date AS date
            FROM "event" AS e
            LEFT JOIN "event_types" AS et ON e.event_type_id = et.event_type_id
            LEFT JOIN "event_tags" AS etgs ON e.event_id = etgs.event_id
            LEFT JOIN "event_tag_organization" AS etgo ON etgo.event_id = etgs.event_id
            LEFT JOIN "event_tag_leader" AS etgl ON etgl.event_id = etgs.event_id
            LEFT JOIN "event_recipient" AS er ON e.event_id = er.event_id
            LEFT JOIN "user" AS u ON u.user_id = er.created_by
            LEFT JOIN "event_udr" AS eudr ON eudr.event_id = e.event_id
            LEFT JOIN "event_fir" AS efir ON efir.event_id = e.event_id
            LEFT JOIN "grade" AS gr ON e.grade_id = gr.grade_id  ${eventWhereClause} 
        `;

        let countQuery = `SELECT
        COUNT(*) As totalRecord
    FROM
        ( SELECT 
            DISTINCT e.event_id
      FROM "event" AS e
      LEFT JOIN "event_types" AS et ON e.event_type_id = et.event_type_id
      LEFT JOIN "event_tags" AS etgs ON e.event_id = etgs.event_id
      LEFT JOIN "event_tag_organization" AS etgo ON etgo.event_id = etgs.event_id
      LEFT JOIN "event_tag_leader" AS etgl ON etgl.event_id = etgs.event_id
      LEFT JOIN "event_recipient" AS er ON e.event_id = er.event_id
      LEFT JOIN "user" AS u ON u.user_id = er.created_by
      LEFT JOIN "event_udr" AS eudr ON eudr.event_id = e.event_id
      LEFT JOIN "event_fir" AS efir ON efir.event_id = e.event_id
      LEFT JOIN "grade" AS gr ON e.grade_id = gr.grade_id  ${eventWhereClause}
      ) AS totalRecord`

        // Total Count Query


        query += `
        ORDER BY ${orderByQuery}
            OFFSET ${pageNo}
            LIMIT ${per_page};
        `;

        console.log(query)
        await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then(async function (total) {
            console.log(total)
            await sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(async function (rows) {
                if (rows && rows.length > 0) {
                    // Attachments
                    let results = new Object();
                    results.data = rows;
                    results.total = total[0].totalrecord;
                    return res.json(await commonResponse.response(true, commonMessage.Message.DATA_FOUND, results));
                } else {
                    let results = new Object();
                    results.data = rows;
                    results.total = 0;
                    return res.json(await commonResponse.response(true, commonMessage.Message.DATA_FOUND, results));
                }
            });
        });
    } catch (err) {
        if (process.env.APP_ENV == "development") {
            console.error(
                "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                err.message
            );
            let url =
                "Location of Error : " +
                req.originalUrl +
                "  Method : " +
                req.method +
                "  Request Body : " +
                JSON.stringify(req.body);
            console.error(url);
        }

        return res.json(await commonResponse.responseReturn(false, req, commonMessage.Message.SOMETHING_WRONG, null));
    }
}

module.exports = {
    getUserList,
    createEvent,
    getEventMetaDataTags,
    storeEventTags,
    getEventWithPagination,
    getEventById,
    forwardEvent,
    createEventDraft,
    submitEvent,
    getEventDraftWithPagination,
    updateEventSummary,
    addEventAttachment,
    updateOtherTags,
    updateEventStatus,
    printPdf,
    updateEventUnitSectionDesk,
    getEventSentWithPagination,
    getEventInboxWithPagination,
    deleteDraft,
    getEventActionDueWithPagination,
    UpdateEventDraft,
    deleteSentEvent,
    deleteInboxEvent,
    addEventTags,
    getEventDraftById,
    updateEventsOtherTags,
    getEventArchiveWithPagination,
    addEventDraftTags,
    deleteAttachment,
    deleteDraftAttachment,
    getEventWithFilter
}