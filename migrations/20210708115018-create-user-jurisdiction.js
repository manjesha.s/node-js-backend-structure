'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('user_jurisdiction', {
      user_jurisdiction_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user',
          key: 'user_id',
          as: 'user_id',
        }
      },
      jurisdiction_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'jurisdiction',
          key: 'jurisdiction_id',
          as: 'jurisdiction_id',
        }
      },
      created_by: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      created_date: {
        created_date: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('user_jurisdiction');
  }
};